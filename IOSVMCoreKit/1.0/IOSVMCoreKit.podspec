Pod::Spec.new do |s|
  s.name             = "IOSVMCoreKit"
  s.version          = "1.0"
  s.summary          = "Useful set of timesaving tools"
  s.homepage         = "https://voodoo-mobile@bitbucket.org/voodoo-mobile/iosvmcorekit.git"
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { "Voodoo Mobile" => "public@voodoo-mobile.com" }
  s.source           = { :git => "https://voodoo-mobile@bitbucket.org/voodoo-mobile/iosvmcorekit.git", branch:'master', tag:"1.0"}

  s.platform     = :ios, '7.0'
  s.requires_arc = true

  s.source_files = 'iOSVM.Core/**/*.{h,m}'
  s.resource_bundles = {
    'iOSVM.Core' => ['Pod/Assets/*.png']
  }

    s.public_header_files = 'iOSVM.Core/**/*.h'
    s.frameworks = 'Foundation', 'AVFoundation', 'UIKit'
    s.dependency 'AFNetworking', '~> 2.3'
    s.dependency 'MBProgressHUD', '~> 0.8'
    s.dependency 'TDBadgedCell', '~> 3.0'
end
