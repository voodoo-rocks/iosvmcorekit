Pod::Spec.new do |s|
  s.name             = "IOSVMCoreKit"
  s.version          = "2.4.5"
  s.summary          = "Useful set of timesaving tools"
  s.homepage         = "https://voodoo-rocks@bitbucket.org/voodoo-rocks/iosvmcorekit.git"
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { "Voodoo Mobile" => "public@voodoo-mobile.com" }
  s.source           = { :git => "https://voodoo-rocks@bitbucket.org/voodoo-rocks/iosvmcorekit.git", branch:'master', :tag => s.version.to_s }

  s.platform     = :ios, '8.0'
  s.requires_arc = true

  s.source_files = 'iOSVM.Core/**/*.{h,m}'
  #s.resource_bundles = {
  #  'iOSVM.Core' => ['Pod/Assets/*.png']
  #}

    s.public_header_files = 'iOSVM.Core/**/*.h'
    s.frameworks = 'Foundation', 'AVFoundation', 'UIKit'
    s.dependency 'AFNetworking', '~> 3.0'
    s.dependency 'MBProgressHUD', '~> 0.8'
    s.dependency 'XMLDictionary', '~> 1.4'

end
