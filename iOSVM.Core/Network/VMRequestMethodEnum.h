//
//  VMRequestMethodEnum.h
//  iOSVM.Core
//
//  Created by Dmitry Tihonov on 19.11.14.
//  Copyright (c) 2014 Voodoo Mobile. All rights reserved.
//

#import "VMNamedEnum.h"

typedef NS_ENUM(NSInteger, VMRequestMethodType){
    VMRequestMethodTypePost,
    VMRequestMethodTypeGet,
    VMRequestMethodTypePut,
    VMRequestMethodTypeDelete
};

@interface VMRequestMethodEnum : VMNamedEnum

+ (instancetype)get;
+ (instancetype)post;
+ (instancetype)put;
+ (instancetype)delete;

@end
