//
//  Exception.h
//  RippIn
//
//  Created by Alexander Kryshtalev on 09.11.12.
//  Copyright (c) 2012 Voodoo Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VMEntity.h"

@interface VMException : VMEntity

@property (copy) NSString *name;
@property (copy) NSNumber *code;
@property (copy) NSNumber *status;
@property (copy) NSString *message;

@end
