//
//  VMBaseRequest.m
//  RippIn
//
//  Created by Alexander Kryshtalev on 06.12.12.
//  Copyright (c) 2012 Voodoo Mobile. All rights reserved.
//

#import "VMBaseRequest.h"
#import "VMRequestHeader.h"

@implementation VMBaseRequest
{
	VMRequestHeader *header;
}

@synthesize method, baseUrl;

- (id)initWithMethod:(NSString *)_method;
{
	self = [self init];
	method = _method;
	return self;
}

- (void)setBaseUrl:(NSString *)_baseUrl {
	baseUrl = _baseUrl;
}

- (NSString *)baseUrl {
	return baseUrl;
}

- (NSString *)url
{
    if (!self.baseUrl) {
        @throw [NSException exceptionWithName:@"Uninitialized exception" reason:@"baseUrl is null" userInfo:nil];
    }
    
    NSString *current = self.method;
 	if (current != nil && current.length != 0) {
        return [self.baseUrl stringByAppendingString:current];
    } else {
 		return self.baseUrl;
 	}
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *dictionary = [super dictionaryRepresentation].mutableCopy;
    [dictionary removeObjectForKey:@"method"];
    [dictionary removeObjectForKey:@"baseUrl"];
    return dictionary;
}

- (NSDictionary *)headers;
{
	return nil;
}

- (NSDictionary *)parameters;
{
    return self.dictionaryRepresentation;
}

@end
