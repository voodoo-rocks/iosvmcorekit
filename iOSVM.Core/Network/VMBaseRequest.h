//
//  VMBaseRequest.h
//  RippIn
//
//  Created by Alexander Kryshtalev on 06.12.12.
//  Copyright (c) 2012 Voodoo Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VMEntity.h"
#import "VMRequestOptions.h"

@interface VMBaseRequest : VMEntity

@property (copy) NSString *method;
@property (copy, nonatomic) NSString *baseUrl;


- (id)initWithMethod:(NSString *)method;
- (NSString *)url;
- (NSDictionary *)headers;
- (NSDictionary *)parameters;

@end
