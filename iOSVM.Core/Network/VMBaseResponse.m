//
//  VMBaseResponse.m
//  RippIn
//
//  Created by Alexander Kryshtalev on 06.12.12.
//  Copyright (c) 2012 Voodoo Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VMBaseResponse.h"

@implementation VMBaseResponse

- (id)initWithJson:(NSString *)json
{
	return [self initWithDictionary:[self dictionaryFromJson:json]];
}

- (id)initWithDictionary:(NSDictionary *)dictionary
{
    self = [self init];
    if (dictionary) {
        self.dictionaryRepresentation = dictionary;
    }
    return self;
}

@end
