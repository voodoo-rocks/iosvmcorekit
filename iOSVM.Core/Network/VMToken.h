//
//  VMToken.h
//  iOSVM.Core
//
//  Created by Alexander Kryshtalev on 19/06/14.
//  Copyright (c) 2014 Voodoo Mobile. All rights reserved.
//

#import "VMEntity.h"

@interface VMToken : VMEntity

@property (copy) NSString *hash;

@end
