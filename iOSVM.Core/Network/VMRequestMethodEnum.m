//
//  VMRequestMethodEnum.m
//  iOSVM.Core
//
//  Created by Dmitry Tihonov on 19.11.14.
//  Copyright (c) 2014 Voodoo Mobile. All rights reserved.
//

#import "VMRequestMethodEnum.h"

@implementation VMRequestMethodEnum

- (instancetype)init;
{
    self = [super init];
    names = @[@"POST", @"GET", @"PUT", @"DELETE"];
    return self;
}

+ (instancetype)get
{
    VMRequestMethodEnum *methodEnum = [[VMRequestMethodEnum alloc] init];
    methodEnum.typeId = VMRequestMethodTypeGet;
    return methodEnum;
}

+ (instancetype)post
{
    VMRequestMethodEnum *methodEnum = [[VMRequestMethodEnum alloc] init];
    methodEnum.typeId = VMRequestMethodTypePost;
    return methodEnum;
}

+ (instancetype)put;
{
    VMRequestMethodEnum *methodEnum = [[VMRequestMethodEnum alloc] init];
    methodEnum.typeId = VMRequestMethodTypePut;
    return methodEnum;
}

+ (instancetype)delete;
{
    VMRequestMethodEnum *methodEnum = [[VMRequestMethodEnum alloc] init];
    methodEnum.typeId = VMRequestMethodTypeDelete;
    return methodEnum;
}

@end
