//
//  VMRequestManager.m
//  Pulse
//
//  Created by Dmitry Tihonov on 21.05.13.
//  Copyright (c) 2013 com.voodoo-mobile. All rights reserved.
//

#import "VMRequestManager.h"
#import "AFNetworking.h"
#import "VMFlashManager.h"
#import "VMConstants.h"
#import "MBProgressHud.h"
#import "NSDictionary+DictionaryFormat.h"

@implementation VMRequestManager

NSString const *kRepeats = @"repeats";
NSString const *kShouldDumpResponse = @"shouldDumpResponse";

static VMRequestManager *instance;

- (id)init
{
    const int kMemoryCache = 2 * 1024 * 1024; //2MB
    const int kDiskCache = 100 * 1024 * 1024; //100MB
    
    self = [super init];
    if (self) {
        self.defaultOptions = [VMRequestOptions defaultOptions];
        NSURLCache.sharedURLCache = [[NSURLCache alloc] initWithMemoryCapacity:kMemoryCache
                                                                  diskCapacity:kDiskCache
                                                                      diskPath:nil];
        
        NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
        networkSessionManager = [[AFHTTPSessionManager alloc] initWithSessionConfiguration:configuration];
        cancelableNetworkSessionManager = [[AFHTTPSessionManager alloc] initWithSessionConfiguration:configuration];
    }
    return self;
}


- (void)executeRequest:(VMBaseRequest *)post withNetworkSessionManager:(AFHTTPSessionManager *)_networkSessionManager withOptions:(VMRequestOptions *)requestOptions withCallback:(RequestCompletionBlock)completionBlock;
{
    [self showProgressForOptions:requestOptions];
    
    AFHTTPRequestSerializer *serializer = [self serializerForRequestType:requestOptions];
    
    NSDictionary *postParameters = post.parameters;
    NSMutableURLRequest *request = [serializer requestWithMethod:requestOptions.requestMethod.name URLString:post.url parameters:postParameters error:nil];
    
    request.timeoutInterval = 60 * 10; // timeout is 10 minutes
    
    for (NSString *key in post.headers.allKeys) {
        [request addValue:[post.headers objectForKey:key] forHTTPHeaderField:key];
    }
    
    NSDictionary *cookiesDict = [NSHTTPCookie requestHeaderFieldsWithCookies:cookies];
    [request setAllHTTPHeaderFields:cookiesDict];
    
    if (requestOptions.shouldPrintLogs) {
        [self dumpRequestDictionary:postParameters forRequest:request];
    }
    
    NSURLSessionDataTask *task = [_networkSessionManager dataTaskWithRequest:request completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
        
        [self stopProgress];
        if (requestOptions.shouldPrintLogs) {
            [self dumpResponseDictionary:responseObject forRequest:request];
        }
        
        if(!error) {
            if (requestOptions.automaticallyHandleApiErrors) {
                VMBaseResponse *baseResponse = [[VMBaseResponse alloc] initWithDictionary:responseObject];
                if (!baseResponse.success) {
                    [[VMFlashManager sharedManager] showFlash:baseResponse.exception.message forDuration:VMConstants.flashAnimationDurationLong];
                    if ([baseResponse.exception.name isEqualToString:@"UnauthorizedHttpException"]) {
                        if (self.didReceiveUnauthorizedError) {
                            self.didReceiveUnauthorizedError();
                        }
                    }
                }
            }
        }
        else {
            VMBaseResponse *baseResponse = [[VMBaseResponse alloc] initWithDictionary:responseObject];
            if (baseResponse.exception.status.intValue == 401 || baseResponse.exception.status.intValue == 403) {
                if (self.didReceiveUnauthorizedError) {
                    self.didReceiveUnauthorizedError();
                }
            }
            
            if (error.code == -1009) {
                if (self.didReceiveNoConnectionError) {
                    self.didReceiveNoConnectionError();
                }
            }
            
            if([error.localizedDescription isEqualToString:@"The network connection was lost."]) {
                [self executeRequest:post withNetworkSessionManager:_networkSessionManager withOptions:requestOptions withCallback:completionBlock];
                return;
            }
            
            if (requestOptions.automaticallyHandleHttpErrors) {
                VMBaseResponse *baseResponse = [[VMBaseResponse alloc] initWithDictionary:responseObject];
                [[VMFlashManager sharedManager] showFlash:baseResponse.exception.message forDuration:VMConstants.flashAnimationDurationLong];
            }
        }
        
        if (completionBlock) {
            completionBlock(responseObject, error);
        }
    }];
    [task resume];
}

- (AFHTTPRequestSerializer *)serializerForRequestType:(VMRequestOptions *)options
{
    AFHTTPRequestSerializer *serializer = (options.requestMethod.typeId == VMRequestMethodTypeGet) ?
    [AFHTTPRequestSerializer serializer] :
    [AFJSONRequestSerializer serializer];
    return serializer;
}

- (void)executeRequest:(VMBaseRequest *)post withOptions:(VMRequestOptions *)requestOptions withCallback:(RequestCompletionBlock)completionBlock;
{
    [self executeRequest:post withNetworkSessionManager:networkSessionManager withOptions:requestOptions withCallback:completionBlock];
}

- (void)executeCancelableRequest:(VMBaseRequest *)post withOptions:(VMRequestOptions *)requestOptions withCallback:(RequestCompletionBlock)completionBlock;
{
    [self executeRequest:post withNetworkSessionManager:cancelableNetworkSessionManager withOptions:requestOptions withCallback:completionBlock];
}

- (void)executeRequest:(VMBaseRequest *)post withCallback:(RequestCompletionBlock)completionBlock;
{
    [self executeRequest:post withNetworkSessionManager:networkSessionManager withOptions:self.defaultOptions withCallback:completionBlock];
}

- (void)executeCancelableRequest:(VMBaseRequest *)post withCallback:(RequestCompletionBlock)completionBlock;
{
    [self executeRequest:post withNetworkSessionManager:cancelableNetworkSessionManager withOptions:self.defaultOptions withCallback:completionBlock];
}

- (void)executeBaseRequestWithUrl:(NSString *)url withCallback:(RequestCompletionBlock)completionBlock;
{
    VMBaseRequest *request = [VMBaseRequest new];
    request.baseUrl = url;
    
    return [self executeRequest:request withCallback:completionBlock];
}

- (void)cancelRequests
{
    [cancelableNetworkSessionManager.operationQueue cancelAllOperations];
}

- (void)showProgressForOptions:(VMRequestOptions *)options
{
    if (options.showActivityIndicator) {
        UIView *view = [UIApplication sharedApplication].keyWindow.rootViewController.view;
        if (view) {
            progress = [MBProgressHUD showHUDAddedTo:view animated:YES];
        }
    }
}

- (void)stopProgress
{
    [MBProgressHUD hideAllHUDsForView:[UIApplication sharedApplication].keyWindow.rootViewController.view animated:YES];
}

- (void)dumpRequestDictionary:(NSDictionary *)requestDictionary forRequest:(NSURLRequest *)request
{
    NSLog(@"%@", [NSString stringWithFormat:@"\n\nRequest to %s\n%s\n\n", [request.URL.absoluteString UTF8String], [[requestDictionary printBeautifulDictionary] UTF8String]]);
}

- (void)dumpResponseDictionary:(NSDictionary *)responseDictionary forRequest:(NSURLRequest *)request
{
    NSLog(@"%@", [NSString stringWithFormat:@"\n\nResponse from %s\n%s\n\n", [request.URL.absoluteString UTF8String], [[responseDictionary printBeautifulDictionary] UTF8String]]);
}

@end
