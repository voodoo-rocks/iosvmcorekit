//
//  VMBaseResponse.h
//  RippIn
//
//  Created by Alexander Kryshtalev on 06.12.12.
//  Copyright (c) 2012 Voodoo Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VMException.h"

@interface VMBaseResponse : VMEntity

@property BOOL success;
@property (copy) VMException *exception;

@end
