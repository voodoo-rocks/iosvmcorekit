//
//  VMRequestHeader.h
//  iOSVM.Core
//
//  Created by Alexander Kryshtalev on 29/07/14.
//  Copyright (c) 2014 Voodoo Mobile. All rights reserved.
//

#import "VMEntity.h"

@interface VMRequestHeader : VMEntity

@property (copy) NSString *bundleId;
@property (copy) NSString *version;
@property (copy) NSString *apiKey;

@end
