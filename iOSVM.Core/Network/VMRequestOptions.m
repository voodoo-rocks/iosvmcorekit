//
//  VMRequestOptions.m
//  Qtrank
//
//  Created by Alexander Kryshtalev on 02/05/14.
//  Copyright (c) 2014 Voodoo Mobile. All rights reserved.
//

#import "VMRequestOptions.h"

@implementation VMRequestOptions

+ (VMRequestOptions *)defaultOptions;
{
	VMRequestOptions *options = [[VMRequestOptions alloc] init];
	
	options.repeatsCount = 3; // Amount of repeats for the error connection has been lost
    options.automaticallyHandleHttpErrors = YES;
    options.automaticallyHandleApiErrors = YES;
	options.showActivityIndicator = YES;
    options.shouldPrintLogs = YES;
	options.timeout = 60 * 10; // 10 minutes
	options.cachePolicy = NSURLRequestUseProtocolCachePolicy;
    options.requestMethod = [VMRequestMethodEnum post];
    
	return options;
}

@end
