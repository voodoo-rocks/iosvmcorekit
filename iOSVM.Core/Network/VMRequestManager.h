//
//  VMRequestManager.h
//  Pulse
//
//  Created by Dmitry Tihonov on 21.05.13.
//  Copyright (c) 2013 com.voodoo-mobile. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VMBaseRequest.h"
#import "VMBaseResponse.h"
#import "VMRequestOptions.h"
#import "VMSharedManager.h"
#import "VMBlocks.h"

@class MBProgressHUD, AFHTTPSessionManager;

typedef void (^RequestCompletionBlock)(NSDictionary *responseDictionary, NSError *error);

@interface VMRequestManager : VMSharedManager
{
    MBProgressHUD *progress;
    AFHTTPSessionManager *networkSessionManager, *cancelableNetworkSessionManager;
    NSArray *cookies;
}

- (void)cancelRequests;

- (void)executeRequest:(VMBaseRequest *)post withOptions:(VMRequestOptions *)requestOptions withCallback:(RequestCompletionBlock)completionBlock;
- (void)executeCancelableRequest:(VMBaseRequest *)post withOptions:(VMRequestOptions *)requestOptions withCallback:(RequestCompletionBlock)completionBlock;

- (void)executeRequest:(VMBaseRequest *)post withCallback:(RequestCompletionBlock)completionBlock;
- (void)executeCancelableRequest:(VMBaseRequest *)post withCallback:(RequestCompletionBlock)completionBlock;

- (void)executeBaseRequestWithUrl:(NSString *)url withCallback:(RequestCompletionBlock)completionBlock;

@property (retain) VMRequestOptions *defaultOptions;

@property (copy) VMVoidBlock didReceiveUnauthorizedError, didReceiveNoConnectionError;

@end
