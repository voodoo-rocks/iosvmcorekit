//
//  VMRequestOptions.h
//  Qtrank
//
//  Created by Alexander Kryshtalev on 02/05/14.
//  Copyright (c) 2014 Voodoo Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VMRequestMethodEnum.h"

@interface VMRequestOptions : NSObject

+ (VMRequestOptions *)defaultOptions;

@property BOOL automaticallyHandleHttpErrors;
@property BOOL automaticallyHandleApiErrors; // can't rely on isSucceded flag - it exists only in our services
@property (copy) VMRequestMethodEnum *requestMethod;
@property BOOL showActivityIndicator;
@property BOOL shouldPrintLogs;
@property NSTimeInterval timeout;
@property NSURLRequestCachePolicy cachePolicy;
@property NSUInteger repeatsCount;

@end
