//
//  VMValidationForm.h
//  iOSVM.Core
//
//  Created by Alexander Kryshtalev on 23/07/14.
//  Copyright (c) 2014 Voodoo Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VMValidationDelegate.h"
#import "VMValidationGroup.h"

@interface VMValidationForm : NSObject

@property (nonatomic, retain) NSArray *validationGroups;

- (BOOL)validate;

@property BOOL stopValidationOnError;
@property (retain) IBOutlet NSObject <VMValidationDelegate> *delegate;

@end
