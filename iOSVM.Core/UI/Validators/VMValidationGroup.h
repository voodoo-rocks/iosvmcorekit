//
//  VMValidationGroup.h
//  iOSVM.Core
//
//  Created by Alexander Kryshtalev on 23/07/14.
//  Copyright (c) 2014 Voodoo Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VMBaseValidator.h"
#import "VMValidationDelegate.h"
#import "VMValidatable.h"

@class VMValidationForm;

@interface VMValidationGroup : NSObject

@property (retain) IBOutletCollection(NSObject) NSArray *fieldsForValidation;
@property (retain) IBOutlet VMBaseValidator *validator;

@property (retain, nonatomic) IBOutlet VMValidationForm *validationForm;

@property IBInspectable BOOL shouldDisplayFlash;
@property IBInspectable BOOL shouldFocusOnField;

- (BOOL)validate;

@end
