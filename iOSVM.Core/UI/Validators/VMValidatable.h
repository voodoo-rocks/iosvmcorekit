//
//  VMValidatable.h
//  iOSVM.Core
//
//  Created by Alexander Kryshtalev on 23/07/14.
//  Copyright (c) 2014 Voodoo Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol VMValidatable <NSObject>

@property (nonatomic, readonly, retain) NSString *textForValidation;
@property (nonatomic, readonly, retain) NSString *messageOnFail;

- (void)focus;

@end
