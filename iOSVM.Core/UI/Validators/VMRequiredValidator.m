//
//  VMRequiredValidator.m
//  iOSVM.Core
//
//  Created by Alexander Kryshtalev on 23/07/14.
//  Copyright (c) 2014 Voodoo Mobile. All rights reserved.
//

#import "VMRequiredValidator.h"

@implementation VMRequiredValidator

- (id)init
{
	self = [super init];
	self.messageOnFail = @"The field is required";
	return self;
}

- (BOOL)validate:(NSString *)value;
{
	return value && value.length > 0;
}

@end
