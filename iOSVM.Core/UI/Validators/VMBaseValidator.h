//
//  BaseValidator.h
//  BetYou
//
//  Created by Alexander Kryshtalev on 19.11.12.
//  Copyright (c) 2012 Voodoo Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VMBaseValidator : NSObject

- (BOOL)validate:(NSString *)value;

@property (retain) IBOutlet NSString *messageOnFail;

@end
