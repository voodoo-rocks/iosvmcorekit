//
//  VMValidationGroup.m
//  iOSVM.Core
//
//  Created by Alexander Kryshtalev on 23/07/14.
//  Copyright (c) 2014 Voodoo Mobile. All rights reserved.
//

#import "VMValidationGroup.h"
#import "VMValidationForm.h"
#import "VMFlashManager.h"

@implementation VMValidationGroup

@synthesize validationForm;

- (id)init
{
    self = [super init];
    if (self) {
        _shouldDisplayFlash = YES;
        _shouldFocusOnField = YES;
    }
    return self;
}

- (BOOL)validate
{
	BOOL result = YES;
	for (NSObject <VMValidatable> *validatable in self.fieldsForValidation) {
		result &= [self.validator validate:validatable.textForValidation];
		if (!result) {
			if (self.validationForm.delegate &&
				[self.validationForm.delegate respondsToSelector:@selector(didFailValidation:forValidator:)]) {
				[self.validationForm.delegate didFailValidation:validatable forValidator:self.validator];
			}

            if (self.shouldFocusOnField) {
                [validatable focus];
            }
            
            if (self.shouldDisplayFlash) {
                [[VMFlashManager sharedManager] showFlash:(validatable.messageOnFail ?: self.validator.messageOnFail)];
            }

			if (self.validationForm.stopValidationOnError) {
				break;
			}
		}
		
		if (result && self.validationForm.delegate && [self.validationForm.delegate respondsToSelector:@selector(didPassValidation:forValidator:)]) {
			[self.validationForm.delegate didPassValidation:validatable forValidator:self.validator];
		}
	}
	return result;
}

- (void)setValidationForm:(VMValidationForm *)_validationForm
{
	validationForm = _validationForm;
	if (![validationForm.validationGroups containsObject:self]) {
		NSMutableArray *mutable = validationForm.validationGroups.mutableCopy;
		[mutable addObject:self];
		
		validationForm.validationGroups = mutable.copy;
	}
}

@end
