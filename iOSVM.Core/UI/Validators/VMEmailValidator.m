//
//  VMEmailValidator.m
//  iOSVM.Core
//
//  Created by Alexander Kryshtalev on 23/07/14.
//  Copyright (c) 2014 Voodoo Mobile. All rights reserved.
//

#import "VMEmailValidator.h"

@implementation VMEmailValidator

- (id)init
{
	self = [super init];
	self.messageOnFail = @"The value is not a valid email";
	return self;
}

- (BOOL)validate:(NSString *)value;
{
	if (self.allowNilOrEmpty && (value == nil || value.length == 0)) {
		return YES;
	}
	
	NSString *emailRegex = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,25}";
	NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
	return [emailTest evaluateWithObject:value];
}

@end
