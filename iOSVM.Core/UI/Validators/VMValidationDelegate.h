//
//  VMValidationDelegate.h
//  iOSVM.Core
//
//  Created by Alexander Kryshtalev on 23/07/14.
//  Copyright (c) 2014 Voodoo Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VMValidatable.h"

@class VMBaseValidator;
@class VMValidatable;

@protocol VMValidationDelegate <NSObject>

@optional

- (BOOL)didFailValidation:(NSObject<VMValidatable> *)validatable forValidator:(VMBaseValidator *)validator;

- (void)didPassValidation:(NSObject<VMValidatable> *)validatable forValidator:(VMBaseValidator *)validator;

@end
