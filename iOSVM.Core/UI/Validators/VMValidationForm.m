//
//  VMValidationForm.m
//  iOSVM.Core
//
//  Created by Alexander Kryshtalev on 23/07/14.
//  Copyright (c) 2014 Voodoo Mobile. All rights reserved.
//

#import "VMValidationForm.h"

@implementation VMValidationForm

@synthesize validationGroups;

- (id)init
{
	self = [super init];
	self.validationGroups = [NSArray array];
	self.stopValidationOnError = YES;
	return self;
}

- (BOOL)validate;
{
	BOOL result = YES;
	for (VMValidationGroup *group in validationGroups) {
		result &= [group validate];
		
		if (!result && self.stopValidationOnError) {
			return result;
		}
	}
	
	return result;
}

- (NSArray *)validationGroups
{
	return validationGroups;
}

@end
