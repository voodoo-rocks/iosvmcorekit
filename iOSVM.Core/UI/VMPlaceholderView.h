//
//  VMPlaceholderView.h
//  Malco
//
//  Created by Alexander Kryshtalev on 12.10.13.
//  Copyright (c) 2013 Voodoo Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VMPlaceholderView : UIView

@property (retain) IBOutlet UIViewController *controller;

@end
