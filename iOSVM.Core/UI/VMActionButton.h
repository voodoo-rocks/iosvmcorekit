//
//  VMAlertButton.h
//  Ovenbot
//
//  Created by Olesya Kondrashkina on 11/14/13.
//  Copyright (c) 2013 Voodoo Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VMBlocks.h"

@interface VMActionButton : NSObject

@property (copy) NSString *title;
@property (copy) VMVoidBlock actionBlock;

+ (VMActionButton *)buttonWithTitle:(NSString *)title;
+ (VMActionButton *)buttonWithTitle:(NSString *)title withAction:(VMVoidBlock)action;

@end
