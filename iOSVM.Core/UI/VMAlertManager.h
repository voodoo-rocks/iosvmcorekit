//
//  VMAlert.h
//  NueTune
//
//  Created by Alexander Kryshtalev on 16.10.13.
//  Copyright (c) 2013 Voodoo Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VMSharedManager.h"

@interface VMAlertManager : VMSharedManager

- (void)notImplementedYet;
- (void)showError:(NSString *)error;
- (void)showInfo:(NSString *)error;
- (void)showInfo:(NSString *)error withTitle:(NSString *)title;

@end
