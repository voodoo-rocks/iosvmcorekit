//
//  VMActionSheet.h
//  Ovenbot
//
//  Created by Olesya Kondrashkina on 11/15/13.
//  Copyright (c) 2013 Voodoo Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VMActionButton.h"

@interface VMActionSheet : UIActionSheet <UIActionSheetDelegate>
{
    NSMutableArray *buttons;
}

- (id)initWithTitle:(NSString *)title cancelButton:(VMActionButton *)cancelButton destructiveButton:(VMActionButton *)destructiveButton otherButtonsAsArray:(NSArray *)otherButtons;

- (id)initWithTitle:(NSString *)title cancelButton:(VMActionButton *)cancelButton destructiveButton:(VMActionButton *)destructiveButton otherButtons:(VMActionButton *)otherButtons, ... NS_REQUIRES_NIL_TERMINATION;

- (NSInteger)addButton:(VMActionButton *)button;
- (void)show;

@end
