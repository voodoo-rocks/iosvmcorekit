//
//  UIView+Activity.m
//  iOSVM.Core
//
//  Created by Alexander Kryshtalev on 25/09/14.
//  Copyright (c) 2014 Voodoo Mobile. All rights reserved.
//

#import "UIView+Activity.h"
#import "MBProgressHUD.h"

@implementation ActivityHandler
{
	MBProgressHUD *hud;
}

- (id)initWithHud:(MBProgressHUD *)_hud
{
	self = [self init];
	hud = _hud;
	return self;
}

- (void)dismiss;
{
    [MBProgressHUD hideAllHUDsForView:hud.superview animated:YES];
}

@end

@implementation UIView (Activity)

- (void)showProgressWhile:(ActivityBlock)block;
{
	MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self animated:YES];
	ActivityHandler *handler = [[ActivityHandler alloc] initWithHud:hud];
	block(handler);
}

@end
