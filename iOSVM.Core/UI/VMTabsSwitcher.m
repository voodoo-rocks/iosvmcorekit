//
//  VMTabsSwitcher.m
//  iOSVM.Core
//
//  Created by Alexander Kryshtalev on 07/07/14.
//  Copyright (c) 2014 Voodoo Mobile. All rights reserved.
//

#import "VMTabsSwitcher.h"

@implementation VMTabsSwitcher

@synthesize controllers;

- (id)init
{
	self = [super init];
	self.controllers = [NSArray new];
	return self;
}

- (void)setSelectedIndex:(NSUInteger)selectedIndex
{
	self.contentView.controller = nil;
	
	if (!self.contentView) {
		[NSException raise:@"contentView is null" format:@"Please assign a view to the contentView variable"];
	}
	
	selectedController = [self.controllers objectAtIndex:selectedIndex];
	self.contentView.controller = selectedController;
    
    [super setSelectedIndex:selectedIndex];
}

@end
