//
//  VMAlert.m
//  NueTune
//
//  Created by Alexander Kryshtalev on 16.10.13.
//  Copyright (c) 2013 Voodoo Mobile. All rights reserved.
//

#import "VMAlertManager.h"

@implementation VMAlertManager

- (void)notImplementedYet
{
	UIAlertView* view = [[UIAlertView alloc] initWithTitle:@"" message:NSLocalizedString(@"This feature or screen is not implemented yet", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles: nil];
	[view performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:YES];
}

- (void)showError:(NSString *)error
{
	[[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:NSLocalizedString(error, nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles: nil] performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:YES];
}

- (void)showInfo:(NSString *)error withTitle:(NSString *)title;
{
    [[[UIAlertView alloc] initWithTitle:NSLocalizedString(title, nil) message:NSLocalizedString(error, nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles: nil] performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:YES];
}

- (void)showInfo:(NSString *)error;
{
	[self showInfo:error withTitle:nil];
}

@end
