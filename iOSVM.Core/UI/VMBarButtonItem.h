//
//  VMBarButtonItem.h
//  NueTune
//
//  Created by Alexander Kryshtalev on 16.10.13.
//  Copyright (c) 2013 Voodoo Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>

@class VMBarButtonItem;

typedef void(^BarButtonItemCallback)(VMBarButtonItem *buttonItem);

@interface VMBarButtonItem : UIBarButtonItem
{
	BarButtonItemCallback callback;
}

+ (instancetype)barButtonItemWithImage:(UIImage *)image andTitle:(NSString *)title forTarget:(id)target withAction:(SEL)action;

+ (instancetype)barButtonItemWithImage:(UIImage *)image forTarget:(id)target withAction:(SEL)action;
+ (instancetype)barButtonItemWithImage:(UIImage *)image withCallback:(BarButtonItemCallback)callback;

+ (instancetype)barButtonItemWithImage:(UIImage *)image withSelectedImage:(UIImage *)selectedImage forTarget:(id)target withAction:(SEL)action;
+ (instancetype)barButtonItemWithImage:(UIImage *)image withSelectedImage:(UIImage *)selectedImage andTitle:(NSString *)title forTarget:(id)target withAction:(SEL)action;

+ (instancetype)barButtonItemWithTitle:(NSString *)title forTarget:(id)target withAction:(SEL)action;
+ (instancetype)barButtonItemWithTitle:(NSString *)title withCallback:(BarButtonItemCallback)callback;

@end
