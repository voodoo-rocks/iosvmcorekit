//
//  VMCollectionViewCell.h
//  Qtrank
//
//  Created by Olesya Kondrashkina on 3/20/14.
//  Copyright (c) 2014 Voodoo Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UICollectionViewCell (ReusableCell)

+ (instancetype)cellForCollectionView:(UICollectionView *)collectionView forIndexPath:(NSIndexPath *)indexPath;
+ (instancetype)cellForCollectionView:(UICollectionView *)collectionView fromNib:(NSString *)nibName forIndexPath:(NSIndexPath *)indexPath;

@end
