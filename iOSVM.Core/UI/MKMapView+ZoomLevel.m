//
//  VMMapView.m
//  Foodomart
//
//  Created by Olesya Kondrashkina on 2/6/14.
//  Copyright (c) 2014 Alexei Pozdnyakov. All rights reserved.
//

#import "MKMapView+ZoomLevel.h"

@implementation MKMapView (ZoomLevel)

- (CGFloat)currentRadius
{
    MKMapPoint mapTopLeft = self.visibleMapRect.origin;
    
    MKMapPoint mapBottomRight = MKMapPointMake(
                                               self.visibleMapRect.origin.x + self.visibleMapRect.size.width,
                                               self.visibleMapRect.origin.y + self.visibleMapRect.size.height);
    
    CLLocationDistance diagonalDist = MKMetersBetweenMapPoints(mapTopLeft, mapBottomRight);
    return diagonalDist / 2;
}

- (void)centerAt:(CLLocationCoordinate2D)coordinate
{
	const NSUInteger zoomLevel = 16;
	[self centerAt:coordinate andZoomTo:zoomLevel];
}

- (void)centerAt:(CLLocationCoordinate2D)coordinate andZoomTo:(NSUInteger)zoomLevel
{
	// Magic numbers. I have no idea what they do here but it works.
	MKCoordinateSpan span = MKCoordinateSpanMake(0, 360.0 / pow(2, zoomLevel) * self.frame.size.width / 256.0);
    [self setRegion:MKCoordinateRegionMake(coordinate, span) animated:YES];
}

- (void)putSimplePinAt:(CLLocationCoordinate2D)coordinate withTitle:(NSString *)title;
{
	MKPointAnnotation *annotation = [[MKPointAnnotation alloc] init];
	[annotation setCoordinate:coordinate];
	[annotation setTitle:title]; //You can set the subtitle too
	[self addAnnotation:annotation];
}

@end
