//
//  ResizableTableView.m
//  Koolooks
//
//  Created by Olesya Kondrashkina on 10/02/16.
//
//

#import "VMResizableTableView.h"

@implementation VMResizableTableView

- (CGSize)intrinsicContentSize {
    [self layoutIfNeeded]; 
    return CGSizeMake(UIViewNoIntrinsicMetric, self.contentSize.height + self.contentInset.top + self.contentInset.bottom);
}

- (void)reloadData
{
    [super reloadData];
    [self invalidateIntrinsicContentSize];
    [self.superview layoutIfNeeded];
}

@end
