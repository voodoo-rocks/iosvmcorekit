//
//  VMButtonsSwitcher.h
//  iOSVM.Core
//
//  Created by Alexander Kryshtalev on 18/06/14.
//  Copyright (c) 2014 Voodoo Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "VMBlocks.h"
#import "VMSwitcher.h"

@interface VMButtonsSwitcher : NSObject <VMSwitcher>

@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *buttons;

- (id)initWithButtons:(NSArray *)buttons;

@property (copy) VMIntegerBlock didPressButtonAtIndex; // actually pressed a button with a finger

@property BOOL canDeselectButton;

@end
