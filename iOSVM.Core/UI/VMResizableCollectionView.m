//
//  ResizibleCollectionView.m
//  FriendZone
//
//  Created by Olesya Kondrashkina on 21/03/16.
//  Copyright © 2016 Alexander Kryshtalev. All rights reserved.
//

#import "VMResizableCollectionView.h"

@implementation VMResizableCollectionView

- (CGSize)intrinsicContentSize {
    [self layoutIfNeeded];
    return CGSizeMake(UIViewNoIntrinsicMetric, self.collectionViewLayout.collectionViewContentSize.height + self.contentInset.top + self.contentInset.bottom);
}

- (void)reloadData
{
    [super reloadData];
    [self invalidateIntrinsicContentSize];
    [self.superview layoutIfNeeded];
}

@end
