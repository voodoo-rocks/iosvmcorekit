//
//  VMCoolPopupOptions.h
//  WhereItsLive
//
//  Created by Alexander Kryshtalev on 29/04/14.
//  Copyright (c) 2014 Voodoo Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface VMPopupOptions : NSObject

+ (VMPopupOptions *)defaultOptions;

@property (assign) NSTextAlignment horizontalAlignment;
@property (assign) NSTextAlignment verticalAlignment;
@property (assign) float animationDuration;
@property (assign) BOOL enableDimmedBackground;


@end
