//
//  UIView+Capture.m
//  iOSVM.Core
//
//  Created by Alexander Kryshtalev on 29/08/14.
//  Copyright (c) 2014 Voodoo Mobile. All rights reserved.
//

#import "UIView+Capture.h"

@implementation UIView (Capture)

- (UIImage *)capture {
	
	if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)])
		UIGraphicsBeginImageContextWithOptions(self.bounds.size, NO, [UIScreen mainScreen].scale);
	else
		UIGraphicsBeginImageContext(self.bounds.size);
	
	[self.layer renderInContext:UIGraphicsGetCurrentContext()];
	UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	
	return image;
}


@end
