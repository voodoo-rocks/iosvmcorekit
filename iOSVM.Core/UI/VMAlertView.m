//
//  VMAlertView.m
//  Ovenbot
//
//  Created by Olesya Kondrashkina on 11/14/13.
//  Copyright (c) 2013 Voodoo Mobile. All rights reserved.
//

#import "VMAlertView.h"

@implementation VMAlertView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (NSInteger)addButton:(VMActionButton *)button
{
    [buttons addObject:button];
    return [super addButtonWithTitle:NSLocalizedString(button.title, nil)];
}

- (id)initWithTitle:(NSString *)title message:(NSString *)message cancelButton:(VMActionButton *)cancelButton;
{
    self = [super initWithTitle:NSLocalizedString(title, nil) message:NSLocalizedString(message, nil) delegate:self cancelButtonTitle:NSLocalizedString(cancelButton.title, nil) otherButtonTitles:nil];
    buttons = [NSMutableArray array];
    if (cancelButton) {
        [buttons addObject:cancelButton];
    }
    return self;
}

- (id)initWithTitle:(NSString *)title message:(NSString *)message cancelButton:(VMActionButton *)cancelButton otherButtons:(VMActionButton *)otherButtons, ...;
{
    self = [self initWithTitle:title message:message cancelButton:cancelButton];
    if (otherButtons) {
        VMActionButton *button = otherButtons;
        va_list list;
        va_start(list, otherButtons);
        do {
            [self addButton:button];
        } while ((button = va_arg(list, VMActionButton *)));
        va_end(list);
    }
    return self;
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (buttonIndex >= 0) {
        VMActionButton *button = [buttons objectAtIndex:buttonIndex];
        if (button.actionBlock) {
            button.actionBlock();
        }
    }
}

@end
