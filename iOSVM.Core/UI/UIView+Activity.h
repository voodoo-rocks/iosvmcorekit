//
//  UIView+Activity.h
//  iOSVM.Core
//
//  Created by Alexander Kryshtalev on 25/09/14.
//  Copyright (c) 2014 Voodoo Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VMBlocks.h"

@interface ActivityHandler : NSObject

- (void)dismiss;

@end

typedef void(^ActivityBlock)(ActivityHandler *handler);

@interface UIView (Activity)

- (void)showProgressWhile:(ActivityBlock)block;

@end
