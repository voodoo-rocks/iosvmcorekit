//
//  VMCoolPopupManager.h
//  WhereItsLive
//
//  Created by Alexander Kryshtalev on 29/04/14.
//  Copyright (c) 2014 Voodoo Mobile. All rights reserved.
//

#import "VMSharedManager.h"
#import "VMPopupOptions.h"
#import "VMBlocks.h"

@interface VMPopupManager : VMSharedManager
{
	UIViewController *activeController, *parentController;
	UIView *activeView;
}

@property (retain) VMPopupOptions *options;

@property (copy) VMVoidBlock wasPresented;
@property (copy) VMVoidBlock wasDismissed;

- (void)popupViewController:(UIViewController *)controller withOptions:(VMPopupOptions *)options;
- (void)popupViewController:(UIViewController *)controller;
- (void)dismissAnimated:(BOOL)animated;

@end
