//
//  VMAlertView.h
//  Ovenbot
//
//  Created by Olesya Kondrashkina on 11/14/13.
//  Copyright (c) 2013 Voodoo Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VMActionButton.h"

@interface VMAlertView : UIAlertView
{
    NSMutableArray *buttons;
}

- (id)initWithTitle:(NSString *)title message:(NSString *)message cancelButton:(VMActionButton *)cancelButton;
- (id)initWithTitle:(NSString *)title message:(NSString *)message cancelButton:(VMActionButton *)cancelButton otherButtons:(VMActionButton *)otherButtons, ... NS_REQUIRES_NIL_TERMINATION;

- (NSInteger)addButton:(VMActionButton *)button;

@end
