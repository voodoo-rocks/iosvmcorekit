//
//  VMPagesViewController.h
//  NueTune
//
//  Created by Alexander Kryshtalev on 16.10.13.
//  Copyright (c) 2013 Voodoo Mobile. All rights reserved.
//

#import "VMBaseViewController.h"

typedef void(^PageChanged)(NSInteger index, UIViewController *selectedController);

@interface VMPagesViewController : VMBaseViewController <UIPageViewControllerDataSource, UIPageViewControllerDelegate>
{
	UIPageViewController *pageViewController;
	PageChanged pageChangedBlock;
}

- (void)subscribeForPageChanged:(PageChanged)block;

@property (nonatomic, copy) IBOutletCollection(UIViewController) NSArray *controllers;

@property (nonatomic, retain) IBOutlet UIPageControl *pageControl;

@property (nonatomic) NSInteger selectedPageIndex;
@property BOOL animated;
@property BOOL infiniteLoop;

@property UIPageViewControllerNavigationOrientation navigationOrientation;

@end
