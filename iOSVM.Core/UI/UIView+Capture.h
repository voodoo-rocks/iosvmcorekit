//
//  UIView+Capture.h
//  iOSVM.Core
//
//  Created by Alexander Kryshtalev on 29/08/14.
//  Copyright (c) 2014 Voodoo Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Capture)

- (UIImage *)capture;

@end
