//
//  VMPagesViewController.m
//  NueTune
//
//  Created by Alexander Kryshtalev on 16.10.13.
//  Copyright (c) 2013 Voodoo Mobile. All rights reserved.
//

#import "VMPagesViewController.h"

@interface VMPagesViewController ()

@end

@implementation VMPagesViewController

@synthesize controllers;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
		// self.controllers = [[NSArray alloc] init];
    }
    return self;
}

- (void)subscribeForPageChanged:(PageChanged)block;
{
	pageChangedBlock = block;
}

- (void)setControllers:(NSArray *)_controllers
{
	controllers = _controllers;
    if (self.pageControl) {
        self.pageControl.numberOfPages = _controllers.count;
    }
}

- (NSArray *)controllers
{
	return controllers;
}

- (void)setPageControl:(UIPageControl *)pageControl
{
    _pageControl = pageControl;
    _pageControl.numberOfPages = self.controllers.count;
}

- (NSInteger)selectedPageIndex
{
	if (!pageViewController.viewControllers.count) {
		return NSNotFound;
	}

	UIViewController *controller = [pageViewController.viewControllers objectAtIndex:0];
	return [self.controllers indexOfObject:controller];
}

- (void)setSelectedPageIndex:(NSInteger)selectedPageIndex
{
	UIViewController *controller = [self.controllers objectAtIndex:selectedPageIndex];
	
	NSInteger currentIndex = self.selectedPageIndex;
	UIPageViewControllerNavigationDirection direction = UIPageViewControllerNavigationDirectionForward;
	
	// Calculating the direction. It is very important for the first and last controllers
	if (currentIndex != NSNotFound) {
		direction = selectedPageIndex > self.selectedPageIndex ?
		UIPageViewControllerNavigationDirectionForward : UIPageViewControllerNavigationDirectionReverse;
	}
	
	__weak VMPagesViewController *weakSelf = self;
	[pageViewController setViewControllers:@[controller] direction:direction animated:weakSelf.animated completion:^(BOOL finished) {
		
	}];
	
	if (pageChangedBlock) {
		pageChangedBlock(selectedPageIndex, controller);
	}
    
    if (self.pageControl) {
        self.pageControl.currentPage = selectedPageIndex;
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
	
	pageViewController = [[UIPageViewController alloc]initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll navigationOrientation:self.navigationOrientation options:@{ }];
	
	pageViewController.view.frame = self.view.bounds;
	[self.view addSubview:pageViewController.view];
	
	pageViewController.delegate = self;
	pageViewController.dataSource = self;
}

- (void)pageViewController:(UIPageViewController *)_pageViewController didFinishAnimating:(BOOL)finished previousViewControllers:(NSArray *)previousViewControllers transitionCompleted:(BOOL)completed;
{
    if (completed) {
        if (self.pageControl) {
            self.pageControl.currentPage = self.selectedPageIndex;
        }
        if (pageChangedBlock) {
            UIViewController *controller = [pageViewController.viewControllers lastObject];
            pageChangedBlock([controllers indexOfObject:controller], controller);
        }
    }
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController;
{
	NSInteger index = [self.controllers indexOfObject:viewController];
	if (index != NSNotFound && index - 1 >= 0) {
		return [self.controllers objectAtIndex:index - 1];
    } else if(self.infiniteLoop) {
        return [self.controllers objectAtIndex:self.controllers.count - 1];
    }
	
	return nil;
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController;
{
	NSInteger index = [self.controllers indexOfObject:viewController];
	if (index != NSNotFound && index + 1 < self.controllers.count) {
		return [self.controllers objectAtIndex:index + 1];
    } else if(self.infiniteLoop) {
        return [self.controllers objectAtIndex:0];
    }
	
	return nil;
}

@end
