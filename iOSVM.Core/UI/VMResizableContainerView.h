//
//  ResizableContainerView.h
//  FriendZone
//
//  Created by Olesya Kondrashkina on 22/03/16.
//  Copyright © 2016 Alexander Kryshtalev. All rights reserved.
//

#import <UIKit/UIKit.h>

//Use this class for Container View on Storyboard
//It will automatically resize to fit content (and update when content size changed)

@interface VMResizableContainerView : UIView

@end
