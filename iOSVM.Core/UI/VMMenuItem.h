//
//  VMSideMenuItem.h
//  Ovenbot
//
//  Created by Alexander Kryshtalev on 10.10.13.
//  Copyright (c) 2013 Voodoo Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VMBlocks.h"

@interface VMMenuItem : NSObject

- (id)initWithTitle:(NSString *)title forController:(UIViewController *)controller;
- (id)initWithTitle:(NSString *)title andIconName:(NSString *)icon andSelectedIconName:(NSString *)icon forController:(UIViewController *)controller;
- (id)initWithTitle:(NSString *)title andIconName:(NSString *)iconName andSelectedIconName:(NSString *)selectedIconName;
- (id)initWithTitle:(NSString *)title andIconName:(NSString *)icon forController:(UIViewController *)controller;
- (id)initWithTitle:(NSString *)title andIconName:(NSString *)icon;
- (id)initWithTitle:(NSString *)title;

@property (copy) VMVoidBlock didSelectMenuItem;
@property (strong, nonatomic) IBInspectable NSString *title;
@property (strong, nonatomic) IBInspectable NSString *iconName;
@property (strong, nonatomic) IBInspectable NSString *selectedIconName;
@property (strong, nonatomic) IBOutlet UIViewController *controller;

@end
