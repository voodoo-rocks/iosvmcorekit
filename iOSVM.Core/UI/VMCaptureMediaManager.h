//
//  VMTakePhotoManager.h
//  Cash-a-Lot
//
//  Created by Alexander Kryshtalev on 26/01/14.
//  Copyright (c) 2014 Voodoo Mobile. All rights reserved.
//

#import "VMSharedManager.h"
#import "VMBlocks.h"

enum VMMediaType {
    VMMediaTypePhoto,
    VMMediaTypeVideo
};

typedef void(^TakePhotoBlock)(NSData *data);
typedef void(^SelectSourceBlock)(VMVoidBlock fromGallery, VMVoidBlock fromCamera, VMVoidBlock cancelBlock);

@interface VMCaptureMediaManager : VMSharedManager <UINavigationControllerDelegate, UIImagePickerControllerDelegate, UIViewControllerTransitioningDelegate>
{
	TakePhotoBlock photoBlock;
}

- (void)takePhotoWithBlock:(TakePhotoBlock)block usingViewController:(UIViewController *)viewController;
- (void)capturePhotoOnView:(UIView *)view withBlock:(TakePhotoBlock)block usingViewController:(UIViewController *)viewController;

@property (copy) SelectSourceBlock selectSourceBlock;
@property (copy) UIColor *tintColor;

@property enum VMMediaType mediaType;

@end
