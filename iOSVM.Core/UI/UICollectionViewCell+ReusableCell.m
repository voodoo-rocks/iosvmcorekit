//
//  VMCollectionViewCell.m
//  Qtrank
//
//  Created by Olesya Kondrashkina on 3/20/14.
//  Copyright (c) 2014 Voodoo Mobile. All rights reserved.
//

#import "UICollectionViewCell+ReusableCell.h"

@implementation UICollectionViewCell (ReusableCell)

+ (instancetype)cellForCollectionView:(UICollectionView *)collectionView forIndexPath:(NSIndexPath *)indexPath;
{
	NSString *reuseIdentifier = NSStringFromClass(self.class);
	[collectionView registerNib:[UINib nibWithNibName:reuseIdentifier bundle:nil] forCellWithReuseIdentifier:reuseIdentifier];
	
	UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
	return cell;
}

+ (instancetype)cellForCollectionView:(UICollectionView *)collectionView fromNib:(NSString *)nibName forIndexPath:(NSIndexPath *)indexPath;
{
    [collectionView registerNib:[UINib nibWithNibName:nibName bundle:nil] forCellWithReuseIdentifier:nibName];
    
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:nibName forIndexPath:indexPath];
    return cell;
}

@end
