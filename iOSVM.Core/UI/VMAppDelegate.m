//
//  VMAppDelegate.m
//  Cash-a-Lot
//
//  Created by Alexander Kryshtalev on 20/01/14.
//  Copyright (c) 2014 Voodoo Mobile. All rights reserved.
//

#import "VMAppDelegate.h"
#import "VMPushManager.h"
#import "VMFlashManager.h"
#import "VMInteractivePush.h"

@implementation VMAppDelegate

+ (instancetype)sharedDelegate;
{
	return (VMAppDelegate *)[[UIApplication sharedApplication] delegate];
}

- (id)init
{
	self = [super init];
	
	self.window = [UIWindow new];
	[self.window makeKeyAndVisible];
	self.window.frame = [[UIScreen mainScreen] bounds];
	
	return self;
}

- (UINavigationController *)loadViewController:(UIViewController *)controller withNavigationClass:(Class)navClass;
{
	navigationController = [[UINavigationController alloc] initWithNavigationBarClass:navClass toolbarClass:nil];
	navigationController.viewControllers = @[controller];
	self.window.rootViewController = navigationController;
	
	return navigationController;
}

- (UINavigationController *)loadViewController:(UIViewController *)controller asRoot:(BOOL)asRoot;
{
	navigationController = [UINavigationController new];
	navigationController.viewControllers = @[controller];
	
	if (asRoot) {
		self.window.rootViewController = navigationController;
	}
	
	return navigationController;
}

- (void)setLightStatusBar:(BOOL)lightStatusBar
{
	[UIApplication sharedApplication].statusBarStyle = lightStatusBar ?
						   UIStatusBarStyleLightContent : UIStatusBarStyleDefault;
}

- (BOOL)lightStatusBar
{
	return [UIApplication sharedApplication].statusBarStyle == UIStatusBarStyleLightContent;
}

- (void)registerPushesWithCallback:(VMPushCompletionBlock)callback;
{
	pushCallback = callback;
	
    [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
    [[UIApplication sharedApplication] registerForRemoteNotifications];
}

- (void)enableInteractivePushes:(NSArray *)pushes forCategory:(NSString *)categoryIdentifier
{
	if (!interactivePushes) {
		interactivePushes = [NSMutableDictionary new];
	}
	
	NSMutableArray *actions = [NSMutableArray new];
	for (VMInteractivePush *interactivePush in pushes) {
		UIMutableUserNotificationAction *action = [UIMutableUserNotificationAction new];
		
		action.identifier = interactivePush.identifier;
		action.title = interactivePush.title;
		
		action.activationMode = interactivePush.usesUI ?
		UIUserNotificationActivationModeForeground :
		UIUserNotificationActivationModeBackground;
		
		action.destructive = interactivePush.isDestructive;
		action.authenticationRequired = NO;
		
		[actions addObject:action];
		
		[interactivePushes setObject:interactivePush forKey:interactivePush.identifier];
	}
 
	UIMutableUserNotificationCategory *category = [UIMutableUserNotificationCategory new];
	category.identifier = categoryIdentifier;
	[category setActions:actions forContext:UIUserNotificationActionContextDefault];
	[category setActions:actions forContext:UIUserNotificationActionContextMinimal];
	
	NSSet *categories = [NSSet setWithObjects:category, nil];
	
	UIUserNotificationSettings *settings =
	[UIUserNotificationSettings settingsForTypes:
	 UIUserNotificationTypeAlert |
	 UIUserNotificationTypeSound |
	 UIUserNotificationTypeBadge categories:categories];
	
	[[UIApplication sharedApplication] registerUserNotificationSettings:settings];
}

- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken{
	NSLog(@"deviceToken: %@", deviceToken);
	if (pushCallback) {
		pushCallback(YES, [[VMPushManager sharedManager] stringFromDeviceToken:deviceToken], nil);
	}
}

- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error{
	NSLog(@"Failed to register with error : %@", error);
	if (pushCallback) {
		pushCallback(NO, nil, error);
	}
}

- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification {
    NSString *message = notification.alertBody;
    
    if (self.didReceiveLocalPush) {
        self.didReceiveLocalPush(self, message, notification.userInfo);
    } else {
        [[VMFlashManager sharedManager] showFlash:message];
    }
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
	NSDictionary *aps = userInfo[@"aps"];
	NSString *message = aps[@"alert"];
	
	if (self.didReceivePush) {
		self.didReceivePush(self, message, userInfo);
	} else {
		[[VMFlashManager sharedManager] showFlash:message];
	}
}

- (void)application:(UIApplication *)application handleActionWithIdentifier:(NSString *)identifier forRemoteNotification:(NSDictionary *) notification completionHandler: (void (^)()) completionHandler {
	
	if (![interactivePushes.allKeys containsObject:identifier]) {
		NSLog(@"Could not find any push with '%@' identifier", identifier);
	}

	VMInteractivePush *push = [interactivePushes objectForKey:identifier];
	if (push.didPerformAction) {
		push.didPerformAction(self, push);
	}
	
	completionHandler();
}

@end
