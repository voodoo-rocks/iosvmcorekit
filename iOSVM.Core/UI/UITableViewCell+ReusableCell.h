//
//  VMTableViewCell.h
//  Foodomart
//
//  Created by Alexander Kryshtalev on 27/01/14.
//  Copyright (c) 2014 Alexei Pozdnyakov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITableViewCell (ReusableCell)

+ (instancetype)cellForTableView:(UITableView *)tableView;
+ (instancetype)cellForTableView:(UITableView *)tableView forIndexPath:(NSIndexPath *)indexPath;

+ (instancetype)cellForTableView:(UITableView *)tableView fromNib:(NSString *)nibName;
+ (instancetype)cellForTableView:(UITableView *)tableView forIndexPath:(NSIndexPath *)indexPath fromNib:(NSString *)nibName;

@property (nonatomic, readonly) CGFloat height;

- (CGFloat)layoutHeightForTableView:(UITableView *)tableView;

@end
