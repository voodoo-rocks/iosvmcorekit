//
//  VMTabsSwitcher.h
//  iOSVM.Core
//
//  Created by Alexander Kryshtalev on 07/07/14.
//  Copyright (c) 2014 Voodoo Mobile. All rights reserved.
//

#import "VMButtonsSwitcher.h"
#import "VMPlaceholderView.h"

@interface VMTabsSwitcher : VMButtonsSwitcher
{
	UIViewController *selectedController;
}

@property (strong, nonatomic) IBOutlet VMPlaceholderView *contentView;
@property (strong, nonatomic) IBOutletCollection(UIViewController) NSArray *controllers;

@property BOOL animatedTransitions;

@end
