//
//  UIImagePickerController+NonRotating.m
//  Watch Dog
//
//  Created by Alice on 11/08/15.
//  Copyright (c) 2015 Voodoo-Mobile. All rights reserved.
//

#import "UIImagePickerController+NonRotating.h"

@implementation UIImagePickerController (NonRotating)

- (BOOL)shouldAutorotate {
    return NO;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return UIInterfaceOrientationPortrait;
}

@end
