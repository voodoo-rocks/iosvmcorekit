//
//  VMButtonsSwitcher.m
//  iOSVM.Core
//
//  Created by Alexander Kryshtalev on 18/06/14.
//  Copyright (c) 2014 Voodoo Mobile. All rights reserved.
//

#import "VMButtonsSwitcher.h"

@implementation VMButtonsSwitcher

@synthesize selectedIndex;
@synthesize didChangeIndex;
@synthesize buttons;

- (id)init
{
	self = [super init];
	if (self) {
		selectedIndex = -1;
	}
	return self;
}

- (id)initWithButtons:(NSArray *)_buttons;
{
	self = [self init];
	
	if (self) {
		buttons = _buttons;
	}
	
	return self;
}

- (void)setButtons:(NSArray *)_buttons
{
	buttons = _buttons;
	
	for (UIButton *button in buttons) {
		[button addTarget:self action:@selector(onButton:) forControlEvents:UIControlEventTouchUpInside];
	}
}

- (NSArray *)buttons
{
	return buttons;
}

- (void)onButton:(id)sender
{
    NSInteger buttonIndex = [buttons indexOfObject:sender];
    if(self.didPressButtonAtIndex) {
        self.didPressButtonAtIndex(buttonIndex);
    }
    if (self.canDeselectButton && buttonIndex == self.selectedIndex) {
        self.selectedIndex = -1;
    }
    else {
        self.selectedIndex = buttonIndex;
    }
}

- (NSUInteger)getSelectedIndex
{
	return selectedIndex;
}

- (void)setSelectedIndex:(NSUInteger)index
{
	if (selectedIndex != index) {
		if (selectedIndex != -1) {
			[[buttons objectAtIndex:selectedIndex] setSelected:NO];
		} else {
			for (UIButton *button in buttons) {
				button.selected = NO;
			}
		}
		
		selectedIndex = index;
        if (selectedIndex != -1) {
            [[buttons objectAtIndex:selectedIndex] setSelected:YES];
        }
		
		if (self.didChangeIndex) {
			self.didChangeIndex(selectedIndex);
		}
	}
}

@end
