//
//  UI.h
//  NueTune
//
//  Created by Alexander Kryshtalev on 06.11.13.
//  Copyright (c) 2013 Voodoo Mobile. All rights reserved.
//

#import "VMAlertManager.h"
#import "VMAlertView.h"
#import "VMActionButton.h"
#import "VMBarButtonItem.h"
#import "VMCheckButton.h"
#import "VMMenuItem.h" 
#import "VMMenuTableView.h"
#import "VMNavigationBar.h"
#import "VMPopupManager.h"
#import "VMButtonsSwitcher.h"
#import "VMTabsSwitcher.h"
#import "VMFlashManager.h"
#import "UIView+Activity.h"
#import "UIView+Capture.h"
#import "MKMapView+ZoomLevel.h"
#import "UITableViewCell+ReusableCell.h"
#import "UICollectionViewCell+ReusableCell.h"

#pragma mark Views

#import "VMPlaceholderView.h"
#import "VMActionSheet.h"
#import "VMResizableTableView.h"
#import "VMResizableCollectionView.h"
#import "VMResizableContainerView.h"

#pragma mark Controllers

#import "VMBaseViewController.h"
#import "VMPagesViewController.h"

#pragma mark Custom Controls

#import "VMTextField.h"
#import "VMTextView.h"

#pragma mark Utils

#import "VMCaptureMediaManager.h"
#import "VMAppDelegate.h"
#import "VMPhotoPicker.h"

#pragma mark Validators

#import "VMValidationGroup.h"
#import "VMValidationForm.h"
#import "VMRequiredValidator.h"
#import "VMEmailValidator.h"
#import "VMBaseValidator.h"
#import "VMValidationDelegate.h"
#import "VMValidatable.h"
