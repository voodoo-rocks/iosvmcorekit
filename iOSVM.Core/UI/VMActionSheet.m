//
//  VMActionSheet.m
//  Ovenbot
//
//  Created by Olesya Kondrashkina on 11/15/13.
//  Copyright (c) 2013 Voodoo Mobile. All rights reserved.
//

#import "VMActionSheet.h"

@implementation VMActionSheet

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (NSInteger)addButton:(VMActionButton *)button
{
    [buttons addObject:button];
    return [super addButtonWithTitle:NSLocalizedString(button.title, nil)];
}

- (id)initWithTitle:(NSString *)title cancelButton:(VMActionButton *)cancelButton destructiveButton:(VMActionButton *)destructiveButton otherButtonsAsArray:(NSArray *)otherButtons
{
	self = [super initWithTitle:NSLocalizedString(title, nil) delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:nil];
    buttons = [NSMutableArray array];
    if (destructiveButton) {
        NSInteger destructiveIndex = [self addButton:destructiveButton];
        self.destructiveButtonIndex = destructiveIndex;
    }
    if (otherButtons) {
        for (VMActionButton *alertButton in otherButtons) {
			[self addButton:alertButton];
		}
    }
    if (cancelButton) {
        NSInteger cancelIndex = [self addButton:cancelButton];
        self.cancelButtonIndex = cancelIndex;
    }
    return self;
}

- (id)initWithTitle:(NSString *)title cancelButton:(VMActionButton *)cancelButton destructiveButton:(VMActionButton *)destructiveButton otherButtons:(VMActionButton *)otherButtons, ...
{
    self = [super initWithTitle:NSLocalizedString(title, nil) delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:nil];
    buttons = [NSMutableArray array];
    if (destructiveButton) {
        NSInteger destructiveIndex = [self addButton:destructiveButton];
        self.destructiveButtonIndex = destructiveIndex;
    }
    if (otherButtons) {
        VMActionButton *button = otherButtons;
        va_list list;
        va_start(list, otherButtons);
        do {
            [self addButton:button];
        } while ((button = va_arg(list, VMActionButton *)));
        va_end(list);
    }
    if (cancelButton) {
        NSInteger cancelIndex = [self addButton:cancelButton];
        self.cancelButtonIndex = cancelIndex;
    }
    return self;
}

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex;
{
    if (buttonIndex >= 0) {
        VMActionButton *button = [buttons objectAtIndex:buttonIndex];
        if (button.actionBlock) {
            button.actionBlock();
        }
    }
}

- (void)show
{
    [self showInView:[UIApplication sharedApplication].keyWindow];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
