//
//  ResizableContainerView.m
//  FriendZone
//
//  Created by Olesya Kondrashkina on 22/03/16.
//  Copyright © 2016 Alexander Kryshtalev. All rights reserved.
//

#import "VMResizableContainerView.h"

@implementation VMResizableContainerView

- (void)addSubview:(UIView *)subview {
    subview.translatesAutoresizingMaskIntoConstraints = NO;
    
    [super addSubview:subview];

    NSDictionary *views = NSDictionaryOfVariableBindings(subview);
    [self addConstraints:[NSLayoutConstraint
                          constraintsWithVisualFormat:@"H:|[subview]|"
                          options:0 metrics:nil views:views]];
    [self addConstraints:[NSLayoutConstraint
                          constraintsWithVisualFormat:@"V:|[subview]|"
                          options:0 metrics:nil views:views]];
}

@end
