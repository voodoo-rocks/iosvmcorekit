//
//  VMBarButtonItem.m
//  NueTune
//
//  Created by Alexander Kryshtalev on 16.10.13.
//  Copyright (c) 2013 Voodoo Mobile. All rights reserved.
//

#import "VMBarButtonItem.h"

@implementation VMBarButtonItem

+ (instancetype)barButtonItemWithImage:(UIImage *)image forTarget:(id)target withAction:(SEL)action;
{
	return [VMBarButtonItem barButtonItemWithImage:image withSelectedImage:image forTarget:target withAction:action];
}

+ (instancetype)barButtonItemWithImage:(UIImage *)image withCallback:(BarButtonItemCallback)_callback;
{
	VMBarButtonItem *item = [VMBarButtonItem barButtonItemWithImage:image forTarget:nil withAction:nil];
	UIButton *button = (UIButton *)item.customView;
	[button addTarget:item action:@selector(onClick:) forControlEvents:UIControlEventTouchUpInside];
	item->callback = _callback;
	return item;
}

+ (instancetype)barButtonItemWithImage:(UIImage *)image withSelectedImage:(UIImage *)selectedImage andTitle:(NSString *)title forTarget:(id)target withAction:(SEL)action;
{
	UIButton *button = [UIButton buttonWithType: UIButtonTypeCustom];
	
	[button setTitle:title forState:UIControlStateNormal];
	[button setImage:image forState:UIControlStateNormal];
	[button setImage:selectedImage forState:UIControlStateSelected];
	
	[button addTarget:target action:action forControlEvents: UIControlEventTouchUpInside];
	[button sizeToFit];
	
	return [[VMBarButtonItem alloc]initWithCustomView:button];
}

+ (instancetype)barButtonItemWithImage:(UIImage *)image withSelectedImage:(UIImage *)selectedImage forTarget:(id)target withAction:(SEL)action;
{
	return [VMBarButtonItem barButtonItemWithImage:image withSelectedImage:selectedImage andTitle:nil forTarget:target withAction:action];
}

+ (instancetype)barButtonItemWithImage:(UIImage *)image andTitle:(NSString *)title forTarget:(id)target withAction:(SEL)action;
{
	return [VMBarButtonItem barButtonItemWithImage:image withSelectedImage:nil andTitle:title forTarget:target withAction:action];
}

+ (instancetype)barButtonItemWithTitle:(NSString *)title forTarget:(id)target withAction:(SEL)action;
{
	return [[VMBarButtonItem alloc]initWithTitle:title style:UIBarButtonItemStylePlain target:target action:action];
}

+ (instancetype)barButtonItemWithTitle:(NSString *)title withCallback:(BarButtonItemCallback)_callback;
{
	VMBarButtonItem *item = [[VMBarButtonItem alloc]initWithTitle:title style:UIBarButtonItemStylePlain target:self action:@selector(onClick:)];
	
	item.target = item;
	item->callback = _callback;
	return item;
}

- (void)onClick:(id)sender
{
	if (callback) {
		callback(sender);
	}
}


@end
