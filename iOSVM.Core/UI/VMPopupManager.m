//
//  VMCoolPopupManager.m
//  WhereItsLive
//
//  Created by Alexander Kryshtalev on 29/04/14.
//  Copyright (c) 2014 Voodoo Mobile. All rights reserved.
//

#import "VMPopupManager.h"

@implementation VMPopupManager

- (id)init
{
	self = [super init];
	self.options = VMPopupOptions.defaultOptions;
	return self;
}

- (void)popupViewController:(UIViewController *)controller withOptions:(VMPopupOptions *)options;
{
	UIView *topmost = self.topmostView;
	NSAssert(topmost != nil, @"There is no view that can be used for showing your UI");
	
	activeController = controller;
	activeView = activeController.view;
	
	if (options.enableDimmedBackground) {

	}
	
	activeView.alpha = 0;
	
    CGFloat popupViewWidth = MIN(controller.view.frame.size.width, topmost.frame.size.width);
    CGFloat popupViewHeight = MIN(controller.view.frame.size.height, topmost.frame.size.height);

	controller.view.frame = CGRectMake(0, 0, popupViewWidth, popupViewHeight);
    
    controller.view.frame = [self centerRect:controller.view.frame intoRect:topmost.frame];
	[topmost addSubview:controller.view];
	
	// TODO: to options
	[UIView animateWithDuration:self.options.animationDuration animations:^{
		activeView.alpha = 1;
	} completion:^(BOOL finished) {
		if (self.wasPresented) {
			self.wasPresented();
		}
	}];
}

- (void)dismissAnimated:(BOOL)animated;
{
	if (activeController) {
		[activeController dismissViewControllerAnimated:animated completion:nil];
	}
	
	[UIView animateWithDuration:self.options.animationDuration animations:^{
		
		if (activeView) {
			activeView.alpha = 0;
		}
		
	} completion:^(BOOL finished) {
		
		if (activeView) {
			[activeView removeFromSuperview];
		}
		
		if (self.wasDismissed) {
			self.wasDismissed();
		}
	}];
}

- (void)popupViewController:(UIViewController *)controller;
{
	[self popupViewController:controller withOptions:self.options];
}

- (UIView *)topmostView
{
	parentController = [[UIApplication sharedApplication] keyWindow].rootViewController;
	
	if (!parentController) {
		parentController = [[UIViewController alloc] init];
		parentController.view.frame = [UIScreen mainScreen].bounds;
		parentController.view.backgroundColor = [UIColor clearColor];
	}
	
	[[UIApplication sharedApplication] keyWindow].rootViewController = parentController;
	return parentController.view;
}

- (CGRect)centerRect:(CGRect)rect intoRect:(CGRect)outerRect;
{
    CGPoint newCenter = CGPointMake(outerRect.origin.x + outerRect.size.width / 2, outerRect.origin.y + outerRect.size.height / 2);
    CGPoint newOrigin = CGPointMake(newCenter.x - rect.size.width / 2, newCenter.y - rect.size.height / 2);
    CGRect newRect = CGRectMake(newOrigin.x, newOrigin.y, rect.size.width, rect.size.height);
    return newRect;
}


@end
