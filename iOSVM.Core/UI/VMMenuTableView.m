//
//  VMMenuTableView.m
//  Ovenbot
//
//  Created by Alexander Kryshtalev on 17.10.13.
//  Copyright (c) 2013 Voodoo Mobile. All rights reserved.
//

#import "VMMenuTableView.h"

@implementation VMMenuTableView

@synthesize menuItems;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
	}
    return self;
}

- (void)awakeFromNib
{
	[super awakeFromNib];
	
	if (!self.delegate) {
		self.delegate = self;
	}
	
	if (!self.dataSource) {
		self.dataSource = self;
	}
}

- (void)reloadMenuItems;
{
	[self reloadData];
}

- (void)subscribeForMenuItemTap:(MenuItemTapBlock)block;
{
	tapBlock = block;
}

- (void)setMenuItems:(NSArray *)_menuItems
{
	menuItems = _menuItems;
	[self reloadData];
}

- (NSArray *)menuItems
{
	return menuItems;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
	return self.menuItems.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
{
	VMMenuItem *item = [self.menuItems objectAtIndex:indexPath.row];
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"VMSideMenuViewCell"];
	if (cell == nil) {
		cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"VMSideMenuViewCell"];
		cell.backgroundColor = [UIColor clearColor];
		cell.textLabel.text = item.title;
		cell.imageView.image = [UIImage imageNamed:item.iconName];
	};
	
	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	VMMenuItem *item = [self.menuItems objectAtIndex:indexPath.row];
	if (tapBlock) {
		tapBlock(indexPath.row, item);
	}
}

@end
