//
//  VMCheckButton.m
//  NDA
//
//  Created by Alexander Kryshtalev on 18.01.13.
//  Copyright (c) 2013 Voodoo Mobile. All rights reserved.
//

#import "VMCheckButton.h"

@implementation VMCheckButton

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
		
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
	[self addTarget:self action:@selector(onTap:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)onTap:(UIButton *)sender
{
	sender.selected = !sender.selected;
    if(self.didToggle) {
        self.didToggle(sender.selected);
    }
}

@end
