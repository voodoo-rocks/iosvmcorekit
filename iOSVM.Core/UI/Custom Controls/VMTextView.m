//
//  VMTextView.m
//  iBeacon Demo
//
//  Created by Alexander Kryshtalev on 09/01/14.
//  Copyright (c) 2014 Voodoo Mobile. All rights reserved.
//

#import "VMTextView.h"

@implementation VMTextView

@synthesize isLocalized, attributedComponents;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.delegate = self;
    }
    return self;
}

- (id)init
{
    self = [super init];
    
    self.isLocalized = YES;
    
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    
    self.isLocalized = YES;
    
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    if (!self.delegate) {
        self.delegate = self;
    }
    if (self.placeholder) {
        [self createPlaceholderLabel];
    }
    if (isLocalized) {
        self.text = NSLocalizedString(self.text, nil);
    }
}

- (void)setText:(NSString *)text
{
    if (self.isLocalized) {
        [super setText:NSLocalizedString(text, nil)];
    }
    else {
        [super setText:text];
    }
    [self showPlaceholderIfNeeded];
}

- (void)showPlaceholderIfNeeded
{
    if (!placeholderLabel) {
        return;
    }
    if(!self.hasText) {
        [self addSubview:placeholderLabel];
    } else if ([self.subviews containsObject:placeholderLabel]) {
        [placeholderLabel removeFromSuperview];
    }
}

- (void)setCustomFont:(NSString *)customFont
{
    CGFloat currentSize = self.font.pointSize;
    self.font = [UIFont fontWithName:customFont size:currentSize];
}

- (NSString *)customFont
{
    return self.font.fontName;
}

- (void)textViewDidChange:(UITextView *)textView
{
    [self showPlaceholderIfNeeded];
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    [self showPlaceholderIfNeeded];
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text;
{
    NSString *result = [NSString stringWithFormat:@"%@%@", textView.text, text];
    
    if(self.textLengthLimit && result.length > self.textLengthLimit.intValue) {
        return NO;
    }
    
    return YES;
}

- (BOOL)isAcceptableTextLength:(NSUInteger)length {
    return self.textLengthLimit.intValue > 0 ? length <= self.textLengthLimit.intValue : YES;
}

- (void)createPlaceholderLabel
{
    placeholderLabel = [[UILabel alloc] initWithFrame:CGRectMake(5.0, 5.0, self.frame.size.width - 20.0, 34.0)];
    placeholderLabel.numberOfLines = 0;
    [placeholderLabel setText:self.placeholder];
    [placeholderLabel sizeToFit];
    
    placeholderLabel.backgroundColor = [UIColor clearColor];
    placeholderLabel.font = self.font;
    placeholderLabel.textColor = [UIColor lightGrayColor];
    
    [self addSubview:placeholderLabel];
}

- (CGSize)bestSizePreservingWidth;
{
    CGSize size = [self sizeThatFits:CGSizeMake(self.frame.size.width, MAXFLOAT)];
    return CGSizeMake(self.frame.size.width, size.height);
}

- (void)updateAttributedText
{
    NSMutableAttributedString *componentsString = [[NSMutableAttributedString alloc] initWithString:@""];
    
    for (VMAttributedTextComponent *component in self.attributedComponents) {
        if (!component.textColor) {
            component.textColor = self.textColor;
        }
        if (!component.fontName) {
            component.fontName = self.font.fontName;
        }
        if (!component.fontSize) {
            component.fontSize = self.font.pointSize;
        }
        if (component.skipSpace || [component isEqual:self.attributedComponents.lastObject] || component.text.length == 0) {
            [componentsString appendAttributedString:component.attributedString];
        }
        else {
            [componentsString appendAttributedString:component.attributedStringWithSpace];
        }
        component.didModify = ^{
            
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"
            
            [self updateAttributedText];
            
#pragma clang diagnostic pop
        };
    }
    
    self.attributedText = componentsString;
}

- (void)setAttributedComponents:(NSArray *)_attributedComponents
{
    attributedComponents = _attributedComponents;
    
    [self updateAttributedText];
}

- (void)setTextComponents:(NSArray *)components
{
    NSAssert(components.count == attributedComponents.count, @"Different components count");
    
    for (int i = 0; i < attributedComponents.count; i++){
        VMAttributedTextComponent *component = [attributedComponents objectAtIndex:i];
        component.text = [components objectAtIndex:i];
    }
}

- (NSString *)textForValidation;
{
    return self.text;
}

- (void)focus;
{
    [self becomeFirstResponder];
}

@end
