//
//  VMFlashView.m
//  iOSVM.Core
//
//  Created by Alexander Kryshtalev on 23/07/14.
//  Copyright (c) 2014 Voodoo Mobile. All rights reserved.
//

#import "VMFlashView.h"
#import "VMConstants.h"

@implementation VMFlashView

const CGFloat kFlashViewButtonPadding = 10;

- (id)initWithFrame:(CGRect)frame
{
	self = [super initWithFrame:frame];
	if (self) {
		[self setUp];
	}
	return self;
}

- (id)initWithTitle:(NSString *)title;
{
	const int kDefaultHeight = 44;
	
	self = [self initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, kDefaultHeight)];
	
	if (self) {
		self.title = title;
	}
	
	return self;
}

- (void)hideAnimated:(BOOL)animated withDelay:(NSTimeInterval)delay;
{
	[NSTimer scheduledTimerWithTimeInterval:delay target:self selector:@selector(onHideTimer:) userInfo:[NSNumber numberWithBool:animated] repeats:NO];
}

- (void)onHideTimer:(NSTimer *)timer
{
	[self hideAnimated:[timer.userInfo boolValue]];
}

- (void)hideAnimated:(BOOL)animated;
{
	[UIView animateWithDuration:VMConstants.defaultAnimationDuration animations:^{
		self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, 0);
		closeButton.alpha = 0;
	}];
}

- (void)hide;
{
	[self hideAnimated:YES];
}

- (void)setUp
{
	self.backgroundColor = [UIColor blackColor];
	self.foregroundColor = [UIColor whiteColor];
	self.font = [UIFont systemFontOfSize:[UIFont systemFontSize]];
	
	closeButton = [UIButton new];
	[closeButton setTitle:@"✕" forState:UIControlStateNormal];
	
	closeButton.titleLabel.textColor = [UIColor whiteColor];
	[closeButton sizeToFit];
	
	closeButton.frame = CGRectMake(self.frame.size.width - kFlashViewButtonPadding - closeButton.frame.size.width,
								   (self.frame.size.height - closeButton.frame.size.height) / 2,
								   closeButton.frame.size.width, closeButton.frame.size.height);
	
	closeButton.autoresizingMask =
	UIViewAutoresizingFlexibleTopMargin |
	UIViewAutoresizingFlexibleLeftMargin |
	UIViewAutoresizingFlexibleWidth;
	
	[closeButton addTarget:self action:@selector(onClose) forControlEvents:UIControlEventTouchUpInside];
	[self addSubview:closeButton];
}

- (void)onClose
{
	[self hideAnimated:YES];
}

- (void)setBackgroundColor:(UIColor *)backgroundColor
{
	const CGFloat kDefaultAlpha = 0.75f;
	[super setBackgroundColor:[backgroundColor colorWithAlphaComponent:kDefaultAlpha]];
}

-(void)drawRect:(CGRect)rect
{
	[super drawRect:rect];
	
	[self drawString:self.title inRect:rect];
}

- (void)drawString:(NSString *)string inRect:(CGRect)contextRect
{
	string = [string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
	
	if (!string) {
		string = @"";
	}
	
	const CGFloat kLeftPadding = 15;
	NSDictionary *attributes = @{ NSForegroundColorAttributeName: self.foregroundColor,
								  NSFontAttributeName: self.font};
	
	// TODO: remove it
	CGFloat width = contextRect.size.width - kLeftPadding * 2 - kFlashViewButtonPadding * 2 - closeButton.frame.size.width;
	
	NSAttributedString *attributedText = [[NSAttributedString alloc]
										  initWithString:string attributes:attributes];
	
	CGRect bounding = [attributedText boundingRectWithSize:(CGSize) { width, CGFLOAT_MAX }
												   options:NSStringDrawingUsesLineFragmentOrigin
												   context:nil];
	
	CGFloat offset = (contextRect.size.height - bounding.size.height) / 2.0;
	CGRect textRect = CGRectMake(kLeftPadding, offset, width, bounding.size.height);
	
	[string drawInRect:textRect withAttributes:attributes];
}


@end
