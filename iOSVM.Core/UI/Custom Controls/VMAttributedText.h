//
//  VMAttributedText.h
//  Qtrank
//
//  Created by Olesya Kondrashkina on 3/24/14.
//  Copyright (c) 2014 Voodoo Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VMAttributedTextComponent.h"

@protocol VMAttributedText <NSObject>

@property (retain, nonatomic) IBOutletCollection(VMAttributedTextComponent) NSArray *attributedComponents;

- (void)setTextComponents:(NSArray *)components;

@end