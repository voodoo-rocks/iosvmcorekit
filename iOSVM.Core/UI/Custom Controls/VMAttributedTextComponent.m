//
//  VMAttributedStringComponent.m
//  Qtrank
//
//  Created by Olesya Kondrashkina on 3/24/14.
//  Copyright (c) 2014 Voodoo Mobile. All rights reserved.
//

#import "VMAttributedTextComponent.h"

@implementation VMAttributedTextComponent

@synthesize isLocalized;

- (id)init
{
    self = [super init];
    
    self.text = @"";
    
    self.isLocalized = YES;
    
    return self;
}

- (void)setText:(NSString *)text
{
    if (isLocalized) {
        _text = NSLocalizedString(text, nil);
    }
    else {
        _text = text;
    }
    
    if (self.didModify) {
        self.didModify ();
    }
}

- (void)setFontName:(NSString *)fontName
{
    _fontName = fontName;
    
    if (self.didModify) {
        self.didModify ();
    }
}

- (void)setFontSize:(CGFloat)fontSize
{
    _fontSize = fontSize;
    
    if (self.didModify) {
        self.didModify ();
    }
}

- (void)setFont:(UIFont *)font
{
    _fontSize = font.pointSize;
    _fontName = font.fontName;
    
    if (self.didModify) {
        self.didModify ();
    }
}

- (UIFont *)font
{
    return [UIFont fontWithName:self.fontName size:self.fontSize];
}

- (void)setTextColor:(UIColor *)textColor
{
    _textColor = textColor;
    
    if (self.didModify) {
        self.didModify ();
    }
}

- (NSAttributedString *)attributedString
{
    return [[NSAttributedString alloc] initWithString:self.text attributes:@{NSFontAttributeName: self.font, NSForegroundColorAttributeName: self.textColor}];
}

- (NSAttributedString *)attributedStringWithSpace
{
    return [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ ", self.text] attributes:@{NSFontAttributeName: self.font, NSForegroundColorAttributeName: self.textColor}];
}

@end
