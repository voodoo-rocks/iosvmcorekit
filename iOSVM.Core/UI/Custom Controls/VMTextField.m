//
//  VMTextField.m
//  DoingFine
//
//  Created by Alexander Kryshtalev on 13/01/14.
//  Copyright (c) 2014 Voodoo Mobile. All rights reserved.
//

#import "VMTextField.h"

@implementation VMTextField

@synthesize isLocalized, messageOnFail;

#define DEFAULT_INSET 8

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.isLocalized = YES;
    }
    return self;
}

- (id)init
{
    self = [super init];
    
    self.isLocalized = YES;
    
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    
    self.isLocalized = YES;
    
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    if (isLocalized) {
        self.placeholder = NSLocalizedString(self.placeholder, nil);
    }
}

- (void)setPlaceholder:(NSString *)placeholder
{
    if (isLocalized) {
        [super setPlaceholder:NSLocalizedString(placeholder, nil)];
    }
    else {
        [super setPlaceholder:placeholder];
    }
}

- (void)setCustomFont:(NSString *)customFont
{
	CGFloat currentSize = self.font.pointSize;
	self.font = [UIFont fontWithName:customFont size:currentSize];
}

- (NSString *)customFont
{
	return self.font.fontName;
}

- (void)setCustomPlaceholderFont:(NSString *)customPlaceholderFont
{
    _customPlaceholderFont = customPlaceholderFont;
    self.attributedPlaceholder = [[NSAttributedString alloc] initWithString:self.placeholder attributes:@{NSFontAttributeName : [UIFont fontWithName:customPlaceholderFont size:self.font.pointSize]}];
}

- (CGSize)bestSizePreservingWidth;
{
	return self.frame.size;
}

- (CGRect)textRectForBounds:(CGRect)bounds;
{
    return CGRectInset(bounds, self.textRectInset.floatValue, 0);
}

- (NSNumber *)textRectInset
{
    if (!_textRectInset) {
        return @(DEFAULT_INSET);
    }
    return _textRectInset;
}

- (CGRect)editingRectForBounds:(CGRect)bounds {
	return [self textRectForBounds:bounds];
}

- (NSString *)textForValidation;
{
	return self.text;
}

- (void)focus;
{
	[self becomeFirstResponder];
}

- (BOOL)shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string;
{
    if (!self.textLengthLimit) {
        return YES;
    }
    return (self.text.length - range.length + string.length <= self.textLengthLimit.integerValue || [string isEqualToString:@"\n"]);
}

@end
