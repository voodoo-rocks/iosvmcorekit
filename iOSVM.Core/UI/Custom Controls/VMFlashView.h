//
//  VMFlashView.h
//  iOSVM.Core
//
//  Created by Alexander Kryshtalev on 23/07/14.
//  Copyright (c) 2014 Voodoo Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VMFlashView : UIView
{
	UIButton *closeButton;
}

@property (copy) NSString *title;
@property (copy) UIFont *font;
@property (copy) UIColor *foregroundColor;

- (id)initWithTitle:(NSString *)title;

- (void)hideAnimated:(BOOL)animated withDelay:(NSTimeInterval)delay;
- (void)hideAnimated:(BOOL)animated;
- (void)hide;

@end
