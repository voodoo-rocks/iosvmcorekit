//
//  VMLoaderImageView.h
//  iOSVM.Core
//
//  Created by Alexander Kryshtalev on 27/06/14.
//  Copyright (c) 2014 Voodoo Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VMLoaderImageView : UIImageView

@end
