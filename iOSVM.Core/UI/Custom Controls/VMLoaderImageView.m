//
//  VMLoaderImageView.m
//  iOSVM.Core
//
//  Created by Alexander Kryshtalev on 27/06/14.
//  Copyright (c) 2014 Voodoo Mobile. All rights reserved.
//

#import "VMLoaderImageView.h"

@implementation VMLoaderImageView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
	[super awakeFromNib];
	
	[self runSpinAnimationWithDuration:5];
}

- (void)runSpinAnimationWithDuration:(CGFloat)duration
{
    CABasicAnimation* rotationAnimation;
    rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    rotationAnimation.toValue = [NSNumber numberWithFloat: -M_PI * 2.0 /* full rotation*/ ];
    rotationAnimation.duration = duration;
    rotationAnimation.cumulative = YES;
	
	// TODO: do something with it
    rotationAnimation.repeatCount = 1000;
	
    [self.layer addAnimation:rotationAnimation forKey:@"rotationAnimation"];
}


@end
