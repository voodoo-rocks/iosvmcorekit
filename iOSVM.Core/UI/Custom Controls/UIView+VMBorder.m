//
//  UIView+VMBorder.m
//  Foodomart
//
//  Created by Alexander Kryshtalev on 01/02/14.
//  Copyright (c) 2014 Alexei Pozdnyakov. All rights reserved.
//

#import "UIView+VMBorder.h"

@implementation UIView (VMBorder)

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.layer.borderWidth = self.borderWidth;
    self.layer.borderColor = [self.borderColor CGColor];
    self.layer.cornerRadius = self.viewCornerRadius;
    self.layer.masksToBounds = YES;
}

- (void)setBorderWidth:(float)width
{
    self.layer.borderWidth = width;
}

- (float)borderWidth
{
    return self.layer.borderWidth;
}

- (void)setBorderColor:(UIColor *)color
{
    self.layer.borderColor = [color CGColor];
}

- (UIColor *)borderColor
{
    return [UIColor colorWithCGColor:self.layer.borderColor];
}

- (void)setViewCornerRadius:(float)cornerRadius
{
    self.layer.cornerRadius = cornerRadius;
    self.layer.masksToBounds = YES;
}

- (float)viewCornerRadius
{
    return self.layer.cornerRadius;
}

@end
