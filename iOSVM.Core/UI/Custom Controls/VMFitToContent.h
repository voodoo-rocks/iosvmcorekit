//
//  VMFitToContent.h
//  Marktgevoel
//
//  Created by Alexander Kryshtalev on 07/01/14.
//  Copyright (c) 2014 Voodoo Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol VMFitToContent <NSObject>

@required
- (CGSize)bestSizePreservingWidth;

@optional
- (CGSize)bestSizePreservingHeight;

@end
