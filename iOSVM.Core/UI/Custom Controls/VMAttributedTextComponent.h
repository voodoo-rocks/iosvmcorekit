//
//  VMAttributedTextComponent.h
//  Qtrank
//
//  Created by Olesya Kondrashkina on 3/24/14.
//  Copyright (c) 2014 Voodoo Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VMBlocks.h"
#import "VMLocalizedText.h"

@interface VMAttributedTextComponent : NSObject <VMLocalizedText>

@property (copy, nonatomic) NSString *text;
@property (copy, nonatomic) UIColor *textColor;
@property (copy, nonatomic) NSString *fontName;
@property (nonatomic) CGFloat fontSize;

@property (copy, nonatomic) UIFont *font;

@property BOOL skipSpace;

@property (nonatomic, readonly) NSAttributedString *attributedString;
@property (nonatomic, readonly) NSAttributedString *attributedStringWithSpace;

@property (copy) VMVoidBlock didModify;

@end
