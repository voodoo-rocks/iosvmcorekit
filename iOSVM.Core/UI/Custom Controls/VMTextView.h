//
//  VMTextView.h
//  iBeacon Demo
//
//  Created by Alexander Kryshtalev on 09/01/14.
//  Copyright (c) 2014 Voodoo Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VMCustomFont.h"
#import "UIView+VMBorder.h"
#import "VMFitToContent.h"
#import "VMLocalizedText.h"
#import "VMAttributedText.h"

@interface VMTextView : UITextView <VMCustomFont, UITextViewDelegate, VMFitToContent, VMLocalizedText, VMAttributedText>
{
	UILabel *placeholderLabel;
}

- (BOOL)isAcceptableTextLength:(NSUInteger)length;

@property (copy) NSString *placeholder;
@property (copy) NSNumber *textLengthLimit;

@property (nonatomic, readonly, retain) NSString *textForValidation;
@property (nonatomic, readonly, retain) NSString *messageOnFail;

- (void)focus;
- (void)showPlaceholderIfNeeded;

@end
