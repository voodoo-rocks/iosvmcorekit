//
//  VMLocalizedText.h
//  ZenFriend
//
//  Created by Olesya Kondrashkina on 5/1/14.
//  Copyright (c) 2014 com.voodoo-mobile. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol VMLocalizedText <NSObject>

@property BOOL isLocalized;

@end

