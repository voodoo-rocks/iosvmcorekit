//
//  UIView_VMBorder.h
//  Foodomart
//
//  Created by Alexander Kryshtalev on 01/02/14.
//  Copyright (c) 2014 Alexei Pozdnyakov. All rights reserved.
//

#import <UIKit/UIKit.h>

IB_DESIGNABLE
@interface UIView ()

@property (nonatomic) IBInspectable float borderWidth;
@property (nonatomic) IBInspectable UIColor *borderColor;
@property (nonatomic) IBInspectable float viewCornerRadius;

@end
