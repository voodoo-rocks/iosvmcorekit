//
//  VMTextField.h
//  DoingFine
//
//  Created by Alexander Kryshtalev on 13/01/14.
//  Copyright (c) 2014 Voodoo Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VMCustomFont.h"
#import "VMFitToContent.h"
#import "UIView+VMBorder.h"
#import "VMLocalizedText.h"
#import "VMValidatable.h"

@interface VMTextField : UITextField  <VMCustomFont, VMFitToContent, VMLocalizedText, VMValidatable>

@property (nonatomic, readonly, retain) NSString *textForValidation;
@property (nonatomic, readonly, retain) NSString *messageOnFail;

@property (nonatomic, copy) NSString *customPlaceholderFont;

@property (copy) NSNumber *textLengthLimit;

@property (copy, nonatomic) NSNumber *textRectInset;

- (BOOL)shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string;

- (void)focus;

@end
