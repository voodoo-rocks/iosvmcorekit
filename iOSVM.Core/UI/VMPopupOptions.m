//
//  VMCoolPopupOptions.m
//  WhereItsLive
//
//  Created by Alexander Kryshtalev on 29/04/14.
//  Copyright (c) 2014 Voodoo Mobile. All rights reserved.
//

#import "VMPopupOptions.h"

@implementation VMPopupOptions

#define kDefaultAnimationDuration 0.3

+ (VMPopupOptions *)defaultOptions;
{
	VMPopupOptions *options = [[VMPopupOptions alloc] init];
	options.enableDimmedBackground = YES;
	options.animationDuration = kDefaultAnimationDuration;
	return options;
}

@end
