//
//  VMSwitcher.h
//  iOSVM.Core
//
//  Created by Olesya Kondrashkina on 9/12/14.
//  Copyright (c) 2014 Voodoo Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VMBlocks.h"

@protocol VMSwitcher <NSObject>

@property (nonatomic) NSUInteger selectedIndex;
@property (copy) VMIntegerBlock didChangeIndex;

@end
