//
//  VMPhotoPicker.m
//  Qtrank
//
//  Created by Olesya Kondrashkina on 7/7/14.
//  Copyright (c) 2014 Voodoo Mobile. All rights reserved.
//

#import "VMPhotoPicker.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import "VMActionSheet.h"
#import "UIImage+VMImageUtils.h"

#define kMaxPhotoSize 640

@interface UIImagePickerController (iOS6AutorotateFix)

@end

@implementation UIImagePickerController (iOS6AutorotateFix)

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return [UIApplication sharedApplication].statusBarOrientation;
}

@end

@implementation VMPhotoPicker

- (id)init
{
    self = [super init];
    
    self.maxPhotoSize = kMaxPhotoSize;
    
    return self;
}

- (void)takePhotoWithCallback:(VMImageBlock)block
{
	VMVoidBlock fromGallery = ^{
		[self takePhotoWithSourceType:UIImagePickerControllerSourceTypePhotoLibrary withCallback:block];
	};
	
	VMVoidBlock fromCamera = ^{
		[self takePhotoWithSourceType:UIImagePickerControllerSourceTypeCamera withCallback:block];
	};
	
	VMVoidBlock cancelBlock = ^{
		block(nil);
	};
	
    [[[VMActionSheet alloc] initWithTitle:@"Take Photo"
                             cancelButton:[VMActionButton buttonWithTitle:@"Cancel" withAction:cancelBlock]
                        destructiveButton:nil otherButtons:
      [VMActionButton buttonWithTitle:@"From Gallery" withAction:fromGallery],
      [VMActionButton buttonWithTitle:@"From Camera" withAction:fromCamera], nil] show];
}

- (void)closePicker
{
	[picker dismissViewControllerAnimated:YES completion:^{
        if (self.didDismissPicker) {
            self.didDismissPicker ();
        }
    }];
    picker.delegate = nil;
	picker = nil;
}

- (void) imagePickerController: (UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *) info {
    UIImage *originalImage = [info objectForKey:UIImagePickerControllerOriginalImage];
    UIImage *resizedImage = [originalImage scaleImageToWidth:self.maxPhotoSize];
    didTakePhoto(resizedImage);
    [self closePicker];
}

- (void)takePhotoWithSourceType:(UIImagePickerControllerSourceType)type withCallback:(VMImageBlock)block;
{
	if (![UIImagePickerController isSourceTypeAvailable:type]) {
		return;
	}
	
    didTakePhoto = block;
    
	picker = [[UIImagePickerController alloc] init];
	picker.sourceType = type;
    picker.mediaTypes = [NSArray arrayWithObjects:(NSString *) kUTTypeImage, nil];
	picker.allowsEditing = NO;
	picker.delegate = self;
	
	[[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:picker animated:YES completion:nil];
}

- (void)imagePickerControllerDidCancel: (UIImagePickerController *)uiPicker {
	
    [self closePicker];
}


@end
