//
//  ResizibleCollectionView.h
//  FriendZone
//
//  Created by Olesya Kondrashkina on 21/03/16.
//  Copyright © 2016 Alexander Kryshtalev. All rights reserved.
//

#import <UIKit/UIKit.h>

//automatically updates its height to fit content

@interface VMResizableCollectionView : UICollectionView

@end
