//
//  VMAlertButton.m
//  Ovenbot
//
//  Created by Olesya Kondrashkina on 11/14/13.
//  Copyright (c) 2013 Voodoo Mobile. All rights reserved.
//

#import "VMActionButton.h"

@implementation VMActionButton

+ (VMActionButton *)buttonWithTitle:(NSString *)title;
{
    return [VMActionButton buttonWithTitle:title withAction:nil];
}

+ (VMActionButton *)buttonWithTitle:(NSString *)title withAction:(VMVoidBlock)action;
{
    VMActionButton *button = [[VMActionButton alloc] init];
    button.title = title;
    button.actionBlock = action;
    return button;
}

@end
