//
//  VMPhotoPicker.h
//  Qtrank
//
//  Created by Olesya Kondrashkina on 7/7/14.
//  Copyright (c) 2014 Voodoo Mobile. All rights reserved.
//

#import "VMSharedObject.h"
#import "VMBlocks.h"

@interface VMPhotoPicker : VMSharedObject <UINavigationControllerDelegate, UIImagePickerControllerDelegate>
{
    VMImageBlock didTakePhoto;
    UIImagePickerController *picker;
}

@property CGFloat maxPhotoSize;

- (void)takePhotoWithCallback:(VMImageBlock)block;
- (void)takePhotoWithSourceType:(UIImagePickerControllerSourceType)sourceType withCallback:(VMImageBlock)block;

@property (copy) VMVoidBlock didDismissPicker;

@end
