//
//  VMNavigationBar.h
//  NueTune
//
//  Created by Alexander Kryshtalev on 16.10.13.
//  Copyright (c) 2013 Voodoo Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VMNavigationBar : UINavigationBar

@property BOOL fixOffsets;

- (void)customize;

@end
