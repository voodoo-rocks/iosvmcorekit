//
//  VMTableViewCell.m
//  Foodomart
//
//  Created by Alexander Kryshtalev on 27/01/14.
//  Copyright (c) 2014 Alexei Pozdnyakov. All rights reserved.
//

#import "UITableViewCell+ReusableCell.h"

@implementation UITableViewCell (ReusableCell)

+ (instancetype)cellForTableView:(UITableView *)tableView forIndexPath:(NSIndexPath *)indexPath;
{
	return [self cellForTableView:tableView forIndexPath:indexPath fromNib:NSStringFromClass(self.class)];
}

+ (instancetype)cellForTableView:(UITableView *)tableView;
{
    return [self cellForTableView:tableView fromNib:NSStringFromClass(self.class)];
}

+ (instancetype)cellForTableView:(UITableView *)tableView fromNib:(NSString *)nibName;
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:nibName];
    if (!cell){
        cell = [[[NSBundle mainBundle] loadNibNamed:nibName owner:self options:nil] objectAtIndex:0];
    }
    return cell;
}

+ (instancetype)cellForTableView:(UITableView *)tableView forIndexPath:(NSIndexPath *)indexPath fromNib:(NSString *)nibName;
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:nibName];
    if (!cell){
        cell = [[[NSBundle mainBundle] loadNibNamed:nibName owner:self options:nil] objectAtIndex:0];
    }
    return cell;
}

- (CGFloat)height
{
	return self.contentView.frame.size.height;
}

- (CGFloat)layoutHeightForTableView:(UITableView *)tableView;
{
    CGFloat height = [self.contentView systemLayoutSizeFittingSize:CGSizeMake(CGRectGetWidth(tableView.bounds), 0) withHorizontalFittingPriority:UILayoutPriorityRequired verticalFittingPriority:UILayoutPriorityDefaultLow].height;

    return height + 1;
}

@end
