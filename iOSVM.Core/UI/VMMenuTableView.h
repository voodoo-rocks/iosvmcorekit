//
//  VMMenuTableView.h
//  Ovenbot
//
//  Created by Alexander Kryshtalev on 17.10.13.
//  Copyright (c) 2013 Voodoo Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VMMenuItem.h"

typedef void(^MenuItemTapBlock)(NSInteger index, VMMenuItem *item);

@interface VMMenuTableView : UITableView <UITableViewDelegate, UITableViewDataSource>
{
	MenuItemTapBlock tapBlock;
}

@property (strong, nonatomic) IBOutletCollection(VMMenuItem) NSArray *menuItems;

- (void)subscribeForMenuItemTap:(MenuItemTapBlock)block;
- (void)reloadMenuItems;

@end
