//
//  VMAppDelegate.h
//  Cash-a-Lot
//
//  Created by Alexander Kryshtalev on 20/01/14.
//  Copyright (c) 2014 Voodoo Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VMBlocks.h"

@class VMAppDelegate;

typedef void(^VMPushCompletionBlock)(BOOL succeeded, NSString *deviceToken, NSError *error);
typedef void(^VMPushReceivedBlock)(VMAppDelegate *delegate, NSString *message, NSDictionary *pushInfo);

@interface VMAppDelegate: UIResponder <UIApplicationDelegate>
{
	NSMutableDictionary *interactivePushes;
	UINavigationController *navigationController;
	VMPushCompletionBlock pushCallback;
}

+ (instancetype)sharedDelegate;

@property (strong, nonatomic) UIWindow *window;
@property (copy) VMPushReceivedBlock didReceivePush, didReceiveLocalPush;
@property (nonatomic) BOOL lightStatusBar;

- (UINavigationController *)loadViewController:(UIViewController *)controller asRoot:(BOOL)asRoot;
- (UINavigationController *)loadViewController:(UIViewController *)controller withNavigationClass:(Class)navClass;

- (void)registerPushesWithCallback:(VMPushCompletionBlock)callback;
- (void)enableInteractivePushes:(NSArray *)pushes forCategory:(NSString *)categoryIdentifier;

@end
