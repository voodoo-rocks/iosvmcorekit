//
//  VMBaseViewController.m
//  RippIn
//
//  Created by Alexander Kryshtalev on 06.12.12.
//  Copyright (c) 2012 Voodoo Mobile. All rights reserved.
//

#import "VMBaseViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "UIView+Subviews.h"
#import "VMPlaceholderView.h"

@implementation VMBaseViewController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    if ([self respondsToSelector:@selector(setAutomaticallyAdjustsScrollViewInsets:)]) {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
}

- (void)viewDidAppear:(BOOL)animated
{
	[super viewDidAppear:animated];
	
    [self registerForKeyboardNotifications];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
	
    [self unregisterForKeyboardNotifications];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if ([text isEqualToString:@"\n"])
    {
        [textView resignFirstResponder];
    }
    return YES;
}

#pragma mark GCC diagnostic pop

- (void)navigate: (UIViewController *)controller;
{
	[self.navigationController pushViewController:controller animated:YES];
}

- (void)navigateBack
{
	[self.navigationController popViewControllerAnimated:YES];
}

- (void)navigateHome;
{
	[self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)renameBackButtonTo:(NSString *)title;
{
	self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:title style: UIBarButtonItemStylePlain target: nil action: nil];
}

- (void)hideBackButton;
{
	self.navigationItem.hidesBackButton = YES;
}

- (void)setLeftBarButtonItem:(VMBarButtonItem *)leftBarButtonItem
{
    self.navigationItem.leftBarButtonItem = leftBarButtonItem;
}

- (void)setRightBarButtonItem:(VMBarButtonItem *)rightBarButtonItem
{
    self.navigationItem.rightBarButtonItem = rightBarButtonItem;
}

- (void)setNavigationController:(UINavigationController *)navigationController
{
    externalNavigationController = navigationController;
}

- (UINavigationController *)navigationController
{
    return externalNavigationController ? externalNavigationController : [super navigationController];
}

- (BOOL)IsFirstInNavigationChain
{
    return (self.navigationController.viewControllers.count > 1 &&
            [self.navigationController.viewControllers indexOfObject:self] == 0) ||
    self.navigationController.viewControllers.count == 1;
}

#pragma mark keyboard handling

- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeShown:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}

- (void)unregisterForKeyboardNotifications
{
	[[NSNotificationCenter defaultCenter] removeObserver:self
													name:UIKeyboardDidShowNotification
												  object:nil];
	
    [[NSNotificationCenter defaultCenter] removeObserver:self
													name:UIKeyboardWillHideNotification
												  object:nil];
}

- (void)keyboardWasShown:(NSNotification *)aNotification
{
	NSDictionary *info = [aNotification userInfo];
	CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
	
    for(VMBaseViewController *controller in [self getControllersOnPlaceholders]) {
        if ([controller isKindOfClass:[VMBaseViewController class]] && controller.didShowKeyboard) {
            controller.didShowKeyboard(self, kbSize);
        }
    }
}

- (void)keyboardWillBeShown:(NSNotification *)aNotification
{
    NSDictionary *info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    
    for(VMBaseViewController *controller in [self getControllersOnPlaceholders]) {
        if ([controller isKindOfClass:[VMBaseViewController class]] && controller.willShowKeyboard) {
            controller.willShowKeyboard(self, kbSize);
        }
    }
}

- (void)keyboardWillBeHidden:(NSNotification *)aNotification
{
	NSDictionary *info = [aNotification userInfo];
	CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
	
	for(VMBaseViewController *controller in [self getControllersOnPlaceholders]) {
        if ([controller isKindOfClass:[VMBaseViewController class]] && controller.didHideKeyboard) {
            controller.didHideKeyboard(self, kbSize);
        }
    }
}

- (NSArray *)getControllersOnPlaceholders
{
    NSMutableArray *controllers = [NSMutableArray new];
    [controllers addObject:self];
    for(UIView *subview in self.view.allSubviews) {
        if([subview isKindOfClass:[VMPlaceholderView class]]) {
            id placeholderController = ((VMPlaceholderView *)subview).controller;
            if([placeholderController isKindOfClass:[UINavigationController class]]) {
                [controllers addObject:((UINavigationController *)placeholderController).viewControllers.firstObject];
            }
            else if([placeholderController isKindOfClass:[UIViewController class]]) {
                [controllers addObject:placeholderController];
            }
        }
    }
    return controllers;
}

#pragma mark close keyboard on tap

- (void)setKeyboardHandling
{
    [self setKeyboardHandlingWithInsets:UIEdgeInsetsZero forScrollView:(UIScrollView *)self.view withCloseOnTap:YES];
}

- (void)setKeyboardHandlingForScrollView:(UIScrollView *)scrollView
{
    [self setKeyboardHandlingWithInsets:UIEdgeInsetsZero forScrollView:scrollView withCloseOnTap:YES];
}

- (void)setKeyboardHandlingForScrollView:(UIScrollView *)scrollView withCloseOnTap:(BOOL)closeOnTap
{
    [self setKeyboardHandlingWithInsets:UIEdgeInsetsZero forScrollView:scrollView withCloseOnTap:closeOnTap];
}

- (void)setKeyboardHandlingWithInsets:(UIEdgeInsets)insets forScrollView:(UIScrollView *)scrollView withCloseOnTap:(BOOL)closeOnTap
{
    scrollView.contentSize = self.view.frame.size;
    scrollView.contentInset = insets;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    
    __weak typeof(self) weakSelf = self;
    
    self.didShowKeyboard = ^(VMBaseViewController *controller, CGSize keyboardSize) {
        scrollView.contentInset = UIEdgeInsetsMake(0, 0, keyboardSize.height, 0);
        closeOnTap ? [weakSelf.view addGestureRecognizer:tap] : nil;
    };
    
    self.didHideKeyboard = ^(VMBaseViewController *controller, CGSize keyboardSize) {
        scrollView.contentInset = insets;
        closeOnTap ? [weakSelf.view removeGestureRecognizer:tap] : nil;
    };
}

- (void)dismissKeyboard {
    [self.view endEditing:YES];
}

#pragma mark turn off autorotation

- (BOOL)shouldAutorotate
{
	return NO;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
	return UIInterfaceOrientationMaskPortrait;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
	return UIInterfaceOrientationPortrait;
}

@end
