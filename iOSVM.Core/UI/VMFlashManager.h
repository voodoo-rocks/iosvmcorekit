//
//  VMFlashManager.h
//  iOSVM.Core
//
//  Created by Alexander Kryshtalev on 24/07/14.
//  Copyright (c) 2014 Voodoo Mobile. All rights reserved.
//

#import "VMSharedManager.h"
#import "VMFlashView.h"

@interface VMFlashManager : VMSharedManager
{
	VMFlashView *currentFlash;
}

- (VMFlashView *)showFlash:(NSString *)title;
- (VMFlashView *)showFlash:(NSString *)title forDuration:(CGFloat)duration;

@end
