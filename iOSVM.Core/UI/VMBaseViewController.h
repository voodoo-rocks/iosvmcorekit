//
//  VMBaseViewController.h
//  RippIn
//
//  Created by Alexander Kryshtalev on 06.12.12.
//  Copyright (c) 2012 Voodoo Mobile. All rights reserved.
//

#import "VMBarButtonItem.h"

@class VMBaseViewController;

typedef void(^KeyboardShownBlock)(VMBaseViewController *controller, CGSize keyboardSize);

@interface VMBaseViewController : UIViewController <UITextFieldDelegate, UITextViewDelegate>
{
	UINavigationController *externalNavigationController;
}

- (void)navigate:(UIViewController *)controller;
- (void)navigateBack;
- (void)navigateHome;
- (void)renameBackButtonTo:(NSString *)title;
- (void)hideBackButton;
- (BOOL)IsFirstInNavigationChain;

- (NSArray *)getControllersOnPlaceholders;

- (void)setKeyboardHandling;
- (void)setKeyboardHandlingForScrollView:(UIScrollView *)scrollView;
- (void)setKeyboardHandlingForScrollView:(UIScrollView *)scrollView withCloseOnTap:(BOOL)closeOnTap;
- (void)setKeyboardHandlingWithInsets:(UIEdgeInsets)insets forScrollView:(UIScrollView *)scrollView withCloseOnTap:(BOOL)closeOnTap;

@property (copy) KeyboardShownBlock didShowKeyboard;
@property (copy) KeyboardShownBlock willShowKeyboard;
@property (copy) KeyboardShownBlock didHideKeyboard;

@property (nonatomic, retain) UINavigationController *navigationController;

@property (weak, nonatomic) IBOutlet VMBarButtonItem *leftBarButtonItem, *rightBarButtonItem;

@end

