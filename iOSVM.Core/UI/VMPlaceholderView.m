//
//  VMPlaceholderView.m
//  Malco
//
//  Created by Alexander Kryshtalev on 12.10.13.
//  Copyright (c) 2013 Voodoo Mobile. All rights reserved.
//

#import "VMPlaceholderView.h"

@implementation VMPlaceholderView

@synthesize controller;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
	[super awakeFromNib];
	
	if (self.controller) {
		[self showController];
	}
}

- (void)setController:(UIViewController *)_controller
{
	if (controller) {
        [controller.view removeFromSuperview];
    }
    controller = _controller;
    [self showController];
}

- (void)showController
{
    if (controller.view) {
        controller.view.frame = self.bounds;
        [self addSubview:self.controller.view];
    }
}

- (UIViewController *)controller
{
	return controller;
}

@end
