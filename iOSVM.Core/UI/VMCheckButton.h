//
//  VMCheckButton.h
//  NDA
//
//  Created by Alexander Kryshtalev on 18.01.13.
//  Copyright (c) 2013 Voodoo Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VMBlocks.h"

@interface VMCheckButton : UIButton

@property (copy) VMBooleanBlock didToggle; // returns state after toggle

@end
