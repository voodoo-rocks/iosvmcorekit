//
//  UIImagePickerController+NonRotating.h
//  Watch Dog
//
//  Created by Alice on 11/08/15.
//  Copyright (c) 2015 Voodoo-Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImagePickerController (NonRotating)

- (BOOL)shouldAutorotate;
- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation;

@end
