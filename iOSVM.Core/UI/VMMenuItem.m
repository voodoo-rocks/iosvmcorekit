//
//  VMSideMenuItem.m
//  Ovenbot
//
//  Created by Alexander Kryshtalev on 10.10.13.
//  Copyright (c) 2013 Voodoo Mobile. All rights reserved.
//

#import "VMMenuItem.h"

@implementation VMMenuItem

@synthesize title, iconName, controller, selectedIconName;

- (id)initWithTitle:(NSString *)_title andIconName:(NSString *)_iconName andSelectedIconName:(NSString *)_selectedIconName forController:(UIViewController *)_controller
{
    self = [self init];
	if (self) {
        self.title = _title;
		self.iconName = _iconName;
        self.selectedIconName = _selectedIconName;
		self.controller = _controller;
	}
	return self;
}

- (id)initWithTitle:(NSString *)_title andIconName:(NSString *)_iconName forController:(UIViewController *)_controller;
{
    return [self initWithTitle:_title andIconName:_iconName andSelectedIconName:nil forController:_controller];
}

- (id)initWithTitle:(NSString *)_title andIconName:(NSString *)_iconName andSelectedIconName:(NSString *)_selectedIconName;
{
	return [self initWithTitle:_title andIconName:_iconName andSelectedIconName:_selectedIconName forController:nil];
}

- (id)initWithTitle:(NSString *)_title forController:(UIViewController *)_controller;
{
	return [self initWithTitle:_title andIconName:nil forController:_controller];
}

- (id)initWithTitle:(NSString *)_title andIconName:(NSString *)_iconName;
{
	return [self initWithTitle:_title andIconName:_iconName forController:nil];
}

- (id)initWithTitle:(NSString *)_title;
{
	return [self initWithTitle:_title andIconName:nil];
}

@end
