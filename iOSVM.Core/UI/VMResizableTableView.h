//
//  ResizableTableView.h
//  Koolooks
//
//  Created by Olesya Kondrashkina on 10/02/16.
//
//

#import <UIKit/UIKit.h>

//automatically updates its height to fit content

@interface VMResizableTableView : UITableView

@end
