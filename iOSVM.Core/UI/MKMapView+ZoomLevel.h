//
//  VMMapView.h
//  Foodomart
//
//  Created by Olesya Kondrashkina on 2/6/14.
//  Copyright (c) 2014 Alexei Pozdnyakov. All rights reserved.
//

#import <MapKit/MapKit.h>

@interface MKMapView (ZoomLevel)

@property (nonatomic, readonly) CGFloat currentRadius;

- (void)centerAt:(CLLocationCoordinate2D)coordinate;
- (void)centerAt:(CLLocationCoordinate2D)coordinate andZoomTo:(NSUInteger)zoomLevel;
- (void)putSimplePinAt:(CLLocationCoordinate2D)coordinate withTitle:(NSString *)title;

@end
