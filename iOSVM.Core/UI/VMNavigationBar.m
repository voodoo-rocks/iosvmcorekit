//
//  VMNavigationBar.m
//  NueTune
//
//  Created by Alexander Kryshtalev on 16.10.13.
//  Copyright (c) 2013 Voodoo Mobile. All rights reserved.
//

#import "VMNavigationBar.h"

#define kDefaultOffset 8

@implementation VMNavigationBar

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
		[self customize];
    }
    return self;
}

- (void)layoutSubviews
{
	[super layoutSubviews];
	
	if (self.fixOffsets) {
        NSArray *buttonItems = [self.topItem.rightBarButtonItems arrayByAddingObjectsFromArray:self.topItem.leftBarButtonItems];
        
        for (UIBarButtonItem *item in buttonItems) {
            CGRect frame = item.customView.frame;
            frame.origin.y += kDefaultOffset;
            item.customView.frame = frame;
		}
	}
}

- (void)awakeFromNib
{
	[super awakeFromNib];
	[self customize];
}

- (void)customize;
{
	NSLog(@"VMNavigationBar:customize method is not overloaded. Maybe it is ok. Just overload customize and do nothing else");
}

@end
