//
//  VMFlashManager.m
//  iOSVM.Core
//
//  Created by Alexander Kryshtalev on 24/07/14.
//  Copyright (c) 2014 Voodoo Mobile. All rights reserved.
//

#import "VMFlashManager.h"
#import "VMConstants.h"

@implementation VMFlashManager

- (VMFlashView *)showFlash:(NSString *)title;
{
	return [self showFlash:title forDuration:VMConstants.flashAnimationDurationLong];
}

- (VMFlashView *)showFlash:(NSString *)title forDuration:(CGFloat)duration;
{
	CGFloat statusHeight = [UIApplication sharedApplication].statusBarFrame.size.height;
	
	currentFlash = [[VMFlashView alloc] initWithTitle:title];
	
    CGRect frame = currentFlash.frame;
    frame.origin.y += statusHeight;
	currentFlash.frame = frame;
	
	//UIViewController *rootController = .rootViewController;
	[[UIApplication sharedApplication].keyWindow addSubview:currentFlash];
	[currentFlash hideAnimated:YES withDelay:duration];
	return currentFlash;
}

@end
