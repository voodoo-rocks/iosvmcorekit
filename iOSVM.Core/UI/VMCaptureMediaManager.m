//
//  VMTakePhotoManager.m
//  Cash-a-Lot
//
//  Created by Alexander Kryshtalev on 26/01/14.
//  Copyright (c) 2014 Voodoo Mobile. All rights reserved.
//

#import "VMCaptureMediaManager.h"
#import "VMActionSheet.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import "UIImagePickerController+NonRotating.h"

@implementation VMCaptureMediaManager

- (void)takePhotoWithBlock:(TakePhotoBlock)_photoBlock usingViewController:(UIViewController *)viewController;
{
	photoBlock = _photoBlock;
	
	VMVoidBlock fromGallery = ^{
        UIImagePickerController *picker = [self showPickerForSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
        picker.navigationBar.barTintColor = [UIColor lightGrayColor];
		[viewController presentViewController:picker animated:YES completion:nil];
	};
	
	VMVoidBlock fromCamera = ^{
		UIImagePickerController *picker = [self showPickerForSourceType:UIImagePickerControllerSourceTypeCamera];
		[viewController presentViewController:picker animated:YES completion:nil];
	};
	
	VMVoidBlock cancelBlock = ^{
		photoBlock(nil);
	};
	
    if (self.selectSourceBlock) {
        self.selectSourceBlock (fromGallery, fromCamera, cancelBlock);
    }
    else {
        [[[VMActionSheet alloc] initWithTitle:self.mediaType == VMMediaTypePhoto ? @"Take Photo" : @"Capture Video"
                                cancelButton:[VMActionButton buttonWithTitle:@"Cancel" withAction:cancelBlock]
                           destructiveButton:nil otherButtons:
          [VMActionButton buttonWithTitle:@"From Gallery" withAction:fromGallery],
          [VMActionButton buttonWithTitle:@"From Camera" withAction:fromCamera], nil] showInView:viewController.view];
    }
}

- (void)closePicker:(UIImagePickerController *)picker
{
	[picker dismissViewControllerAnimated:YES completion:nil];
    picker.delegate = nil;
}

- (void)imagePickerController: (UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *) info {
    NSString *type = [info objectForKey:UIImagePickerControllerMediaType];
    if ([type isEqualToString:(NSString *)kUTTypeVideo] ||
        [type isEqualToString:(NSString *)kUTTypeMovie])
    {
        NSURL *urlVideo = [info objectForKey:UIImagePickerControllerMediaURL];
        if (photoBlock) {
            NSData *data = [NSData dataWithContentsOfURL:urlVideo];
            photoBlock (data);
        }
    }
    else if ([type isEqualToString:(NSString *)kUTTypeImage]) {
        UIImage *originalImage = [info objectForKey:UIImagePickerControllerOriginalImage];
        if (photoBlock) {
            NSData *data = UIImageJPEGRepresentation(originalImage, 1.0f);
            photoBlock(data);
        }
	}
	[self closePicker:picker];
}

- (UIImagePickerController *)showPickerForSourceType:(enum UIImagePickerControllerSourceType)type
{
	if (![UIImagePickerController isSourceTypeAvailable:type]) {
		return nil;
	}
	
	UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.navigationBar.tintColor = self.tintColor;
	picker.sourceType = type;
    if (self.mediaType == VMMediaTypePhoto) {
        picker.mediaTypes = [NSArray arrayWithObjects:(NSString *) kUTTypeImage, nil];
    }
    else if (self.mediaType == VMMediaTypeVideo) {
        picker.mediaTypes = [NSArray arrayWithObjects:(NSString *) kUTTypeMovie, nil];
    }
	picker.allowsEditing = NO;
	picker.delegate = self;
	
	return picker;
}

- (void)imagePickerControllerDidCancel: (UIImagePickerController *)picker {
    [self closePicker:picker];
}

- (void)capturePhotoOnView:(UIView *)view withBlock:(TakePhotoBlock)block usingViewController:(UIViewController *)viewController;
{
	UIImagePickerController *picker = [self showPickerForSourceType:UIImagePickerControllerSourceTypeCamera];

	CGSize screenSize = [UIScreen mainScreen].bounds.size;
    CGFloat newHeight = screenSize.height - view.frame.size.height;
    CGFloat cameraAspectRatio = 4.0f / 3.0f;
	
	CGFloat camViewHeight = screenSize.width * cameraAspectRatio;
    CGFloat scale = newHeight / camViewHeight;
	
	if (scale > 1.0){
        picker.cameraViewTransform = CGAffineTransformScale(picker.cameraViewTransform, scale, scale);
        picker.cameraViewTransform = CGAffineTransformTranslate(picker.cameraViewTransform, 0, (newHeight - camViewHeight) / 2);
    }
	
    picker.showsCameraControls = NO;
    picker.cameraCaptureMode = UIImagePickerControllerCameraCaptureModePhoto;
    picker.cameraOverlayView = view;
    picker.transitioningDelegate = self;

	[viewController presentViewController:picker animated:YES completion:nil];
}

@end
