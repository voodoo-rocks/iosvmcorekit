//
//  UIImage+FixOrientation.h
//  Echo
//
//  Created by Mihail Kirillov on 18.03.14.
//  Copyright (c) 2014 Voodoo Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (FixOrientation)

- (UIImage *)fixOrientation;

@end
