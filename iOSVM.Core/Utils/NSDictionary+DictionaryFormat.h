//
//  NSString+DictionaryFormat.h
//  iOSVM.Core
//
//  Created by Alice on 16/11/2016.
//  Copyright © 2016 Voodoo Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (DictionaryFormat)

- (NSString *)printBeautifulDictionary;

@end
