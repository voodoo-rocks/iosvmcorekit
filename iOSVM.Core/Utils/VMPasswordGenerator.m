//
//  VMPasswordGenerator.m
//  iOSVM.Core
//
//  Created by Alexander Kryshtalev on 02/07/14.
//  Copyright (c) 2014 Voodoo Mobile. All rights reserved.
//

#import "VMPasswordGenerator.h"

@implementation VMPasswordGenerator

+ (NSString *)generate;
{
	const int kDefaultPasswordLength = 8;
	return [self generateOfLength:kDefaultPasswordLength];
}

+ (NSString *)generateOfLength:(NSUInteger)length;
{
	const NSString *letters = @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

	NSMutableString *randomString = [NSMutableString stringWithCapacity:length];
	
    for (int i = 0; i < length; i++) {
		[randomString appendFormat: @"%C", [letters characterAtIndex: arc4random_uniform((unsigned int)letters.length) % letters.length]];
    }
	
    return randomString;
}

@end
