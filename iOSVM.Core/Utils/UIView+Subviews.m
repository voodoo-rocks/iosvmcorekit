//
//  UIView+Subviews.m
//  AnyExcuse
//
//  Created by Alice on 03/08/2017.
//  Copyright © 2017 Voodoo Mobile. All rights reserved.
//

#import "UIView+Subviews.h"

@implementation UIView (Subviews)

- (NSArray *)allSubviews
{
    __block NSArray* allSubviews = [NSArray arrayWithObject:self];
    
    [self.subviews enumerateObjectsUsingBlock:^(UIView* view, NSUInteger idx, BOOL *stop) {
        allSubviews = [allSubviews arrayByAddingObjectsFromArray:[view allSubviews]];
    }];
    return allSubviews;
}

@end
