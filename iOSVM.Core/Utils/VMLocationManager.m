//
//  VMLocationManager.m
//  iOSVM
//
//  Created by Dmitry Tihonov on 05.07.13.
//  Copyright (c) 2013 Voodoo Mobile. All rights reserved.
//

#import "VMLocationManager.h"

#define METERS_PER_DEGREE 111319.5

@implementation VMLocationManager

- (id)init
{
	self = [super init];
    
    locationManager = [[CLLocationManager alloc] init];
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    locationManager.delegate = self;
    
    geocoder = [[CLGeocoder alloc] init];
    
	return self;
}

- (BOOL)permissionGranted
{
    return [CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways || [CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse;
}

- (BOOL)permissionRestricted
{
    return [CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied || [CLLocationManager authorizationStatus] == kCLAuthorizationStatusRestricted;
}

- (void)requestForAlways:(BOOL)always withCallback:(PermissionStatusBlock)block
{
    didChangePermissionStatus = block;
    
    always ? [locationManager requestAlwaysAuthorization] : [locationManager requestWhenInUseAuthorization];
}

- (CLLocation *)lastKnownLocation
{
    if(locationManager.location.coordinate.latitude || locationManager.location.coordinate.longitude) {
        _lastKnownLocation = locationManager.location;
    }
    return _lastKnownLocation;
}

#pragma mark Core Location Delegate

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    if (didChangePermissionStatus) {
        didChangePermissionStatus(status);
    }
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
    [self updateLocation:newLocation];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    [self updateLocation:locations.lastObject];
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    [self updateLocation:nil];
}

- (void)currentLocationWithCompletion:(UpdateLocationBlock)block isRecursively:(BOOL)recursively
{
    [self currentLocationWithCompletion:block isRecursively:recursively withDistanceDelta:kCLDistanceFilterNone];
}

- (void)currentLocationWithCompletion:(UpdateLocationBlock)block isRecursively:(BOOL)recursively withDistanceDelta:(CLLocationDistance)distance
{
    [self stopUpdating];
    changeLocationBlock = block;
    isRecursivelyUpdating = recursively;
    locationManager.distanceFilter = distance;
    locationManager.delegate = self;
    
    if ([[NSBundle mainBundle] objectForInfoDictionaryKey:@"NSLocationWhenInUseUsageDescription"] && [locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [locationManager requestWhenInUseAuthorization];
    } else if ([[NSBundle mainBundle] objectForInfoDictionaryKey:@"NSLocationAlwaysUsageDescription"] && [locationManager respondsToSelector:@selector(requestAlwaysAuthorization)]) {
        [locationManager requestAlwaysAuthorization];
    }
    
    [locationManager startUpdatingLocation];
}

- (void)randomLocationInRadius:(float)radius withCompletition:(UpdateLocationBlock)block isRecursively:(BOOL)recursively;
{
    [self currentLocationWithCompletion:^(CLLocation *newLocation) {
        double randomLat = newLocation.coordinate.latitude - radius / METERS_PER_DEGREE + ((float)rand() / RAND_MAX) * 2 * radius / METERS_PER_DEGREE;
        double randomLng = newLocation.coordinate.longitude - radius / METERS_PER_DEGREE + ((float)rand() / RAND_MAX) * 2 * radius / METERS_PER_DEGREE;
        CLLocation *randomLocation = [[CLLocation alloc] initWithLatitude:randomLat longitude:randomLng];
        block (randomLocation);
        
    } isRecursively:recursively];
}

- (void)updateLocation:(CLLocation *)newLocation
{
    if (changeLocationBlock) {
        changeLocationBlock(newLocation);
    }
    if (!isRecursivelyUpdating){
        [self stopUpdating];
    }
}

- (void)stopUpdating
{
    [locationManager stopUpdatingLocation];
    changeLocationBlock = nil;
    locationManager.delegate = nil;
}

- (void)trackDistance:(NSInteger)distance fromLocation:(CLLocation *)location completion:(TrackLocationBlock)block
{
    [self currentLocationWithCompletion:^(CLLocation *newLocation) {
        if ([newLocation distanceFromLocation:location] > distance){
            [self stopUpdating];
            block();
        }
    } isRecursively:YES];
}

- (void)addressForCurrentLocationWithCompletition:(AddressBlock)block;
{
    [self currentLocationWithCompletion:^(CLLocation *newLocation) {
        [self addressForLocation:newLocation withCompletition:block];
    } isRecursively:NO];
}

- (void)addressForLocation:(CLLocation *)location withCompletition:(AddressBlock)block;
{
    [geocoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error) {
        NSLog(@"Found placemarks: %@, error: %@", placemarks, error);
        NSString *address = nil;
        if (error == nil && [placemarks count] > 0) {
            placemark = [placemarks firstObject];
			NSDictionary *dictionary = placemark.addressDictionary;
			if ([dictionary.allKeys containsObject:@"FormattedAddressLines"]) {
				NSArray *addressLines = [dictionary objectForKey:@"FormattedAddressLines"];
				address = [addressLines componentsJoinedByString:@", "];
			}
        } else {
            NSLog(@"%@", error.debugDescription);
        }
        if (block) {
            block(address);
        }
    } ];
}


- (void)locationForAddress:(NSString *)address withCompletion:(LocationBlock)block
{
    [geocoder geocodeAddressString:address completionHandler: block];
}

- (void)regionForAddress:(NSString *)address withCompletion:(VMIdBlock)callback
{
    [geocoder geocodeAddressString:address completionHandler:^(NSArray<CLPlacemark *> * _Nullable placemarks, NSError * _Nullable error) {
        placemark = placemarks.firstObject;
        if(callback) {
            callback(placemark.region);
        }
    }];
}

@end
