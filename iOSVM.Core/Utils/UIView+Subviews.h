//
//  UIView+Subviews.h
//  AnyExcuse
//
//  Created by Alice on 03/08/2017.
//  Copyright © 2017 Voodoo Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Subviews)

- (NSArray *)allSubviews;

@end
