//
//  VMSessionManager.h
//  iOSVM.Core
//
//  Created by Alexander Kryshtalev on 19/06/14.
//  Copyright (c) 2014 Voodoo Mobile. All rights reserved.
//

#import "VMSharedManager.h"
#import "VMToken.h"

@interface VMSessionManager : VMSharedManager

@property (assign, nonatomic, readonly) BOOL firstTimeLaunch;

@end
