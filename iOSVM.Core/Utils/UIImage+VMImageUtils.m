//
//  VMGraphicUtils.m
//  Ours
//
//  Created by Alexander Kryshtalev on 12.12.13.
//  Copyright (c) 2013 Voodoo Mobile. All rights reserved.
//

#import "UIImage+VMImageUtils.h"
#import <QuartzCore/QuartzCore.h>

@implementation UIImage (VMImageUtils)

- (UIImage *)cropImageFromRect:(CGRect)rect
{
	CGFloat scale = [[UIScreen mainScreen] scale];
	if (scale > 1.0f) {
		rect = CGRectMake(rect.origin.x * scale,
						  rect.origin.y * scale,
						  rect.size.width * scale,
						  rect.size.height * scale);
	}
	
	CGImageRef imageRef = CGImageCreateWithImageInRect(self.CGImage, rect);
	UIImage *result = [UIImage imageWithCGImage:imageRef scale:scale orientation:self.imageOrientation];
	CGImageRelease(imageRef);
	
	return result;
}

- (UIImage *)scaleImageToSize:(CGSize)size
{
    UIGraphicsBeginImageContext(size);
    [self drawInRect:CGRectMake(0,0,size.width,size.height)];
    UIImage *result = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return result;
}

- (UIImage *)scaleImageToWidth:(float)width
{
    if (self.size.width <= width) {
        return self;
    }
    
    float factor = self.size.width / width;
    
    CGSize imageSize = CGSizeMake(self.size.width / factor, self.size.height / factor);
    
    return [self scaleImageToSize:imageSize];
}


- (UIImage *)blur
{
    // create our blurred image
    CIContext *context = [CIContext contextWithOptions:nil];
    CIImage *inputImage = [CIImage imageWithCGImage:self.CGImage];
	
    // setting up Gaussian Blur (we could use one of many filters offered by Core Image)
    CIFilter *filter = [CIFilter filterWithName:@"CIGaussianBlur"];
    [filter setValue:inputImage forKey:kCIInputImageKey];
    [filter setValue:[NSNumber numberWithFloat:5.0f] forKey:@"inputRadius"];
    CIImage *result = [filter valueForKey:kCIOutputImageKey];
	
    // CIGaussianBlur has a tendency to shrink the image a little,
    // this ensures it matches up exactly to the bounds of our original image
    CGImageRef cgImage = [context createCGImage:result fromRect:[inputImage extent]];
	
	//return [UIImage imageWithCGImage:cgImage];
	
    // if you need scaling
	UIImage *returnImage = [[self class] scaleIfNeeded:cgImage];
	CGImageRelease(cgImage);
	
    return returnImage;
}

- (UIImage *)scaleIfNeeded:(CGImageRef)cgimage {
    bool isRetina = [[[UIDevice currentDevice] systemVersion] intValue] >= 4 && [[UIScreen mainScreen] scale] == 2.0;
    if (isRetina) {
        return [UIImage imageWithCGImage:cgimage scale:2.0 orientation:UIImageOrientationUp];
    } else {
        return [UIImage imageWithCGImage:cgimage];
    }
}

- (UIImage *)paintImageWithColor:(UIColor *)color;
{
    return [self applyGradientWithStartColor:color endColor:color];
}

- (UIImage *)applyGradientWithStartColor:(UIColor *)startColor endColor:(UIColor *)endColor
{
	UIGraphicsBeginImageContextWithOptions(self.size, NO, self.scale);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextTranslateCTM(context, 0, self.size.height);
    CGContextScaleCTM(context, 1.0, -1.0);
	
    CGContextSetBlendMode(context, kCGBlendModeNormal);
    CGRect rect = CGRectMake(0, 0, self.size.width, self.size.height);
    //CGContextDrawImage(context, rect, img.CGImage);
	
    // Create gradient
    NSArray *colors = [NSArray arrayWithObjects:(id)endColor.CGColor, (id)startColor.CGColor, nil];
    CGColorSpaceRef space = CGColorSpaceCreateDeviceRGB();
    CGGradientRef gradient = CGGradientCreateWithColors(space, (__bridge CFArrayRef)colors, NULL);
	
    // Apply gradient
    CGContextClipToMask(context, rect, self.CGImage);
    CGContextDrawLinearGradient(context, gradient, CGPointMake(0,0), CGPointMake(0, self.size.height), 0);
    UIImage *gradientImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
	
    CGGradientRelease(gradient);
    CGColorSpaceRelease(space);
	
    return gradientImage;
}

@end
