//
//  AuxManager.h
//  Prodensa
//
//  Created by Alexander Kryshtalev on 01.02.13.
//  Copyright (c) 2013 Voodoo Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MessageUI/MessageUI.h>

typedef void(^MailSentBlock)(BOOL succeded);

@interface VMComposeManager : NSObject <MFMailComposeViewControllerDelegate, MFMessageComposeViewControllerDelegate>
{
    MailSentBlock mailSentBlock;
}

+ (VMComposeManager *)sharedManager;

- (void)callPhoneNumber:(NSString *)number;

- (MFMailComposeViewController *)composeEmailTo:(NSString *)recipient withSubject:(NSString *)subject andMessage:(NSString *)message;
- (MFMailComposeViewController *)composeEmailTo:(NSString *)recipient withSubject:(NSString *)subject andMessage:(NSString *)message andImage:(UIImage *)image;
- (MFMailComposeViewController *)composeEmailTo:(NSString *)recipient withSubject:(NSString *)subject andMessage:(NSString *)message andAttachment:(NSData *)attachment mime:(NSString *)mime name:(NSString *)name completion:(MailSentBlock)block;

- (MFMessageComposeViewController *)composeMessage:(NSString *)message;
- (MFMessageComposeViewController *)composeMessageTo:(NSString *)recipient withMessage:(NSString *)message;
- (MFMessageComposeViewController *)composeMultipleMessageTo:(NSArray *)recipients withMessage:(NSString *)message completion:(MailSentBlock)block;
- (MFMessageComposeViewController *)composeMultipleMessageTo:(NSArray *)recipients withMessage:(NSString *)message andAttachment:(NSData *)attachment mime:(NSString *)mime name:(NSString *)name completion:(MailSentBlock)block;

@end
