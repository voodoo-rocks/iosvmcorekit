//
//  VMSharedObject.h
//  iOSVM.Core
//
//  Created by Alexander Kryshtalev on 30/10/14.
//  Copyright (c) 2014 Voodoo Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VMSharedObject : NSObject

+ (instancetype)shared;

@end
