//
//  VMColor.m
//  NueTune
//
//  Created by Alexander Kryshtalev on 06.11.13.
//  Copyright (c) 2013 Voodoo Mobile. All rights reserved.
//

#import "UIColor+HexString.h"

@implementation UIColor (HexString)

+ (UIColor *)colorWithHexCode:(NSUInteger)rgbValue withAlpha:(float)alpha;
{
    return [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:alpha];
}

+ (UIColor *)colorWithHexString:(NSString *)hexString withAlpha:(float)alpha;
{
    NSString *realString = [hexString rangeOfString:@"0x"].location != NSNotFound ? hexString : [@"0x" stringByAppendingString:hexString];
    unsigned int hexValue;
    [[NSScanner scannerWithString:realString] scanHexInt:&hexValue];
    return [UIColor colorWithHexCode:hexValue withAlpha:alpha];
}

+ (UIColor *)colorWithHexString:(NSString *)hexString
{
    return [UIColor colorWithHexString:hexString withAlpha:1.0];
}

+ (UIColor *)colorWithHexCode:(NSUInteger)rgbValue;
{
    return [UIColor colorWithHexCode:rgbValue withAlpha:1.0];
}

+ (UIColor *)makeItDarkerThan:(UIColor *)color;
{
	const float darkerLevel = 0.15f;
	CGFloat red, green, blue, alpha;
	[color getRed:&red green:&green blue:&blue alpha:&alpha];
	
	return [UIColor colorWithRed:MAX(red - darkerLevel, 0)
						   green:MAX(green - darkerLevel, 0)
							blue:MAX(blue - darkerLevel, 0)
						   alpha:alpha];
}

@end
