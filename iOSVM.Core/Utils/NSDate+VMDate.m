//
//  NSDate+VMDate.m
//  GameOn
//
//  Created by Dmitry Tihonov on 16.07.13.
//  Copyright (c) 2013 Voodoo Mobile. All rights reserved.
//

#import "NSDate+VMDate.h"

@implementation NSDate (VMDate)

NSString *mySqlFormat = @"yyyy-MM-dd HH:mm:ss";

+ (NSDate *)utcDate;
{
    NSString *localDateString = [[NSDate date] localDateStringWithFormat:mySqlFormat];
    return [self utcDateFromString:localDateString withFormat:mySqlFormat];
}

+ (NSDate *)utcDateFromString:(NSString *)dateString
{
    return [self dateFromString:dateString withFormat:mySqlFormat withTimezone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
}

+ (NSDate *)localDateFromString:(NSString *)dateString
{
    return [self dateFromString:dateString withFormat:mySqlFormat withTimezone:[NSTimeZone localTimeZone]];
}

+ (NSDate *)utcDateFromString:(NSString *)dateString withFormat:(NSString *)format
{
    return [self dateFromString:dateString withFormat:format withTimezone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
}

+ (NSDate *)localDateFromString:(NSString *)dateString withFormat:(NSString *)format
{
    return [self dateFromString:dateString withFormat:format withTimezone:[NSTimeZone localTimeZone]];
}

+ (NSDate *)dateFromString:(NSString *)dateString withFormat:(NSString *)format withTimezone:(NSTimeZone *)timezone
{
    NSDateFormatter *dateFormatter = [NSDate sharedDateFormatter];
    dateFormatter.dateFormat = format;
    dateFormatter.timeZone = timezone;
    return [dateFormatter dateFromString:dateString];
}

- (NSString *)utcDateString
{
    return [self utcDateStringWithFormat:mySqlFormat];
}

- (NSString *)localDateString
{
    return [self localDateStringWithFormat:mySqlFormat];
}

- (NSString *)utcDateStringWithFormat:(NSString *)format
{
    return [self dateStringWithFormat:format withTimezone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
}

- (NSString *)localDateStringWithFormat:(NSString *)format
{
    return [self dateStringWithFormat:format withTimezone:[NSTimeZone localTimeZone]];
}

- (NSString *)utcDateStringWithFormat:(NSString *)format withLocale:(NSLocale *)locale
{
    return [self dateStringWithFormat:format withTimezone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"] withLocale:locale];
}

- (NSString *)localDateStringWithFormat:(NSString *)format withLocale:(NSLocale *)locale
{
    return [self dateStringWithFormat:format withTimezone:[NSTimeZone localTimeZone] withLocale:locale];
}

- (NSString *)dateStringWithFormat:(NSString *)format withTimezone:(NSTimeZone *)timeZone
{
    NSDateFormatter *dateFormatter = [NSDate sharedDateFormatter];
    dateFormatter.dateFormat = format;
    dateFormatter.timeZone = timeZone;
    return [dateFormatter stringFromDate:self];
}

- (NSString *)dateStringWithFormat:(NSString *)format withTimezone:(NSTimeZone *)timeZone withLocale:(NSLocale *)locale
{
    NSDateFormatter *dateFormatter = [NSDate sharedDateFormatter];
    dateFormatter.dateFormat = format;
    dateFormatter.timeZone = timeZone;
    dateFormatter.locale = locale;
    return [dateFormatter stringFromDate:self];
}

- (NSString *)monthName
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    return [[dateFormatter monthSymbols] objectAtIndex:self.month - 1];
}

- (NSDateComponents *)dateComponents
{
    NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
    
    return [calendar components:(NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond | NSCalendarUnitMonth | NSCalendarUnitYear | NSCalendarUnitDay | NSCalendarUnitWeekday) fromDate:self];
}

- (NSDate *)beginningOfDay
{
    NSDateComponents *components = [[NSCalendar autoupdatingCurrentCalendar] components:( NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay ) fromDate:self];
    return [[NSCalendar autoupdatingCurrentCalendar] dateFromComponents:components];
}

- (NSDate *)addOneDay
{
    NSDateComponents *dayComponent = [[NSDateComponents alloc] init];
    dayComponent.day = 1;
    return [[NSCalendar autoupdatingCurrentCalendar] dateByAddingComponents:dayComponent toDate:self options:0];
}

- (NSInteger)year
{
    return self.dateComponents.year;
}

- (NSInteger)month
{
    return self.dateComponents.month;
}

- (NSInteger)day
{
    return self.dateComponents.day;
}

- (NSInteger)weekDay
{
    return self.dateComponents.weekday;
}

- (NSInteger)minute
{
    return self.dateComponents.minute;
}

- (NSInteger)hour
{
    return self.dateComponents.hour;
}

- (NSInteger)second
{
    return self.dateComponents.second;
}

+ (NSDateFormatter *)sharedDateFormatter
{
    static NSDateFormatter *dateFormatter;
    if(!dateFormatter) {
        dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_GB"];
    }
    return dateFormatter;
}

@end
