//
//  VMAuthSessionManager.h
//  iOSVM.Core
//
//  Created by Alexander Kryshtalev on 19/06/14.
//  Copyright (c) 2014 Voodoo Mobile. All rights reserved.
//

#import "VMSessionManager.h"

@interface VMAuthSessionManager : VMSessionManager
{
	VMToken *token;
}

@property (nonatomic, copy) VMToken *token;
@property (nonatomic, readonly) BOOL isAuthorized;

- (void)logOut;

@end
