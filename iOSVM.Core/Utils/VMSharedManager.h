//
//  VMSharedManager.h
//  iBeacon Demo
//
//  Created by Alexander Kryshtalev on 04/01/14.
//  Copyright (c) 2014 Voodoo Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VMSharedManager: NSObject

+ (instancetype)sharedManager;

@end
