//
//  Uuid.h
//  Ovenbot
//
//  Created by Alexander Kryshtalev on 12.10.13.
//  Copyright (c) 2013 Voodoo Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VMUuid : NSObject

+ (NSString *)generate;

@end
