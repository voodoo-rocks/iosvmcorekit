//
//  Uuid.m
//  Ovenbot
//
//  Created by Alexander Kryshtalev on 12.10.13.
//  Copyright (c) 2013 Voodoo Mobile. All rights reserved.
//

#import "VMUuid.h"

@implementation VMUuid

+ (NSString *)generate
{
	CFUUIDRef theUUID = CFUUIDCreate(NULL);
	CFStringRef string = CFUUIDCreateString(NULL, theUUID);
	CFRelease(theUUID);
	
	NSString *result = (__bridge NSString *)string;
	CFRelease(string);
	
	return result;
}

@end
