//
//  NSString+VMCaseConverter.m
//  GameOn
//
//  Created by Dmitry Tihonov on 16.07.13.
//  Copyright (c) 2013 Voodoo Mobile. All rights reserved.
//

#import "NSString+VMCaseConverter.h"

@implementation NSString (VMCaseConverter)

- (NSString *)camelCaseFormattedString
{
    NSArray *substrings = [self componentsSeparatedByString:@"_"];
    substrings = [substrings filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(NSString *evaluatedObject, NSDictionary<NSString *,id> * _Nullable bindings) {
        return evaluatedObject.length > 0;
    }]];
    NSMutableString *resultString = [[NSMutableString alloc] init];
    [substrings enumerateObjectsUsingBlock:^(NSString *obj, NSUInteger idx, BOOL *stop) {
        if (idx == 0){
            [resultString appendString:obj];
        } else {
            [resultString appendFormat:@"%@%@", [[obj substringToIndex:1] uppercaseString], [obj substringFromIndex:1]];
        }
    }];
    return resultString;
}

- (NSString *)underscoreFormattedString
{
    NSMutableString *resultString = [[NSMutableString alloc] init];
    [self enumerateSubstringsInRange:NSMakeRange(0, self.length) options:NSStringEnumerationByComposedCharacterSequences usingBlock:^(NSString *substring, NSRange substringRange, NSRange enclosingRange, BOOL *stop) {
        NSCharacterSet *uppercaseSet = [NSCharacterSet uppercaseLetterCharacterSet];
        BOOL isUppercase = [uppercaseSet characterIsMember:[substring characterAtIndex:0]];
        BOOL isPrevUppercase = substringRange.location == 0 ? YES : [uppercaseSet characterIsMember:[[self substringWithRange:NSMakeRange(substringRange.location - 1, 1)] characterAtIndex:0]];
        BOOL isNextUppercase = substringRange.location == self.length - 1 ? YES : [uppercaseSet characterIsMember:[[self substringWithRange:NSMakeRange(substringRange.location + 1, 1)] characterAtIndex:0]];
        if ((isUppercase && !isPrevUppercase) || (isUppercase && !isNextUppercase && substringRange.location != 0)){
            [resultString appendString:@"_"];
        }
        [resultString appendString:[substring lowercaseString]];
    }];
    return resultString;
}

@end
