//
//  VMPaymentCardUtils.m
//  iOSVM.Core
//
//  Created by Alexander Kryshtalev on 13/11/14.
//  Copyright (c) 2014 Voodoo Mobile. All rights reserved.
//

#import "VMPaymentCardUtils.h"

@implementation VMPaymentCardUtils

- (BOOL)isCorrect:(NSString *)cardNumber;
{
	NSMutableArray *stringAsChars = [self toCharArray:cardNumber];
	
	BOOL isOdd = YES;
	int oddSum = 0;
	int evenSum = 0;
	
	for (int i = (int)stringAsChars.count - 1; i >= 0; i--) {
		int digit = [(NSString *)[stringAsChars objectAtIndex:i] intValue];
		if (isOdd)
			oddSum += digit;
		else
			evenSum += digit / 5 + (2 * digit) % 10;
		
		isOdd = !isOdd;
	}
	
	return ((oddSum + evenSum) % 10 == 0);
}

- (NSMutableArray *)toCharArray:(NSString *)cardNumber {
	
	NSMutableArray *characters = [[NSMutableArray alloc] initWithCapacity:cardNumber.length];
	for (int i=0; i < cardNumber.length; i++) {
		NSString *ichar  = [NSString stringWithFormat:@"%c", [cardNumber characterAtIndex:i]];
		[characters addObject:ichar];
	}
	
	return characters;
}

- (VMPaymentCard)detectPaymentCard:(NSString *)cardNumber;
{	
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", @"^4[0-9]{6,}$"];
	if ([predicate evaluateWithObject:cardNumber]) {
		return VMPaymentCardVisa;
	}
	
	predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", @"^5[1-5][0-9]{5,}$"];
	if ([predicate evaluateWithObject:cardNumber]) {
		return VMPaymentCardMastercard;
	}
	
	predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", @"^3[47][0-9]{5,}$"];
	if ([predicate evaluateWithObject:cardNumber]) {
		return VMPaymentCardAmEx;
	}
	
	predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", @"^3(?:0[0-5]|[68][0-9])[0-9]{4,}$"];
	if ([predicate evaluateWithObject:cardNumber]) {
		return VMPaymentCardDinersClub;
	}

	predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", @"^6(?:011|5[0-9]{2})[0-9]{3,}$"];
	if ([predicate evaluateWithObject:cardNumber]) {
		return VMPaymentCardDiscover;
	}

	predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", @"^(?:2131|1800|35[0-9]{3})[0-9]{3,}$"];
	if ([predicate evaluateWithObject:cardNumber]) {
		return VMPaymentCardJCB;
	}
	
	return VMPaymentCardIncorrect;
}

@end
