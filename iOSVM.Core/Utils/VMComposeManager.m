//
//  AuxManager.m
//  Prodensa
//
//  Created by Alexander Kryshtalev on 01.02.13.
//  Copyright (c) 2013 Voodoo Mobile. All rights reserved.
//

#import "VMComposeManager.h"
#import "VMFlashManager.h"

@implementation VMComposeManager

static VMComposeManager *instance;

- (id)init
{
    self = [super init];
    return self;
}

+ (VMComposeManager *)sharedManager;
{
	if (instance == nil) {
		instance = [[VMComposeManager alloc] init];
	}
	return instance;
}

- (void)callPhoneNumber:(NSString *)number
{
	NSString *formatted = [number stringByReplacingOccurrencesOfString:@"[^0-9]" withString:@"" options:NSRegularExpressionSearch range:NSMakeRange(0, [number length])];
    NSURL* callUrl=[NSURL URLWithString:[NSString   stringWithFormat:@"tel:+%@", formatted]];
	
	//check  Call Function available only in iphone
    if([[UIApplication sharedApplication] canOpenURL:callUrl])
    {
        [[UIApplication sharedApplication] openURL:callUrl];
    }
    else
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Info" message:@"This function is only available on the iPhone"  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[alert show];
	}
}

+ (UIViewController *)rootController
{
    return [UIApplication sharedApplication].keyWindow.rootViewController;
}

- (MFMailComposeViewController *)composeEmailTo:(NSString *)recipient withSubject:(NSString *)subject andMessage:(NSString *)message
{
	if ([MFMailComposeViewController canSendMail]) {
		UIViewController *rootController = [VMComposeManager rootController];
		MFMailComposeViewController *controller = [[MFMailComposeViewController alloc] init];
        controller.mailComposeDelegate = self;
		[controller setSubject:subject];
		[controller setToRecipients:[[NSArray alloc] initWithObjects:recipient, nil]];
		[controller setMessageBody:message isHTML:FALSE];
		[rootController presentViewController:controller animated:YES
								   completion:^{
									   
								   }];
		
		return controller;
	}
	
	return nil;
}

- (MFMailComposeViewController *)composeEmailTo:(NSString *)recipient withSubject:(NSString *)subject andMessage:(NSString *)message andAttachment:(NSData *)attachment mime:(NSString *)mime name:(NSString *)name completion:(MailSentBlock)block
{
    
    mailSentBlock = block;
    
    if ([MFMailComposeViewController canSendMail]) {
		UIViewController *rootController = [VMComposeManager rootController];
		MFMailComposeViewController *controller = [[MFMailComposeViewController alloc] init];
        controller.mailComposeDelegate = self;
		[controller setSubject:subject];
		[controller setToRecipients:[[NSArray alloc] initWithObjects:recipient, nil]];
		[controller setMessageBody:message isHTML:FALSE];
        [controller addAttachmentData:attachment mimeType:mime fileName:name];
		[rootController presentViewController:controller animated:YES
								   completion:^{
									   
								   }];
		
		return controller;
	}
    
    if (block){
        block(NO);
    }
	
	return nil;
}

- (MFMailComposeViewController *)composeEmailTo:(NSString *)recipient withSubject:(NSString *)subject andMessage:(NSString *)message andImage:(UIImage *)image;
{
    [self composeEmailTo:recipient withSubject:subject andMessage:message andAttachment:[NSData dataWithData:UIImagePNGRepresentation(image)] mime:@"image/png" name:@"image" completion:nil];
	
	return nil;
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error;
{
    [controller dismissViewControllerAnimated:YES completion:nil];
    if (mailSentBlock){
        mailSentBlock(MFMailComposeResultSent == result);
    }
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result;
{
    [controller dismissViewControllerAnimated:YES completion:nil];
    if (mailSentBlock){
        mailSentBlock(MessageComposeResultSent == result);
    }
}

- (MFMessageComposeViewController *)composeMessage:(NSString *)message;
{
	return [self composeMultipleMessageTo:nil withMessage:message completion:nil];
}

- (MFMessageComposeViewController *)composeMessageTo:(NSString *)recipient withMessage:(NSString *)message;
{
	return [self composeMultipleMessageTo:@[recipient] withMessage:message completion:nil];
}

- (MFMessageComposeViewController *)composeMultipleMessageTo:(NSArray *)recipients withMessage:(NSString *)message completion:(MailSentBlock)block
{
    mailSentBlock = block;
    
	if ([MFMessageComposeViewController canSendText]) {
        UIViewController *rootController = [VMComposeManager rootController];
        MFMessageComposeViewController *controller = [[MFMessageComposeViewController alloc] init];
        controller.messageComposeDelegate = self;
        controller.body = message;
        controller.recipients = recipients;
		
        [rootController presentViewController:controller animated:NO completion:nil];
		
        return controller;
    } else {
		[[VMFlashManager sharedManager] showFlash:@"This device can't be used for composing a message"];
	}
    
    if (block){
        block(NO);
    }
    
    return nil;
}

- (MFMessageComposeViewController *)composeMultipleMessageTo:(NSArray *)recipients withMessage:(NSString *)message andAttachment:(NSData *)attachment mime:(NSString *)mime name:(NSString *)name completion:(MailSentBlock)block
{
    mailSentBlock = block;
    
    if ([MFMessageComposeViewController canSendText]) {
        UIViewController *rootController = [VMComposeManager rootController];
        MFMessageComposeViewController *controller = [[MFMessageComposeViewController alloc] init];
        controller.messageComposeDelegate = self;
        controller.body = message;
        controller.recipients = recipients;
        [controller addAttachmentData:attachment typeIdentifier:mime filename:name];
        
        [rootController presentViewController:controller animated:NO completion:nil];
        
        return controller;
    } else {
        [[VMFlashManager sharedManager] showFlash:@"This device can't be used for composing a message"];
    }
    
    if (block){
        block(NO);
    }
    
    return nil;
}


@end
