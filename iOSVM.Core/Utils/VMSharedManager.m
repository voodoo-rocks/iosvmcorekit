//
//  VMSharedManager.m
//  iBeacon Demo
//
//  Created by Alexander Kryshtalev on 04/01/14.
//  Copyright (c) 2014 Voodoo Mobile. All rights reserved.
//

#import "VMSharedManager.h"

@interface VMSharedManagersCollector : NSObject

+ (void)setManager:(VMSharedManager *)manager forKey:(NSString *)key;
+ (VMSharedManager *)managerForKey:(NSString *)key;

@end

@implementation VMSharedManagersCollector

static NSMutableDictionary *managers;

+ (void)setManager:(VMSharedManager *)manager forKey:(NSString *)key;
{
	if (!managers) {
		managers = [NSMutableDictionary dictionary];
	}
	
	[managers setObject:manager forKey:key];
}

+ (VMSharedManager *)managerForKey:(NSString *)key;
{
	return [managers objectForKey:key];
}

@end


@implementation VMSharedManager

+ (instancetype)sharedManager;
{
	VMSharedManager *manager = (VMSharedManager *)[VMSharedManagersCollector managerForKey:NSStringFromClass([self class])];
	
	if (!manager) {
		manager = [[self alloc]init];
		[VMSharedManagersCollector setManager:manager forKey:NSStringFromClass([manager class])];
	}
	
	return manager;
}

@end
