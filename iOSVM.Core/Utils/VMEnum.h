//
//  VMEnum.h
//  iOSVM.Core
//
//  Created by Dmitry Tihonov on 23.10.14.
//  Copyright (c) 2014 Voodoo Mobile. All rights reserved.
//

#import "VMEntity.h"

@interface VMEnum : VMEntity
{
    NSArray *names;
}

@property (assign, nonatomic) NSUInteger typeId;
@property (copy, nonatomic) NSString *name;

@property (copy, readonly) NSString *primaryKey;

@property BOOL skipMissingValues;

@end
