//
//  VMDynamicObject.h
//  iOSVM.Core
//
//  Created by Alexander Kryshtalev on 30/07/14.
//  Copyright (c) 2014 Voodoo Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VMDynamicObject : NSObject <NSCopying>
{
	NSDictionary *properties;
}

- (id)initWithDictionary:(NSDictionary *)dictionary;
- (id)initFromUserDefaultsForKey:(NSString *)key;

- (void)serializeToUserDefaultsForKey:(NSString *)key;
- (id)copyWithZone:(NSZone *)zone;

- (NSDictionary *)dictionaryRepresentation;
- (void)setDictionaryRepresentation:(NSDictionary *)representation;

- (NSDictionary *)arrayClasses;

+ (BOOL)verbose;
+ (void)setVerbose:(BOOL)verbose;

- (BOOL)escapeParameters;

@end
