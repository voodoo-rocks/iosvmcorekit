//
//  VMBlocks.h
//  Cash-a-Lot
//
//  Created by Alexander Kryshtalev on 20/01/14.
//  Copyright (c) 2014 Voodoo Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef void(^VMCompletionBlock)(BOOL succeeded, NSError *error);
typedef void(^VMStringBlock)(NSString *stringValue);
typedef void(^VMBooleanBlock)(BOOL value);
typedef void(^VMArrayBlock)(NSArray *);
typedef void(^VMDataBlock)(NSData *);
typedef void(^VMVoidBlock)();
typedef void(^VMIntegerBlock)(NSInteger intValue);
typedef void(^VMNumberBlock)(NSNumber *number);
typedef void(^VMDateBlock)(NSDate *date);
typedef void(^VMIdBlock)(id value);
typedef void(^VMImageBlock)(UIImage *image);
typedef void(^VMDictionaryBlock)(NSDictionary *dictionary);