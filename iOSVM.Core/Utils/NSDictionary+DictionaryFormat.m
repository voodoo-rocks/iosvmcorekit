//
//  NSString+DictionaryFormat.m
//  iOSVM.Core
//
//  Created by Alice on 16/11/2016.
//  Copyright © 2016 Voodoo Mobile. All rights reserved.
//

#import "NSDictionary+DictionaryFormat.h"

@implementation NSDictionary (DictionaryFormat)

- (NSString *)printBeautifulDictionary
{
    return [self printBeautifulDictionary:self indent:0];
}

- (NSString *)printBeautifulDictionary:(NSDictionary *)dictionary indent:(NSInteger)indent
{
    NSMutableString *result = @"".mutableCopy;
    [result appendString:[NSString stringWithFormat:@"%@{\n", [self createIndentation:indent]]];
    
    for(NSString *key in dictionary.allKeys) {
        
        id value = [dictionary valueForKey:key];
        [result appendString:[NSString stringWithFormat:@"%@\"%@\":", [self createIndentation:indent+1], key]];
        
        if([value isKindOfClass:[NSDictionary class]]) {
            [result appendString:@"\n"];
            [result appendString:[self printBeautifulDictionary:value indent:indent+1]];
        }
        
        else if([value isKindOfClass:[NSArray class]]) {
            NSArray *array = value;
            if(!array.count) {
                [result appendString:@" []"];
            }
            else {
                [result appendString:@" [\n"];
                for(int i = 0; i < array.count; ++i) {
                    if([array[i] isKindOfClass:[NSDictionary class]]) {
                        [result appendString:[self printBeautifulDictionary:array[i] indent:indent+2]];
                    } else {
                        [result appendString:[NSString stringWithFormat:@"%@%@", [self createIndentation:indent+2], [array[i] description]]];
                    }
                    [result appendString:@",\n"];
                }
                [self removeLastCommaInString:&result];
                [result appendString:[NSString stringWithFormat:@"%@]", [self createIndentation:indent+1]]];
            }
        }
        
        else if([value isKindOfClass:[NSString class]]) {
            [result appendString:[NSString stringWithFormat:@" \"%@\"", [self printVersion:value]]];
        }
        
        else if([value isKindOfClass:[NSNull class]]) {
            [result appendString:@" null"];
        }
        
        else {
            [result appendString:[NSString stringWithFormat:@" %@", [value description]]];
        }
        
        [result appendString:@",\n"];
    }
    
    [self removeLastCommaInString:&result];
    [result appendString:[NSString stringWithFormat:@"%@}", [self createIndentation:indent]]];
    return result;
}

- (NSString *)createIndentation:(NSInteger)indent
{
    NSMutableString *result = @"".mutableCopy;
    for(int i = 0; i < indent; ++i) {
        [result appendString:@"\t"];
    }
    return result;
}

- (NSString *)printVersion:(NSString *)string
{
    int maxCharCount = 200;
    if(string.length > maxCharCount) {
        NSString *newString = [string substringToIndex:maxCharCount];
        string = [newString stringByAppendingString:@"<...>"];
    }
    return string;
}

- (void)removeLastCommaInString:(NSMutableString **)string
{
    NSMutableString *res = *string;
    *string = [res substringToIndex:res.length - 2].mutableCopy;
    [*string appendString:@"\n"];
}

@end
