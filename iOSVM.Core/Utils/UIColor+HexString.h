//
//  VMColor.h
//  NueTune
//
//  Created by Alexander Kryshtalev on 06.11.13.
//  Copyright (c) 2013 Voodoo Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (HexString)

+ (UIColor *)colorWithHexCode:(NSUInteger)rgbValue withAlpha:(float)alpha;
+ (UIColor *)colorWithHexString:(NSString *)hexString withAlpha:(float)alpha;

+ (UIColor *)colorWithHexCode:(NSUInteger)rgbValue;
+ (UIColor *)colorWithHexString:(NSString *)hexString;

+ (UIColor *)makeItDarkerThan:(UIColor *)color;

@end
