//
//  VMAuthSessionManager.m
//  iOSVM.Core
//
//  Created by Alexander Kryshtalev on 19/06/14.
//  Copyright (c) 2014 Voodoo Mobile. All rights reserved.
//

#import "VMAuthSessionManager.h"

@implementation VMAuthSessionManager

- (id)init
{
	self = [super init];
	
	token = [[VMToken alloc] initFromUserDefaultsForKey:@"VMSessionManager.token"];
	
	return self;
}

- (VMToken *)token
{
	return token;
}

- (void)setToken:(VMToken *)newToken
{
	token = newToken;
	[token serializeToUserDefaultsForKey:@"VMSessionManager.token"];
}

- (BOOL)isAuthorized
{
	return token != nil;
}

- (void)logOut;
{
    token = nil;
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"VMSessionManager.token"];
}

@end
