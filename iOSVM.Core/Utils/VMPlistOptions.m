//
//  VMPlistOptions.m
//  DanceMusicUsa
//
//  Created by Alexander Kryshtalev on 03/01/14.
//  Copyright (c) 2014 Voodoo Mobile. All rights reserved.
//

#import "VMPlistOptions.h"
#import "VMPropertyUtils.h"

static VMPlistOptions *options;

@implementation VMPlistOptions

- (id)initWithOptionsFile:(NSString *)filename;
{
    NSMutableDictionary *dictionary = [[NSDictionary alloc] initWithContentsOfFile:
                                       [[NSBundle mainBundle] pathForResource:filename ofType:@"plist"]].mutableCopy;
    [dictionary addEntriesFromDictionary:[[NSDictionary alloc] initWithContentsOfFile:
                                          [[NSBundle mainBundle] pathForResource:@"Shared" ofType:@"plist"]]];
    
    if (dictionary) {
        self = [super initWithDictionary:dictionary];
        optionsFile = filename;
        return self;
    } else {
        @throw [NSException exceptionWithName:@"" reason:@"Options file not found" userInfo:nil];
    }
    
    return nil;
}

+ (instancetype)sharedOptions;
{
    if (!options) {
        NSString * const defaultOptionsFile = @"Options";
        options = [[self alloc] initWithOptionsFile:defaultOptionsFile];
    }
    return options;
}

@end
