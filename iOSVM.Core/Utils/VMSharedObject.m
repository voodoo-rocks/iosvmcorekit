//
//  VMSharedObject.m
//  iOSVM.Core
//
//  Created by Alexander Kryshtalev on 30/10/14.
//  Copyright (c) 2014 Voodoo Mobile. All rights reserved.
//

#import "VMSharedObject.h"

@interface VMSharedObjectsCollector : NSObject

+ (void)setObject:(VMSharedObject *)object forKey:(NSString *)key;
+ (VMSharedObject *)objectForKey:(NSString *)key;

@end

@implementation VMSharedObjectsCollector

static NSMutableDictionary *objects;

- (id)init
{
	return [super init];
}

+ (void)setObject:(VMSharedObject *)object forKey:(NSString *)key;
{
	if (!objects) {
		objects = [NSMutableDictionary dictionary];
	}
	
	[objects setObject:object forKey:key];
}

+ (VMSharedObject *)objectForKey:(NSString *)key;
{
	return [objects objectForKey:key];
}

@end


@implementation VMSharedObject

+ (instancetype)shared;
{
	VMSharedObject *object = (VMSharedObject *)[VMSharedObjectsCollector objectForKey:NSStringFromClass([self class])];
	
	if (!object) {
		object = [[self alloc]init];
		[VMSharedObjectsCollector setObject:object forKey:NSStringFromClass([object class])];
	}
	
	return object;
}

@end

