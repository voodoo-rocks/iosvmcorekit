//
//  VMGraphicUtils.h
//  Ours
//
//  Created by Alexander Kryshtalev on 12.12.13.
//  Copyright (c) 2013 Voodoo Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UIImage (VMImageUtils)

- (UIImage *)cropImageFromRect:(CGRect)rect;
- (UIImage *)scaleImageToSize:(CGSize)size;
- (UIImage *)scaleImageToWidth:(float)width;
- (UIImage *)blur;
- (UIImage *)paintImageWithColor:(UIColor *)color;
- (UIImage *)applyGradientWithStartColor:(UIColor *)startColor endColor:(UIColor *)endColor;

@end
