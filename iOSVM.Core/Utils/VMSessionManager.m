//
//  VMSessionManager.m
//  iOSVM.Core
//
//  Created by Alexander Kryshtalev on 19/06/14.
//  Copyright (c) 2014 Voodoo Mobile. All rights reserved.
//

#import "VMSessionManager.h"

@implementation VMSessionManager

- (BOOL)firstTimeLaunch
{
    BOOL hasNoDefaultValue = nil == [[NSUserDefaults standardUserDefaults] objectForKey:@"VMSessionManager.firstTimeLaunch"];
	BOOL firstTime = hasNoDefaultValue || [[NSUserDefaults standardUserDefaults] boolForKey:@"VMSessionManager.firstTimeLaunch"];
	
	if (firstTime) {
		[[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"VMSessionManager.firstTimeLaunch"];
		[[NSUserDefaults standardUserDefaults] synchronize];
	}
	
	return firstTime;
}

@end
