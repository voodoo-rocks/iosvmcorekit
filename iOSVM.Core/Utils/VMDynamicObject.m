//
//  VMDynamicObject.m
//  iOSVM.Core
//
//  Created by Alexander Kryshtalev on 30/07/14.
//  Copyright (c) 2014 Voodoo Mobile. All rights reserved.
//

#import "VMDynamicObject.h"
#import "VMPropertyUtils.h"
#import "VMEnum.h"
#import "NSString+VMCaseConverter.h"
#import <objc/runtime.h>

static BOOL dynamicObjectVerbose;

@implementation VMDynamicObject

- (id)init
{
	self = [super init];
	properties = [VMPropertyUtils propertiesForClass:[self class] recursively:YES];
	
	return self;
}

- (id)initWithDictionary:(NSDictionary *)dictionary
{
	self = [self init];
	self.dictionaryRepresentation = dictionary;
	return self;
}

- (id)initFromUserDefaultsForKey:(NSString *)key;
{
	NSDictionary *dictionary = [[NSUserDefaults standardUserDefaults] dictionaryForKey:key];
	if (!dictionary) {
		return nil;
	}
	
	return [self initWithDictionary:dictionary];
}

- (id)copyWithZone:(NSZone *)zone;
{
	return [[self.class allocWithZone:zone] initWithDictionary:self.dictionaryRepresentation.copy];
}

- (void)serializeToUserDefaultsForKey:(NSString *)key;
{
	[[NSUserDefaults standardUserDefaults] setObject:self.dictionaryRepresentation forKey:key];
	[[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)setDictionaryRepresentation:(NSDictionary *)representation
{
	[self setValuesForKeysWithDictionary:representation];
}

- (void)setValue:(id)value forKey:(NSString *)key
{
    NSString *camelKey = [key camelCaseFormattedString];
    
	Class propertyClass = NSClassFromString([properties objectForKey:camelKey]);
	
	if ([propertyClass isSubclassOfClass:[VMDynamicObject class]]) {
        if ([propertyClass isSubclassOfClass:[VMEnum class]]){
            VMEnum *enumObject = [[propertyClass alloc] init];
            [enumObject setValue:value forKey:enumObject.primaryKey];
            value = enumObject;
        } else {
            value = [[propertyClass alloc] initWithDictionary:value];
        }
	}
	
	if ([[NSArray class] isSubclassOfClass:propertyClass]) {
		Class arrayClass = [self.arrayClasses objectForKey:camelKey];
		if (arrayClass) {
			value = [self setArray:(NSArray *)value ofClass:arrayClass];
		} else {
			[NSException raise:@"Incorrect array type exception"
						format:@"Type for %@.%@ not found. Please set up arrayClasses properly.", NSStringFromClass(self.class), camelKey];
		}
    }
    
    if(!value) { // avoid nulls in integral types
        return;
    }
	
	[super setValue:value forKey:camelKey];
}

- (NSArray *)setArray:(NSArray *)arrayOfValues ofClass:(Class)arrayClass
{
	NSMutableArray *results = [NSMutableArray new];
	for (id value in arrayOfValues) {
		id result = value;
		if ([arrayClass isSubclassOfClass:[VMDynamicObject class]]) {
			result = [[arrayClass alloc] initWithDictionary:value];
		}
		[results addObject:result];
	}
	
	return results;
}

- (void)setValue:(id)value forUndefinedKey:(NSString *)key
{
	
}

- (id)valueForUndefinedKey:(NSString *)key
{
	return nil;
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary* result = [[NSMutableDictionary alloc] init];
	
	for (NSString *propertyName in properties.allKeys) {
		id value = [self valueForKey:propertyName];
		
		if (value == nil || [value isKindOfClass:[NSDate class]]) {
			continue;
		}
		
        if ([value isKindOfClass:[VMDynamicObject class]]){
            if ([value isKindOfClass:[VMEnum class]]){
                value = [value valueForKey:((VMEnum *)value).primaryKey];
            } else {
                value = [value dictionaryRepresentation];
            }
        }
		
		if ([value isKindOfClass:[NSArray class]]){
            NSMutableArray *array = [[NSMutableArray alloc] initWithCapacity:[value count]];
            for (id arrayItem in value) {
                if ([arrayItem isKindOfClass:[VMDynamicObject class]]){
                    [array addObject:[arrayItem dictionaryRepresentation]];
                } else {
                    [array addObject:arrayItem];
                }
            }
			value = array.copy;
        }
		
		[result setObject:value forKey:self.escapeParameters ? [propertyName underscoreFormattedString] : propertyName];
    }
    
	return result;
}

- (NSDictionary *)arrayClasses;
{
	return @{ };
}

+ (BOOL)verbose;
{
	return dynamicObjectVerbose;
}

+ (void)setVerbose:(BOOL)verbose;
{
	dynamicObjectVerbose = verbose;
}

- (BOOL)escapeParameters
{
    return NO;
}

@end
