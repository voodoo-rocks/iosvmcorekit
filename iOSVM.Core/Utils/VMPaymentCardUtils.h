//
//  VMPaymentCardUtils.h
//  iOSVM.Core
//
//  Created by Alexander Kryshtalev on 13/11/14.
//  Copyright (c) 2014 Voodoo Mobile. All rights reserved.
//

#import "VMSharedObject.h"

typedef enum {
	VMPaymentCardIncorrect,
	VMPaymentCardVisa,
	VMPaymentCardMastercard,
	VMPaymentCardAmEx,
	VMPaymentCardDinersClub,
	VMPaymentCardDiscover,
	VMPaymentCardJCB,
} VMPaymentCard;

@interface VMPaymentCardUtils : VMSharedObject

- (BOOL)isCorrect:(NSString *)cardNumber;
- (VMPaymentCard)detectPaymentCard:(NSString *)cardNumber;

@end
