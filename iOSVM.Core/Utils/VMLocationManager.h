//
//  VMLocationManager.h
//  iOSVM
//
//  Created by Dmitry Tihonov on 05.07.13.
//  Copyright (c) 2013 Voodoo Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import "VMSharedManager.h"
#import "VMBlocks.h"

typedef void(^UpdateLocationBlock)(CLLocation *newLocation);
typedef void(^TrackLocationBlock)();
typedef void(^AddressBlock)(NSString *address);
typedef void(^LocationBlock)(NSArray* placemarks, NSError* error);
typedef void(^PermissionStatusBlock)(CLAuthorizationStatus);

@interface VMLocationManager : VMSharedManager <CLLocationManagerDelegate>
{
    CLLocationManager *locationManager;
    UpdateLocationBlock changeLocationBlock;
    PermissionStatusBlock didChangePermissionStatus;
    
    BOOL isRecursivelyUpdating;
    
    CLGeocoder *geocoder;
    CLPlacemark *placemark;
}

@property (copy, nonatomic) CLLocation *lastKnownLocation;

- (void)trackDistance:(NSInteger)distance fromLocation:(CLLocation *)location completion:(TrackLocationBlock)block;
- (void)addressForLocation:(CLLocation *)location withCompletition:(AddressBlock)block;
- (void)currentLocationWithCompletion:(UpdateLocationBlock)block isRecursively:(BOOL)recursively;
- (void)randomLocationInRadius:(float)radius withCompletition:(UpdateLocationBlock)block isRecursively:(BOOL)recursively;
- (void)currentLocationWithCompletion:(UpdateLocationBlock)block isRecursively:(BOOL)recursively withDistanceDelta:(CLLocationDistance)distance;
- (void)addressForCurrentLocationWithCompletition:(AddressBlock)block;
- (void)locationForAddress:(NSString *)address withCompletion:(LocationBlock)block;
- (void)regionForAddress:(NSString *)address withCompletion:(VMIdBlock)callback;
- (void)requestForAlways:(BOOL)always withCallback:(PermissionStatusBlock)block;
- (void)stopUpdating;
- (BOOL)permissionGranted;
- (BOOL)permissionRestricted;

@end
