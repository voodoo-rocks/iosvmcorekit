//
//  NSDate+VMDate.h
//  GameOn
//
//  Created by Dmitry Tihonov on 16.07.13.
//  Copyright (c) 2013 Voodoo Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (VMDate)

+ (NSDate *)utcDate;
+ (NSDate *)utcDateFromString:(NSString *)dateString;
+ (NSDate *)localDateFromString:(NSString *)dateString;
+ (NSDate *)utcDateFromString:(NSString *)dateString withFormat:(NSString *)format;
+ (NSDate *)localDateFromString:(NSString *)dateString withFormat:(NSString *)format;
+ (NSDate *)dateFromString:(NSString *)dateString withFormat:(NSString *)format withTimezone:(NSTimeZone *)timezone;

- (NSDate *)beginningOfDay;
- (NSDate *)addOneDay;

- (NSString *)utcDateString;
- (NSString *)localDateString;
- (NSString *)utcDateStringWithFormat:(NSString *)format;
- (NSString *)localDateStringWithFormat:(NSString *)format;
- (NSString *)utcDateStringWithFormat:(NSString *)format withLocale:(NSLocale *)locale;
- (NSString *)localDateStringWithFormat:(NSString *)format withLocale:(NSLocale *)locale;
- (NSString *)dateStringWithFormat:(NSString *)format withTimezone:(NSTimeZone *)timeZone;

@property (readonly) NSInteger hour;
@property (readonly) NSInteger minute;
@property (readonly) NSInteger second;
@property (readonly) NSInteger day;
@property (readonly) NSInteger weekDay;
@property (readonly) NSInteger month;
@property (readonly, copy) NSString *monthName;
@property (readonly) NSInteger year;

@end
