//
//  VMConstants.h
//  iOSVM.Core
//
//  Created by Alexander Kryshtalev on 27/06/14.
//  Copyright (c) 2014 Voodoo Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VMConstants : NSObject

+ (float)defaultJpegCompression;
+ (float)defaultAnimationDuration;
+ (float)flashAnimationDurationLong;
+ (float)flashAnimationDurationShort;

@end
