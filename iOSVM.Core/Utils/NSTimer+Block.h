//
//  NSTimer+Block.h
//  iOSVM.Core
//
//  Created by Olesya Kondrashkina on 10/08/16.
//  Copyright © 2016 Voodoo Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VMBlocks.h"

@interface NSTimer (Block)

+ (NSTimer *)scheduledTimerWithTimeInterval:(NSTimeInterval)interval block:(VMVoidBlock)block repeats:(BOOL)yesOrNo;

@end
