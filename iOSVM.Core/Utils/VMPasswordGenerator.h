//
//  VMPasswordGenerator.h
//  iOSVM.Core
//
//  Created by Alexander Kryshtalev on 02/07/14.
//  Copyright (c) 2014 Voodoo Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VMPasswordGenerator : NSObject

+ (NSString *)generate;
+ (NSString *)generateOfLength:(NSUInteger)length;

@end
