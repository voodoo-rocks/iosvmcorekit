//
//  NSTimer+Block.m
//  iOSVM.Core
//
//  Created by Olesya Kondrashkina on 10/08/16.
//  Copyright © 2016 Voodoo Mobile. All rights reserved.
//

#import "NSTimer+Block.h"

@interface TimerListener : NSObject

- (void)onTimer:(NSTimer *)timer;

@end

@implementation TimerListener

- (void)onTimer:(NSTimer *)timer;
{
    if (timer.userInfo) {
        ((VMVoidBlock)timer.userInfo)();
    }
}

@end

@implementation NSTimer (Block)

+ (NSTimer *)scheduledTimerWithTimeInterval:(NSTimeInterval)interval block:(VMVoidBlock)block repeats:(BOOL)yesOrNo;
{
    return [NSTimer scheduledTimerWithTimeInterval:interval target:[TimerListener new] selector:@selector(onTimer:) userInfo:block repeats:yesOrNo];
}

@end
