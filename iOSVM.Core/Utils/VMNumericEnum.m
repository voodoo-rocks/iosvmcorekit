//
//  VMNumericEnum.m
//  iOSVM.Core
//
//  Created by Dmitry Tihonov on 23.10.14.
//  Copyright (c) 2014 Voodoo Mobile. All rights reserved.
//

#import "VMNumericEnum.h"

@implementation VMNumericEnum

- (NSString *)primaryKey
{
    return @"typeId";
}

@end
