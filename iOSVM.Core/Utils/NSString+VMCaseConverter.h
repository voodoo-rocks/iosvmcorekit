//
//  NSString+VMCaseConverter.h
//  GameOn
//
//  Created by Dmitry Tihonov on 16.07.13.
//  Copyright (c) 2013 Voodoo Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (VMCaseConverter)

- (NSString *)camelCaseFormattedString;
- (NSString *)underscoreFormattedString;

@end
