//
//  VMConstants.m
//  iOSVM.Core
//
//  Created by Alexander Kryshtalev on 27/06/14.
//  Copyright (c) 2014 Voodoo Mobile. All rights reserved.
//

#import "VMConstants.h"

@implementation VMConstants

+ (float)defaultAnimationDuration;
{
	return 0.3; // Default animation
}

+ (float)flashAnimationDurationLong;
{
    return 3.5;
}

+ (float)flashAnimationDurationShort;
{
    return 2.0;
}

+ (float)defaultJpegCompression;
{
	return 0.85;
}

@end
