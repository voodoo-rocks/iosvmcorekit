//
//  VMPlistOptions.h
//  DanceMusicUsa
//
//  Created by Alexander Kryshtalev on 03/01/14.
//  Copyright (c) 2014 Voodoo Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VMDynamicObject.h"

@interface VMPlistOptions : VMDynamicObject
{
	NSString *optionsFile;
}

- (id)initWithOptionsFile:(NSString *)filename;

+ (instancetype)sharedOptions;

@end
