//
//  VMEnum.m
//  iOSVM.Core
//
//  Created by Dmitry Tihonov on 23.10.14.
//  Copyright (c) 2014 Voodoo Mobile. All rights reserved.
//

#import "VMEnum.h"

@implementation VMEnum

- (NSString *)primaryKey
{
    [NSException raise:[NSString stringWithFormat:@"Primary key not defined for %@", NSStringFromClass([self class])] format:@""];
    return nil;
}

- (void)setTypeId:(NSUInteger)typeId
{
    if (names.count <= typeId){
        [NSException raise:[NSString stringWithFormat:@"%@ not nave type with index %lud", NSStringFromClass([self class]), (unsigned long)typeId] format:@""];
    } else {
        _typeId = typeId;
        _name = [names objectAtIndex:typeId];
    }
}

- (void)setName:(NSString *)name
{
    if ([names indexOfObject:name] == NSNotFound){
        NSString *errorString = [NSString stringWithFormat:@"Type '%@' not defined for %@", name, NSStringFromClass([self class])];
        if (self.skipMissingValues){
            NSLog(@"%@", errorString);
        } else {
            [NSException raise:errorString format:@""];
        }
    } else {
        _name = name;
        _typeId = [names indexOfObject:name];
    }
}

@end
