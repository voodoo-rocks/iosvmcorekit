//
//  PropertyUtils.h
//  JSONObject
//
//  Created by Alexander Kryshtalev on 24.09.12.
//  Copyright (c) 2012 Voodoo Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VMPropertyUtils : NSObject

+ (NSDictionary *)propertiesForClass:(Class)klass recursively:(BOOL)recursively;

@end
