//
//  VMDbEntity.m
//  iOSVM.Core
//
//  Created by Alexander Kryshtalev on 11/12/14.
//  Copyright (c) 2014 Voodoo Mobile. All rights reserved.
//

#import "VMDbEntity.h"

@implementation VMDbEntity

- (id)init
{
	self = [super init];
	
	if (self) {
		self.id = -1;
	}
	
	return self;
}

@end
