//
//  VMDbEntity.h
//  iOSVM.Core
//
//  Created by Alexander Kryshtalev on 11/12/14.
//  Copyright (c) 2014 Voodoo Mobile. All rights reserved.
//

#import "VMEntity.h"

@interface VMDbEntity : VMEntity

@property NSInteger id;

@end
