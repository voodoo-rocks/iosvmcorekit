//
//  VMDataSource.h
//  Ovenbot
//
//  Created by Alexander Kryshtalev on 12.10.13.
//  Copyright (c) 2013 Voodoo Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VMSharedObject.h"

@class VMDataSource, VMEntity;

typedef void(^VMDataSourceFetchCallback)(VMDataSource *source, NSArray *fetched, NSError *error);

@interface VMDataSource : VMSharedObject

- (void)fetch:(VMDataSourceFetchCallback)callback;

@end
