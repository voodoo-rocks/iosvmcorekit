//
//  VMDataSource.m
//  Ovenbot
//
//  Created by Alexander Kryshtalev on 12.10.13.
//  Copyright (c) 2013 Voodoo Mobile. All rights reserved.
//

#import "VMDataSource.h"

@implementation VMDataSource

- (void)fetch:(VMDataSourceFetchCallback)callback;
{
	[NSException raise:@"Please override fetch: in descendant classes" format:@""];
}

@end