//
//  ImagesHelper.h
//  FshBowlBusiness
//
//  Created by Mihail Kirillov on 3/6/15.
//
//

#import "VMStyleable.h"
#import "VMSharedObject.h"

@interface VMStyledViewsHelper : VMSharedObject

@property (copy) NSArray *textColors;
@property (copy) NSArray *borderColors;
@property (copy) NSArray *backgroundColors;
@property (copy) NSArray *leftImagesColors;
@property (copy) NSArray *rightImages;

@property (copy, nonatomic) NSNumber *textInsetForFieldWithImage;
@property (copy, nonatomic) NSNumber *horizontalContentInset;
@property (copy, nonatomic) NSNumber *verticalContentInset;
@property (copy, nonatomic) NSNumber *cornerRadius;
@property (copy, nonatomic) NSNumber *borderWidth;

@property (copy, nonatomic) UIColor *cursorColor;

- (UIColor *)textColorForState:(FieldState)state;
- (UIColor *)borderColorForState:(FieldState)state;
- (UIColor *)backgroundColorForState:(FieldState)state;
- (UIColor *)leftImagesColorForState:(FieldState)state;
- (UIImage *)rightImageForState:(FieldState)state;

@end
