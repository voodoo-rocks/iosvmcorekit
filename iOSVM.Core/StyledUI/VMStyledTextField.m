//
//  CustomTextField.m
//  FshBowlBusiness
//
//  Created by Mihail Kirillov on 3/2/15.
//
//

#import "VMStyledTextField.h"
#import "VMStyledTextFieldStateManager.h"
#import "VMStyledViewsHelper.h"
#import "UIImage+VMImageUtils.h"

@implementation VMStyledTextField

@synthesize fieldState, leftSideImage, rightSideImage, didChangeQuery, didClearText, didEndEditing, didBeginEditing;

#pragma mark textfield methods

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    styleManager = [VMStyledTextFieldStateManager new];
    styleManager.styledField = self;
    
    if(!self.imageHelper) {
        self.imageHelper = [VMStyledViewsHelper shared];
    }
    
    self.rightViewMode = UITextFieldViewModeUnlessEditing;
    self.leftViewMode = UITextFieldViewModeAlways;
    self.viewCornerRadius = self.imageHelper.cornerRadius ? self.imageHelper.cornerRadius.floatValue : CGRectGetHeight(self.frame) / 2;
    self.borderWidth = self.imageHelper.borderWidth.floatValue;
    self.tintColor = self.imageHelper.cursorColor;
    
    self.fieldState = FieldStateEmpty;
}

- (void)setText:(NSString *)text
{
    [super setText:text];
    [self setFieldState:text.length > 0];
}

- (CGRect)leftViewRectForBounds:(CGRect)bounds;
{
    CGSize imageSize = self.leftView.frame.size;
    float yOffset = bounds.size.height / 2 - imageSize.height / 2;
    float xOffset = self.imageHelper.horizontalContentInset.floatValue;
    return CGRectMake(xOffset, yOffset, imageSize.width, imageSize.height);
}

- (CGRect)rightViewRectForBounds:(CGRect)bounds;
{
    CGSize imageSize = self.rightView.frame.size;
    float yOffset = bounds.size.height / 2 - imageSize.height / 2;
    float xOffset = bounds.size.width - self.imageHelper.horizontalContentInset.floatValue - imageSize.width;
    return CGRectMake(xOffset, yOffset, imageSize.width, imageSize.height);
}

- (CGRect)textRectForBounds:(CGRect)bounds;
{
    return CGRectInset(bounds, self.leftSideImage ? self.imageHelper.textInsetForFieldWithImage.floatValue : self.imageHelper.horizontalContentInset.floatValue, 0);
}

- (CGRect)editingRectForBounds:(CGRect)bounds {
    return CGRectInset(bounds, self.leftSideImage ? self.imageHelper.textInsetForFieldWithImage.floatValue : self.imageHelper.horizontalContentInset.floatValue, 0);
}

#pragma mark appearance change

- (void)setFieldState:(FieldState)_fieldState
{
    self.textColor = [self.imageHelper textColorForState:_fieldState];
    self.backgroundColor = [self.imageHelper backgroundColorForState:_fieldState];
    self.borderColor = [self.imageHelper borderColorForState:_fieldState];
    self.rightView = nil;
    
    if (self.rightSideImage) {
        self.rightViewMode = UITextFieldViewModeAlways;
        if (self.rightViewIsClearControl) {
            UIButton *clearButton = [UIButton buttonWithType:UIButtonTypeCustom];
            [clearButton setImage:self.rightSideImage forState:UIControlStateNormal];
            [clearButton setFrame:[[UIImageView alloc] initWithImage:self.rightSideImage].frame];
            [clearButton addTarget:self action:@selector(clearTextField:) forControlEvents:UIControlEventTouchUpInside];
            self.rightView = clearButton;
        } else {
            self.rightView = [[UIImageView alloc] initWithImage:[self.imageHelper rightImageForState:_fieldState]];
        }
    }
    else {
        self.rightViewMode = UITextFieldViewModeNever;
    }
    
    if (self.leftSideImage) {
        self.leftView = [[UIImageView alloc] initWithImage:[self leftImageForState:_fieldState]];
    }
}

- (void)clearTextField:(id)sender
{
    self.text = @"";
    
    if (self.didClearText) {
        self.didClearText();
    }
}

- (UIImage *)leftImageForState:(FieldState)state
{
    UIColor *imageColor = [self.imageHelper leftImagesColorForState:state];
    return [self.leftSideImage paintImageWithColor:imageColor];
}

- (void)focus
{
    [super focus];
    
    self.fieldState = FieldStateInvalid;
}

@end
