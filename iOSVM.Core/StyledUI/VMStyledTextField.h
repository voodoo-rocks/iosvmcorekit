//
//  CustomTextField.h
//  FshBowlBusiness
//
//  Created by Mihail Kirillov on 3/2/15.
//
//

#import "VMTextField.h"
#import "VMStyleable.h"

@class VMStyledTextFieldStateManager, VMStyledViewsHelper;

@interface VMStyledTextField : VMTextField<VMStyleable>
{
    VMStyledTextFieldStateManager *styleManager;
}

@property (strong) VMStyledViewsHelper *imageHelper;

@property BOOL rightViewIsClearControl;

@end
