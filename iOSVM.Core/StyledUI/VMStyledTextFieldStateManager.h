//
//  StyleManager.h
//  FshBowlBusiness
//
//  Created by Mihail Kirillov on 3/11/15.
//
//

#import "VMStyledTextField.h"

@interface VMStyledTextFieldStateManager : NSObject<UITextFieldDelegate>

@property (strong, nonatomic) VMStyledTextField *styledField;

@end
