//
//  StyledTextView.m
//  FshBowlBusiness
//
//  Created by Mihail Kirillov on 3/6/15.
//
//

#import "VMStyledTextView.h"
#import "VMStyledTextViewStateManager.h"
#import "VMStyledViewsHelper.h"
#import "UIImage+VMImageUtils.h"

@implementation VMStyledTextView

@synthesize fieldState, leftSideImage, rightSideImage, didChangeQuery, didClearText, didBeginEditing, didEndEditing;

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    styleManager = [VMStyledTextViewStateManager new];
    styleManager.styledTextView = self;
    
    if(!self.imageHelper) {
        self.imageHelper = [VMStyledViewsHelper shared];
    }
    
    self.viewCornerRadius = self.imageHelper.cornerRadius ? self.imageHelper.cornerRadius.floatValue : CGRectGetHeight(self.frame) / 2;
    self.borderWidth = self.imageHelper.borderWidth.floatValue;
    self.tintColor = self.imageHelper.cursorColor;
    
    self.fieldState = FieldStateEmpty;
    
    placeholderLabel.frame = CGRectMake(self.imageHelper.horizontalContentInset.floatValue, self.imageHelper.verticalContentInset.floatValue, self.frame.size.width, 0);
    [placeholderLabel sizeToFit];
}

- (void)setText:(NSString *)text
{
    [super setText:text];
    self.fieldState = text.length > 0 ? FieldStateFilled : FieldStateEmpty;
}

- (void)setFieldState:(FieldState)_fieldState
{
    fieldState = _fieldState;
    
    self.textColor = [self.imageHelper textColorForState:_fieldState];
    self.backgroundColor = [self.imageHelper backgroundColorForState:_fieldState];
    self.borderColor = [self.imageHelper borderColorForState:_fieldState];
    
    placeholderLabel.hidden = _fieldState;
    
    if (leftSideImage) {
        [self createLeftView:[self leftImageForState:_fieldState]];
    }
    
    if (rightSideImage) {
        [self createRightView:[self.imageHelper rightImageForState:_fieldState]];
    }
    
    self.textContainer.lineFragmentPadding = 0;
    self.textContainerInset = UIEdgeInsetsMake(self.imageHelper.verticalContentInset.floatValue, leftSideImage ? self.imageHelper.textInsetForFieldWithImage.floatValue : self.imageHelper.horizontalContentInset.floatValue, self.imageHelper.verticalContentInset.floatValue, self.imageHelper.horizontalContentInset.floatValue);
}

- (void)createLeftView:(UIImage *)image
{
    if (leftView) {
        [leftView removeFromSuperview];
    }
    
    leftView = [[UIImageView alloc] initWithImage:image];
    
    CGSize imageSize = image.size;
    float yOffset = self.imageHelper.horizontalContentInset.floatValue;
    float xOffset = self.viewCornerRadius / 2 + (self.imageHelper.textInsetForFieldWithImage.floatValue - self.imageHelper.horizontalContentInset.floatValue - imageSize.width) / 2;
    leftView.frame = CGRectMake(xOffset, yOffset, imageSize.width, imageSize.height);
    [self addSubview:leftView];
}

- (void)createRightView:(UIImage *)image
{
    if (rightView) {
        [rightView removeFromSuperview];
    }
    
    rightView = [[UIImageView alloc] initWithImage:image];
    
    CGSize imageSize = image.size;
    float yOffset = self.frame.size.height / 2 - imageSize.height / 2;
    float xOffset = self.frame.size.width - self.viewCornerRadius - imageSize.width / 2;
    rightView.frame = CGRectMake(xOffset, yOffset, imageSize.width, imageSize.height);
    [self addSubview:rightView];
}

- (UIImage *)leftImageForState:(FieldState)state
{
    UIColor *imageColor = [self.imageHelper leftImagesColorForState:state];
    return [self.leftSideImage paintImageWithColor:imageColor];
}

@end
