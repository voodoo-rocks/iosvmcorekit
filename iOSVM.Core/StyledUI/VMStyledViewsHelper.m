//
//  ImagesHelper.m
//  FshBowlBusiness
//
//  Created by Mihail Kirillov on 3/6/15.
//
//

#import "VMStyledViewsHelper.h"

@implementation VMStyledViewsHelper

- (UIColor *)textColorForState:(FieldState)state
{
    return self.textColors ? self.textColors[state] : [UIColor blackColor];
}

- (UIColor *)borderColorForState:(FieldState)state
{
    return self.borderColors? self.borderColors[state] : [UIColor clearColor];
}

- (UIColor *)backgroundColorForState:(FieldState)state
{
    return self.backgroundColors? self.backgroundColors[state] : [UIColor whiteColor];
}

- (UIColor *)leftImagesColorForState:(FieldState)state
{
    return self.leftImagesColors? self.leftImagesColors[state] : [UIColor blackColor];
}

- (UIImage *)rightImageForState:(FieldState)state;
{
    return self.rightImages ? self.rightImages[state] : [UIImage new];
}

- (NSNumber *)textInsetForFieldWithImage
{
    return _textInsetForFieldWithImage ? _textInsetForFieldWithImage : @35;
}

- (NSNumber *)horizontalContentInset
{
    return _horizontalContentInset ? _horizontalContentInset : @8;
}

- (NSNumber *)verticalContentInset
{
    return _verticalContentInset ? _verticalContentInset : @5;
}

- (NSNumber *)borderWidth
{
    return _borderWidth ? _borderWidth : @1;
}

- (UIColor *)cursorColor
{
    return _cursorColor ? _cursorColor : [UIColor blackColor];
}


@end
