//
//  StyleManager.m
//  FshBowlBusiness
//
//  Created by Mihail Kirillov on 3/11/15.
//
//

#import "VMStyledTextFieldStateManager.h"

@implementation VMStyledTextFieldStateManager

@synthesize styledField;

- (void)setStyledField:(VMStyledTextField *)_styledField
{
    styledField = _styledField;
    styledField.delegate = self;
}

- (BOOL)textFieldShouldClear:(UITextField *)textField;
{
    styledField.fieldState = FieldStateEditing;
    
    if (styledField.didClearText) {
        styledField.didClearText();
    }
    
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string;
{
    NSString *result = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    if (result.length == 0 && styledField.didClearText) {
        styledField.didClearText();
    }
    
    if (![result isEqualToString:styledField.text] && styledField.didChangeQuery) {
        styledField.didChangeQuery(result);
    }
    
    return [(VMTextField *)textField shouldChangeCharactersInRange:range replacementString:string];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField;
{
    styledField.fieldState = FieldStateEditing;
    if (styledField.didBeginEditing) {
        styledField.didBeginEditing ();
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField;
{
    styledField.fieldState = styledField.text.length > 0 ? FieldStateFilled : FieldStateEmpty;
    if (styledField.didEndEditing) {
        styledField.didEndEditing ();
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField;
{
    [styledField resignFirstResponder];
    return YES;
}

@end
