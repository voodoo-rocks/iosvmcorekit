//
//  Styleable.h
//  FshBowlBusiness
//
//  Created by Mihail Kirillov on 3/13/15.
//
//

#import "VMBlocks.h"

typedef NS_ENUM(NSUInteger, FieldState) {
    FieldStateEmpty,
    FieldStateFilled,
    FieldStateEditing,
    FieldStateInvalid,
    FieldStateReady
};

@protocol VMStyleable <NSObject>

@property (copy) VMStringBlock didChangeQuery;
@property (copy) VMVoidBlock didClearText;

@property (copy) VMVoidBlock didBeginEditing;
@property (copy) VMVoidBlock didEndEditing;

@property (assign) FieldState fieldState;

@property (copy) NSString *text;
@property (weak) id delegate;

@property (strong) IBInspectable UIImage *leftSideImage, *rightSideImage;

@end
