//
//  VMTextViewStateManager.h
//  Ovenbot
//
//  Created by Alice on 20/01/16.
//  Copyright © 2016 Voodoo Mobile. All rights reserved.
//

#import "VMStyledTextView.h"

@interface VMStyledTextViewStateManager : NSObject<UITextViewDelegate>

@property (strong, nonatomic) VMStyledTextView *styledTextView;

@end
