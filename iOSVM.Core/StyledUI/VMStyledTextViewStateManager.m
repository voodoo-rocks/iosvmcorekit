//
//  VMTextViewStateManager.m
//  Ovenbot
//
//  Created by Alice on 20/01/16.
//  Copyright © 2016 Voodoo Mobile. All rights reserved.
//

#import "VMStyledTextViewStateManager.h"

@implementation VMStyledTextViewStateManager

@synthesize styledTextView;

- (void)setStyledTextView:(VMStyledTextView *)_styledTextView
{
    styledTextView = _styledTextView;
    styledTextView.delegate = self;
}

- (void)clearTextView;
{
    styledTextView.fieldState = FieldStateEditing;
    
    if (styledTextView.didClearText) {
        styledTextView.didClearText();
    }
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text;
{
    NSString *result = [textView.text stringByReplacingCharactersInRange:range withString:text];
    
    if (result.length == 0 && styledTextView.didClearText) {
        styledTextView.didClearText();
    }
    
    if (result.length > textView.text.length && styledTextView.didChangeQuery) {
        styledTextView.didChangeQuery(result);
    }
    
    return YES;
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    styledTextView.fieldState = FieldStateEditing;
    if (styledTextView.didBeginEditing) {
        styledTextView.didBeginEditing ();
    }
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    styledTextView.fieldState = styledTextView.text.length > 0 ? FieldStateFilled : FieldStateEmpty;
    if (styledTextView.didEndEditing) {
        styledTextView.didEndEditing ();
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField;
{
    [styledTextView resignFirstResponder];
    return YES;
}

@end
