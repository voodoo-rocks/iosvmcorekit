//
//  StyledTextView.h
//  FshBowlBusiness
//
//  Created by Mihail Kirillov on 3/6/15.
//
//

#import "VMTextView.h"
#import "VMStyleable.h"

@class VMStyledTextViewStateManager, VMStyledViewsHelper;

@interface VMStyledTextView : VMTextView<VMStyleable>
{
    UIImageView *leftView, *rightView;
    VMStyledTextViewStateManager *styleManager;
}

@property (strong) VMStyledViewsHelper *imageHelper;

@end
