//
//  VMPersistentEntity.m
//  iOSVM.Core
//
//  Created by Alexander Kryshtalev on 17/06/14.
//  Copyright (c) 2014 Voodoo Mobile. All rights reserved.
//

#import "VMEntity.h"
#import "VMEntityCollection.h"
#import "XMLDictionary.h"

@implementation VMEntity

- (id)initWithXml:(NSString *)xml;
{
    [XMLDictionaryParser sharedInstance].attributesMode = XMLDictionaryAttributesModeDiscard;
    NSDictionary *dictionary = [NSDictionary dictionaryWithXMLString:xml];
    if (dictionary) {
        return [self initWithDictionary:dictionary];
    }
    
    return nil;
}

- (id)initWithJson:(NSString *)json
{
	NSDictionary *dictionary = [self dictionaryFromJson:json];
	if (dictionary) {
		return [self initWithDictionary:dictionary];
	}
	
	return nil;
}

// TODO: move to adapters one day
- (NSDictionary *)dictionaryFromJson:(NSString *)json
{
	if (!json) {
		return nil;
	}
	
	NSError *error = nil;
	NSDictionary *dictionary =
	[NSJSONSerialization JSONObjectWithData:[json dataUsingEncoding:NSUTF8StringEncoding]
									options:NSJSONReadingMutableContainers error:&error];
	
	if (error) {
		NSLog(@"Error parsing JSON for entities: %@", error.localizedDescription);
	}
	
	if (dictionary && [dictionary isKindOfClass:NSDictionary.class]) {
		return dictionary;
	}
	
	return nil;
} 

@end
