//
//  VMPersistentEntityCollection.m
//  iOSVM.Core
//
//  Created by Alexander Kryshtalev on 17/06/14.
//  Copyright (c) 2014 Voodoo Mobile. All rights reserved.
//

#import "VMEntityCollection.h"
#import "VMEntity.h"

@implementation VMEntityCollection

@synthesize entities;

- (id)init;
{
	self = [super init];
	return self;
}

- (id)initWithEntities:(NSArray *)_entities;
{
	self = [self init];
	
	if (self) {
		entities = [NSMutableArray arrayWithArray:_entities];
	}
	
	return self;
	
}

- (id)initEntitiesOf:(id)theClass withArray:(NSArray *)array
{
	self = [self init];
	
	entities = [NSMutableArray array];
	
	for (NSDictionary *dictionary in array) {
		if (![dictionary isKindOfClass:NSDictionary.class]) {
			@throw @"The passed array must be an array of dictionaries like parsed JSON or something";
		}
		
		VMEntity *entity = [[theClass alloc] initWithDictionary:dictionary];
		
		[entities addObject:entity];
	}
	
	return self;
}

- (id)initEntitiesOf:(id)theClass withJson:(NSString *)json
{
	NSError *error = nil;
	NSArray *array = [NSJSONSerialization JSONObjectWithData:[json dataUsingEncoding:NSUTF8StringEncoding]
									options:NSJSONReadingMutableContainers error:&error];
	
	if (error) {
		NSLog(@"Error parsing JSON for entities: %@", error.localizedDescription);
	}
	
	if (array && [array isKindOfClass:NSArray.class]) {
		return [self initEntitiesOf:theClass withArray:array];
	}
	
	return nil;
}

- (id)copyWithZone:(NSZone *)zone;
{
	VMEntityCollection *copy = [[self.class allocWithZone:zone] init];
	copy->entities = entities;
	return copy;
}

- (id)collectionFromArrayOfDictionaries:(NSArray *)dictionaries forClass:(id)theClass;
{
	VMEntityCollection *collection = [[VMEntityCollection alloc]init];
	NSMutableArray *mutable = [NSMutableArray array];
	for (NSDictionary *dictionary in dictionaries) {
		VMEntity *entity = [[theClass alloc ] initWithDictionary:dictionary];
		[mutable addObject:entity];
	}
	collection->entities = [mutable copy];
	return collection;
}

- (id)collectionFromEntities:(NSArray *)_entities;
{
	VMEntityCollection *collection = [VMEntityCollection new];
	collection->entities = [_entities copy];
	return collection;
}

- (id)deserializeCollectionOf:(id)theClass usingKey:(NSString *)key;
{
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	NSArray *dictionaries = [defaults objectForKey:key];
	if (dictionaries) {
		return [self collectionFromArrayOfDictionaries:dictionaries forClass:theClass];
	}
	return [VMEntityCollection new];
}

- (void)serializeUsingKey:(NSString *)key;
{
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	[defaults setObject:[self arrayOfDictionaries] forKey:key];
	[defaults synchronize];
}

- (NSArray *)arrayOfDictionaries;
{
	NSMutableArray *mutable = [NSMutableArray array];
	for (VMEntity *entity in entities) {
		[mutable addObject:entity.dictionaryRepresentation];
	}
	return [mutable copy];
}

- (void)setEntities:(NSArray *)newEntities
{
	entities = newEntities.mutableCopy;
}

- (NSArray *)entities
{
	return [NSArray arrayWithArray:entities];
}

@end
