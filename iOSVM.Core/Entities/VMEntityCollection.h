//
//  VMPersistentEntityCollection.h
//  iOSVM.Core
//
//  Created by Alexander Kryshtalev on 17/06/14.
//  Copyright (c) 2014 Voodoo Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VMEntityCollection : NSObject<NSCopying>
{
	NSMutableArray *entities;
}

- (id)init;
- (id)initWithEntities:(NSArray *)entities;
- (id)initEntitiesOf:(id)theClass withArray:(NSArray *)array;
- (id)initEntitiesOf:(id)theClass withJson:(NSString *)json;
- (id)deserializeCollectionOf:(id)theClass usingKey:(NSString *)key;
- (void)serializeUsingKey:(NSString *)key;

@property (nonatomic, retain) NSArray *entities;

@end
