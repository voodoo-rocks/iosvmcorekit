//
//  VMPersistentEntity.h
//  iOSVM.Core
//
//  Created by Alexander Kryshtalev on 17/06/14.
//  Copyright (c) 2014 Voodoo Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VMDynamicObject.h"

@interface VMEntity : VMDynamicObject
{

}

- (id)initWithJson:(NSString *)json;
- (id)initWithXml:(NSString *)xml;

- (NSDictionary *)dictionaryFromJson:(NSString *)json;

@end
