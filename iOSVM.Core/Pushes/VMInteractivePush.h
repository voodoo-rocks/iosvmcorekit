//
//  VMInteractivePush.h
//  iOSVM.Core
//
//  Created by Alexander Kryshtalev on 19/11/14.
//  Copyright (c) 2014 Voodoo Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VMEntity.h"

@class VMInteractivePush;
@class VMAppDelegate;

typedef void(^VMInteractivePushBlock)(VMAppDelegate *delegate, VMInteractivePush *interactivePush);

@interface VMInteractivePush : NSObject

- (id)initWithTitle:(NSString *)title withBlock:(VMInteractivePushBlock)block;

@property (copy) NSString *title;
@property (copy) NSString *identifier;
@property BOOL usesUI;
@property BOOL isDestructive;
@property (copy) VMInteractivePushBlock didPerformAction;

@end
