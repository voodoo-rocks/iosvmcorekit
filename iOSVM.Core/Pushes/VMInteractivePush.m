//
//  VMInteractivePush.m
//  iOSVM.Core
//
//  Created by Alexander Kryshtalev on 19/11/14.
//  Copyright (c) 2014 Voodoo Mobile. All rights reserved.
//

#import "VMInteractivePush.h"

@implementation VMInteractivePush

- (id)initWithTitle:(NSString *)title withBlock:(VMInteractivePushBlock)block;
{
	self = [self init];
	
	if (self) {
		self.title = title;
		self.identifier = title;
		self.didPerformAction = block;
		self.usesUI = YES;
		self.isDestructive = NO;
	}
	
	return self;
}

@end
