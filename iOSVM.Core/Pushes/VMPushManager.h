//
//  VMPushManager.h
//  Munster
//
//  Created by Alexander Kryshtalev on 25/01/14.
//  Copyright (c) 2014 Voodoo Mobile. All rights reserved.
//

#import "VMSharedManager.h"

@interface VMPushManager : VMSharedManager

- (NSString *)stringFromDeviceToken:(NSData *)deviceToken;

@end
