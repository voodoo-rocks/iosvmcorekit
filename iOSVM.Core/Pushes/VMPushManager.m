//
//  VMPushManager.m
//  Munster
//
//  Created by Alexander Kryshtalev on 25/01/14.
//  Copyright (c) 2014 Voodoo Mobile. All rights reserved.
//

#import "VMPushManager.h"

@implementation VMPushManager

- (NSString *)stringFromDeviceToken:(NSData *)deviceToken;
{
	NSString* token = [[[[deviceToken description]
						  stringByReplacingOccurrencesOfString: @"<" withString: @""]
						 stringByReplacingOccurrencesOfString: @">" withString: @""]
						stringByReplacingOccurrencesOfString: @" " withString: @""];
	return token;
}

@end
